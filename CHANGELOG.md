# 0.2.9
## Enhancements
- **nv-search-box:** Remove input outline
- **nv-search-box:** Fix the vertical align of label
- **nv-search-box:** Fix the width of input
- **nv-text-field:** Fix the width of input
- **nv-select:** Remove border-color when disabled
- **nv-button:** Fix circle button sizes

# 0.2.8
## Enhancements
- **inputs:** Remove inputs outlines
- **nv-tab:** Remove tab button outline
- **nv-select:** Add a new animation scale when open the select
- **nv-list-item:** Move the default slot to end after all content

## Fix
 - **nv-panel** Add z-index to nv-panel
# 0.2.7
## Fix
 - **nv-panel** Add z-index to nv-panel

# 0.2.6
## Fix
 - **css** Remove duplicated utils code
 - **css** Add Nuiverse Global style removed in 0.2.4

# 0.2.5
## Fix
 - **dist** Include again dist folder in npm publis

# 0.2.4
## Fix
 - **dist** Include again dist folder in npm publis

# 0.2.3
## Features
 - **nv-panel** Add new wc component. A elegant lateral panel.

# 0.2.2

## Enhancements
- **nv-tabs:** Remove background
- **tables:** Fix the table header's padding 
- **nv-dialog:** Add scrollbar to dialogs's content
- **nv-list:** Add table style with "details"
- **nv-select:** Fix the height
- **nv-select:** Change text-label to label
- **nv-dialogs:** Fix botton button's aligns
- **nv-list:** Fix botton button's aligns
- **fix:** Add z-index to nv-toasts
- **nv-text-field:** Fix textarea width
- **nv-text-field:** Remove textarea background
- **nv-text-field:** Fix label position when use multiline
- **nv-dialog:** Add button's separator
- **nv-dialog:** Fix padding in mobile size
- **nv-dialog:** Fix background-color
- **nv-list:** Fix vertical align row's content
- **nv-loader:** Remove ununcessary margin
- **nv-app:** add nv-toasts by default
- **nv-radio-groups:** Update to use shadow dom
- **nv-radio-groups:** Add mutable value attribute
- **nv-radio-groups:** Add the nvChange event
- **nv-radio:** Remove nvChange event
- **nv-radio:** Add focus (nvFocus) and blur(nvBlur) events
- **nv-radio:** Use shadow dom
- **nv-radio:** Remove "color" prop
- **nv-checkbox:** Use shadow dom
- **nv-switch:** Use shadow dom
- **nv-switch:** Remove "color" prop
- **nv-switch:** Add nvChange, nvBlur and nvFocus events
- **nv-list:** Use shadow dom
- **nv-tabs:** Use shadow dom
- **nv-tab-bar:** Use shadow dom
- **nv-tab-button:** Use shadow dom
- **nv-button:** Use shadow dom
- **nv-button:** Add slots to create dropdows
- **nv-toasts:** Use shadow dom
- **nv-toast:** Use shadow dom
- **nv-toast:** Display a default icon
- **nv-toast:** Change the prop name "textHeader" to "header"
- **nv-search-box:** Use shadow dom
- **nv-search-box:** Add loader
- **nv-search-box:** Add method to clear content
- **nv-select:** Use shadow dom
- **nv-select:** Add filter to search in list
- **nv-select:** Add loader
- **nv-list:** Fix vertical align of headers
- **nv-button:** Fix width of empty slots
- **nv-tooltip:** Use shadow dom
- **nv-button:** Add primary and danger colors
- **nv-switch**: Fix height
- **nv-tabs**: Check if tabBar exists before apply events
- **nv-select**: Sync selected options in value prop
- **nv-people-picker**: Move people list to top of body

## Features
- **CSS:**
  - **z-index utils:** Added z-index utils, now we have the `nv-zindex-{value}` to apply a z-index value. The start is 10 and the end in 1000, with increment of 10.
  - **positions:** Add basic positions helpers, Now we have the top, left, right and bottom css utils with position 0. To use it, add the class `nv-{top | bottom | left | right}-0`.
- **nv-button:** Add slots start and end to buttons to support icons.
- **nv-button:** Add circle style
- **nv-text-field:** Add dense style.
- **nv-text-field:** Add naked style to inputs.

- **nv-avatar:** Display image when the component has a valid src attribute, If the image not exists or the downlaod fail, the component will display the text.
- **nv-app:** A component to apply themes, updates and animations.
- **Themes:** Theme dark and light
- **Colors:** Create a new system color, Added dark and light theme to all components
- **nv-navigation-items:** Fix the display of titles

## Dependencies
- **Stencil:** Update the Stencil version.

# 0.1.5 [(2020-1-4)](https://gitlab.com/nuinalp/nuiverse/nuiverse-ui/compare/v0.1.4...v0.1.5)

## Enhancements
- **SSR:**
  - **nv-navigation-view:** Check if the child elements exists in the array, for don't display an error in the console.
  - **nv-navigation-view-header:** Remove duplicate `header` in the NvNavigationViewHeader.

## Features
- **Font:** Add new font variant `FiraGO Medium`.

## Dependencies
- **Stencil:** Update the Stencil version.


# 0.1.4 [(2019-11-15)](https://gitlab.com/nuinalp/nuiverse/nuiverse-ui/compare/v0.1.3...v0.1.4)
## Bugs
- **CSS:**
  - **Grid system:** Fixed the `nv-grid-full`, `nv-grid-half` and `nv-grid-smart`.

# 0.1.3 [(2019-11-14)](https://gitlab.com/nuinalp/nuiverse/nuiverse-ui/compare/v0.1.2...v0.1.3)

## Bugs
- **nv-text-field:** Fixed the width when show the password.
- **CSS**
  - **.nv-grid-no-margin:** Remove margin from all elements on mobile view.
  - **Grid coluns:** Now the coluns will have width 100% on mobile view.
  - **nv-navigation-view-separator:** Fix the `width` calc.

## Features
- **nv-dialog-header:** New component for the nv-dialog.
- **nv-icon:** New component for the Nuiverse Icons.
- **nv-list-item:** New slots (start and end).
- **nv-navigation-view:** New center slot.

## Enhancements
- **nv-text-field:** New animation for the label.
- **nv-list:** Add `display: grid` property to list container, for correct `text-overflow`.
- **nv-list-item:** Fix the position of the `avatar-slot`.
- **nv-list-item:** Hidden empty `avatar-slot`.
- **nv-dialog:** Fix the z-index.


# 0.1.2 [(2019-10-29)](https://gitlab.com/nuinalp/nuiverse/nuiverse-ui/compare/v0.1.0-Pre-release...v0.1.2)

## Bugs
- **nv-navigation-view-separator:** Fix width calc ([12095c1e](https://gitlab.com/nuinalp/nuiverse/nuiverse-ui/commit/12095c1eb37980f21b1c76b7b393815dc1937b8c)).
- **CSS**
  - **code:** Remove the default padding ([12095c1e](https://gitlab.com/nuinalp/nuiverse/nuiverse-ui/commit/12095c1eb37980f21b1c76b7b393815dc1937b8c)).
  - **kbd:** Remove the default padding ([12095c1e](https://gitlab.com/nuinalp/nuiverse/nuiverse-ui/commit/12095c1eb37980f21b1c76b7b393815dc1937b8c)).

## Features
- **nv-code:** Add support for all languages for [Prismjs](https://prismjs.com/) ([12095c1e](https://gitlab.com/nuinalp/nuiverse/nuiverse-ui/commit/12095c1eb37980f21b1c76b7b393815dc1937b8c))
- **nv-code-demo:** A new component for show a demo of code. It's used with `nv-code`.

## Enhancements
- **form:**  Remove border-radius ([12095c1e](https://gitlab.com/nuinalp/nuiverse/nuiverse-ui/commit/12095c1eb37980f21b1c76b7b393815dc1937b8c)).
- **nv-text-field:** The text helper aren't displayed if is empty ([12095c1e](https://gitlab.com/nuinalp/nuiverse/nuiverse-ui/commit/12095c1eb37980f21b1c76b7b393815dc1937b8c)).
- **nv-text-field:** Remove margin-top if the label is undefined ([12095c1e](https://gitlab.com/nuinalp/nuiverse/nuiverse-ui/commit/12095c1eb37980f21b1c76b7b393815dc1937b8c)).
- **CSS**
  - Add a left padding to `<ul>` elements ([12095c1e](https://gitlab.com/nuinalp/nuiverse/nuiverse-ui/commit/12095c1eb37980f21b1c76b7b393815dc1937b8c)).
  - Add padding to `<td>`, `<th>` elements ([12095c1e](https://gitlab.com/nuinalp/nuiverse/nuiverse-ui/commit/12095c1eb37980f21b1c76b7b393815dc1937b8c)).
- **Build**
  - Add src folder to npm package.

## Breaking Changes
- **Events**
  - **nv-text-field:** Use `nvInput` for listen the input event ([12095c1e](https://gitlab.com/nuinalp/nuiverse/nuiverse-ui/commit/12095c1eb37980f21b1c76b7b393815dc1937b8c)).
  - **nv-search-box:** Use `nvInput` for listen the input event ([12095c1e](https://gitlab.com/nuinalp/nuiverse/nuiverse-ui/commit/12095c1eb37980f21b1c76b7b393815dc1937b8c)).

## 🐣 0.1.0 (2019-10-18)
- The first release \o/