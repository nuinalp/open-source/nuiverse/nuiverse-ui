/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Config }          from '@stencil/core';
import { postcss }         from '@stencil-community/postcss';
import * as IPostCssConfig from '@stencil-community/postcss/dist/declarations';
import autoprefixer        from 'autoprefixer';
import { sass }            from '@stencil/sass';

const postCssConfig: IPostCssConfig.PluginOptions = { plugins: [autoprefixer()] };
export const config: Config = {
  namespace: 'nuiverse-ui',
  autoprefixCss: true,
  sourceMap: true,
  plugins: [
    sass({
      injectGlobalPaths: [
        './src/style/utils/_variables.scss',
        './src/style/_mixins.scss',
      ],
    }),
    postcss(postCssConfig),
  ],
  ...(process.env.NODE_ENV === 'prod' ? { globalStyle: 'src/style/nuiverse.scss' } : {}),
  outputTargets: [
    {
      type: 'dist',
      esmLoaderPath: '../dist/loader',
    },
    {
      type: 'dist-lazy',
      dir: 'dist/lazy',
    },
    {
      type: 'dist-custom-elements',
      customElementsExportBehavior: 'auto-define-custom-elements',
      externalRuntime: false,
      dir: 'dist/custom-elements'
    },
    {
      type: 'docs-readme',
    },
    {
      type: 'docs-json',
      file: '../docs/core.json',
    },
    {
      type: 'dist-hydrate-script',
      dir: 'dist/hydrate',
    },
    {
      type: 'docs-vscode',
      file: 'dist/html.html-data.json',
      sourceCodeBaseUrl: 'https://gitlab.com/nuinalp/open-source/fx/fx-framework/core',
    },
    {
      type: 'dist-custom-elements',
      dir: 'dist/components',
      copy: [{
        src: '../scripts/custom-elements',
        dest: 'dist/components',
        warn: true
      }],
      includeGlobalScripts: false
    },
    {
      type: 'www',
      serviceWorker: null, // disable service workers
      copy: [
        { src: 'fonts', dest: 'collection/assets/fonts' },
        { src: 'demos', dest: 'demos' },
        { src: '../assets/*', dest: 'assets/' },
        { src: '../assets/avatars', dest: 'assets/avatars' },
        { src: '../temp/*', dest: 'build/' },
      ],
    },
  ],
  testing: {
    /**
     * Gitlab CI doesn't allow sandbox, therefor this parameters must be passed to your Headless Chrome
     * before it can run your tests
     */
    browserHeadless: "new",
    browserArgs: ['--no-sandbox', '--disable-setuid-sandbox'],
  },
  devServer: {
    reloadStrategy: 'pageReload',
  },
  buildEs5: 'prod',
  preamble: '(C) Nuiverse UI by Nuinalp - BSD-3-Clause',
  enableCache: true,
  transformAliasedImportPaths: true,
  extras: {
    /**
     * `experimentalSlotFixes` is necessary in Stencil v4 until the fixes described in
     * {@link https://stenciljs.com/docs/config-extras#experimentalslotfixes the Stencil docs for the flag} are the
     * default behavior (slated for a future Stencil major version).
     */
    experimentalSlotFixes: true,
    /**
     * `experimentalScopedSlotChanges` is necessary in Stencil v4 until the fixes described in
     * {@link https://stenciljs.com/docs/config-extras#experimentalscopedslotchanges the Stencil docs for the flag} are
     * the default behavior (slated for a future Stencil major version).
     */
    experimentalScopedSlotChanges: true,
    enableImportInjection: true,
  }
};
