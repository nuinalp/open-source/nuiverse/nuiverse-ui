/**
* Copyright (c) 2020 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { paramCase } from 'change-case';

interface ICreateToast {
  header?: string;
  message: string;
  icon?: HTMLElement | any;
  opts?: {
    showClose?: boolean;
    showActions?: boolean;
    duration?: number;
    autoClose: boolean;
    closeOnClick: boolean;
  };
}

export function createToast({ header, message, icon, opts }: ICreateToast): void {
  const toast = document.createElement('nv-toast');
  const toasts = document.querySelector('nv-toasts');

  if (header) toast.header = header;
  if (message) toast.textContent = message;

  if (icon) toast.appendChild(icon);

  // Set config params
  if (opts && Object.keys(opts).length > 0) {
    Object.keys(opts).forEach((keyName: any) => {
      toast.setAttribute(paramCase(keyName), (<any>opts)[keyName]);
    });
  }

  if (toasts) {
    toasts.appendChild(toast);
    toast.open();
  }
}
