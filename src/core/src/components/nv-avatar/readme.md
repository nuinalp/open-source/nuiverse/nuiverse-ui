# nv-avatar



<!-- Auto Generated Below -->


## Properties

| Property    | Attribute    | Description                             | Type                                                                          | Default     |
| ----------- | ------------ | --------------------------------------- | ----------------------------------------------------------------------------- | ----------- |
| `alt`       | `alt`        | The image alt                           | `string \| undefined`                                                         | `undefined` |
| `color`     | `color`      | The avatar color                        | `"danger" \| "primary" \| "secondary" \| "success" \| "warning" \| undefined` | `undefined` |
| `size`      | `size`       | The avatar size                         | `"large" \| "medium" \| "small" \| undefined`                                 | `'medium'`  |
| `src`       | `src`        | The image src for the `<img/>` element. | `string \| undefined`                                                         | `undefined` |
| `text`      | `text`       | The text for the avatar.                | `string \| undefined`                                                         | `undefined` |
| `textColor` | `text-color` | The text color                          | `"danger" \| "primary" \| "secondary" \| "success" \| "warning" \| undefined` | `undefined` |


## Dependencies

### Used by

 - [nv-people-picker](../nv-people-picker)

### Graph
```mermaid
graph TD;
  nv-people-picker --> nv-avatar
  style nv-avatar fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
