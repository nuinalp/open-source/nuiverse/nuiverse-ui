/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, Prop, h, Host } from '@stencil/core';
import { Color }              from '../../interface';
@Component({
  tag: 'nv-avatar',
  styleUrl: './style.scss',
  shadow: true,
})
export class NvAvatar {

  /**
   * The text for the avatar.
   */
  @Prop() text?: string;

  /**
   * The image src for the `<img/>` element.
   */
  @Prop() src?: string;

  /**
   * The image alt
   */
  @Prop() alt?: string;

  /**
   * The avatar color
   */
  @Prop() color?: Color;

  /**
   * The text color
   */
  @Prop() textColor?: Color;

  /**
   * The avatar size
   */
  @Prop() size?: 'small' | 'medium' | 'large' = 'medium';

  /**
   * Remove the src attribute to display only text if the image is not found.
   */
  private onError = () => this.src = undefined;

  private renderImg(): HTMLObjectElement | null {
    if (this.src) {
      return <img src={ this.src } alt={ this.alt } onError={this.onError}/>;
    }
    return null;
  }

  private renderTxt(): HTMLObjectElement | null {
    if (this.text && !this.src) {
      const text: string = this.text[0].toUpperCase();
      return (
        <span>
          { text }
        </span>
      );
    }
    return null;
  }

  render() {
    return (
      <Host class={{
        'nv-avatar': true,
        'nv-avatar-sm': this.size === 'small' ? true : false,
        'nv-avatar-md': this.size === 'medium' ? true : false,
        'nv-avatar-lg': this.size === 'large' ? true : false,
      }}>
        <div class='nv-avatar-content'>
          { this.renderTxt() }
          { this.renderImg() }
        </div>
      </Host>
    );
  }
}
