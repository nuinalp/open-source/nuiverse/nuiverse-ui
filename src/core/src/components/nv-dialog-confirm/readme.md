# nv-dialog-confirm



<!-- Auto Generated Below -->


## Properties

| Property                   | Attribute      | Description                                  | Type                                                                          | Default     |
| -------------------------- | -------------- | -------------------------------------------- | ----------------------------------------------------------------------------- | ----------- |
| `color`                    | `color`        | The color for the confirm button             | `"danger" \| "primary" \| "secondary" \| "success" \| "warning" \| undefined` | `undefined` |
| `textCancel` _(required)_  | `text-cancel`  | The cancel button text for this dialog       | `string`                                                                      | `undefined` |
| `textConfirm` _(required)_ | `text-confirm` | The confirmation button text for this dialog | `string`                                                                      | `undefined` |


## Methods

### `toggle() => Promise<void>`

Close or open the nv-dialog.

#### Returns

Type: `Promise<void>`




## Dependencies

### Depends on

- [nv-button](../nv-button)

### Graph
```mermaid
graph TD;
  nv-dialog-confirm --> nv-button
  style nv-dialog-confirm fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
