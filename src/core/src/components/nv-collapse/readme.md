# nv-collapse



<!-- Auto Generated Below -->


## Properties

| Property    | Attribute   | Description                                           | Type      | Default |
| ----------- | ----------- | ----------------------------------------------------- | --------- | ------- |
| `accordion` | `accordion` | If `true`, the collapse will have a accordion effect. | `boolean` | `false` |


## Methods

### `closeAllItems(el: HTMLElement) => Promise<void>`

Call this method to close all Collapse Itens.

#### Parameters

| Name | Type          | Description |
| ---- | ------------- | ----------- |
| `el` | `HTMLElement` |             |

#### Returns

Type: `Promise<void>`




----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
