/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h } from '@stencil/core';

@Component({
  tag: 'nv-card-title',
  shadow: false,
})
export class NvCardTitle {
  render() {
    return <h5 class='nv-card-title'><slot /></h5>;
  }
}
