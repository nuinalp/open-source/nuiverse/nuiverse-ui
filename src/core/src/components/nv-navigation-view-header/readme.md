# nv-navigation-view-header



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute   | Description                                                                       | Type      | Default |
| ---------- | ----------- | --------------------------------------------------------------------------------- | --------- | ------- |
| `centerMb` | `center-mb` | If `true`, the nv-navigation-view-header will be moved to center on mobile views. | `boolean` | `false` |


## Dependencies

### Used by

 - [nv-navigation-view-items](../nv-navigation-view-items)

### Graph
```mermaid
graph TD;
  nv-navigation-view-items --> nv-navigation-view-header
  style nv-navigation-view-header fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
