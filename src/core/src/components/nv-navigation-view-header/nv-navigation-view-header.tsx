/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Host, Prop } from '@stencil/core';

@Component({
  tag: 'nv-navigation-view-header',
  shadow: false,
})
export class NvNavigationItemHeader {
  /**
   * If `true`, the nv-navigation-view-header will be moved to center on mobile views.
   */
  @Prop() centerMb: boolean = false;

  render() {
    return (
      <Host class='nv-nav-item-header nv-no-move'>
        <header class='nv-nav-title nv-h6'>
          <slot/>
        </header>
      </Host>
    );
  }
}
