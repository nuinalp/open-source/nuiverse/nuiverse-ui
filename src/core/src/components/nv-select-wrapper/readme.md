# nv-select-wrapper



<!-- Auto Generated Below -->


## Properties

| Property               | Attribute                 | Description                                                                                                                                                              | Type      | Default               |
| ---------------------- | ------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | --------- | --------------------- |
| `debounce`             | `debounce`                | Set the amount of time, in milliseconds, to wait to trigger the nvInputChange event after each keystroke. Don't use it directly, it is only used by nv-select component. | `number`  | `0`                   |
| `filter`               | `filter`                  | If `true`, the select will have a search box to find by label attribute in nv-select-option items. Don't use it directly, it is only used by nv-select component.        | `boolean` | `false`               |
| `loading`              | `loading`                 | If `true`, the loading will be display Don't use it directly, it is only used by nv-select component.                                                                    | `boolean` | `false`               |
| `multiple`             | `multiple`                | If `true`, the select can accept multiple values. Don't use it directly, it is only used by nv-select component.                                                         | `boolean` | `false`               |
| `noDataAvailableLabel` | `no-data-available-label` | The text for the no results label in filter. Don't use it directly, it is only used by nv-select component.                                                              | `string`  | `'No data available'` |


## Events

| Event           | Description                                                                                               | Type                         |
| --------------- | --------------------------------------------------------------------------------------------------------- | ---------------------------- |
| `nvInputChange` | Emmited when the nvInputChange has changed Don't use it directly, it is only used by nv-select component. | `CustomEvent<nvInputChange>` |


## Methods

### `clearSearchBox() => Promise<void>`

Clear nv-text-field content

#### Returns

Type: `Promise<void>`



### `registerOptions(els: HTMLNvSelectOptionElement[]) => Promise<void>`

Add all options inside the nv-select-wrapper

#### Parameters

| Name  | Type                          | Description |
| ----- | ----------------------------- | ----------- |
| `els` | `HTMLNvSelectOptionElement[]` |             |

#### Returns

Type: `Promise<void>`



### `registerSelect(el: HTMLNvSelectElement) => Promise<void>`

Register nv-select

#### Parameters

| Name | Type                  | Description |
| ---- | --------------------- | ----------- |
| `el` | `HTMLNvSelectElement` |             |

#### Returns

Type: `Promise<void>`



### `registerSelectItem(el: HTMLNvSelectOptionElement) => Promise<void>`

Handle nvSelectOptionSelect event from nv-select-option

#### Parameters

| Name | Type                        | Description |
| ---- | --------------------------- | ----------- |
| `el` | `HTMLNvSelectOptionElement` |             |

#### Returns

Type: `Promise<void>`



### `setFocus() => Promise<void>`

Set focus in nv-text-field

#### Returns

Type: `Promise<void>`



### `toggle() => Promise<void>`

Toggle nv-select

#### Returns

Type: `Promise<void>`



### `unregisterSelectItem(el: HTMLNvSelectOptionElement) => Promise<void>`

Remove nvSelectOptionSelect event handle from nv-select-option

#### Parameters

| Name | Type                        | Description |
| ---- | --------------------------- | ----------- |
| `el` | `HTMLNvSelectOptionElement` |             |

#### Returns

Type: `Promise<void>`




## Dependencies

### Used by

 - [nv-select](../nv-select)

### Depends on

- [nv-select-option-wrapper](../nv-select-option-wrapper)
- [nv-search-box](../nv-search-box)

### Graph
```mermaid
graph TD;
  nv-select-wrapper --> nv-select-option-wrapper
  nv-select-wrapper --> nv-search-box
  nv-search-box --> nv-loader
  nv-select --> nv-select-wrapper
  style nv-select-wrapper fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
