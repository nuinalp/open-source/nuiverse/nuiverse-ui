/**
* Copyright (c) 2020 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  h,
  Host,
  Prop,
  State,
  Method,
  Event,
  EventEmitter,
  Element,
  Watch,
} from '@stencil/core';
import {
  SelectOptionSelectedDetail,
  SelectedValues,
  InputChangeEventDetail,
  SelectChangeDetail,
} from '../../interface';
import { nvInputChange } from '../nv-select/interface';
import { INvChangeSelectWrapper } from './interface';

@Component({
  tag: 'nv-select-wrapper',
  styleUrl: './style.scss',
  shadow: true,
})
export class NvSelectWrapper {
  //
  // Reference to the host element.
  //

  @Element() el!: HTMLNvSelectWrapperElement;
  wrapper!: HTMLDivElement;
  options: HTMLNvSelectOptionElement[] = [];
  addedEvents = false;
  searchBoxEl?: HTMLNvSearchBoxElement;
  timer!: ReturnType<typeof setTimeout> | null;
  select!: HTMLNvSelectElement;
  values: any[] = [];

  //
  // State
  //

  @State() filterText?: string;
  @State() displayNoDataAvailable = false;

  //
  // Props
  //

  /**
   * If `true`, the select can accept multiple values.
   * Don't use it directly, it is only used by nv-select component.
   */
  @Prop() multiple: boolean = false;

  /**
   * If `true`, the select will have a search box to find by label attribute in nv-select-option items.
   * Don't use it directly, it is only used by nv-select component.
   */
  @Prop() filter: boolean = false;

  /**
   * If `true`, the loading will be display
   * Don't use it directly, it is only used by nv-select component.
   */
  @Prop({ mutable: true }) loading = false;

  /**
   * The text for the no results label in filter.
   * Don't use it directly, it is only used by nv-select component.
   */
  @Prop({ mutable: true }) noDataAvailableLabel: string = 'No data available';

  /**
   * Set the amount of time, in milliseconds, to wait to trigger the nvInputChange event after each keystroke.
   * Don't use it directly, it is only used by nv-select component.
   */
  @Prop() debounce = 0;

  /**
   * @internal
   */
  @Prop({ mutable: true }) listAlignBottom: boolean = false;

  //
  // Events
  //

  /**
   * Emmited when the nvInputChange has changed
   * Don't use it directly, it is only used by nv-select component.
   */
  @Event() nvInputChange!: EventEmitter<nvInputChange>;

  /**
   * Emitted when the value has changed.
   * @internal
   */
  @Event({
    eventName: 'nvChange',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) nvChange!: EventEmitter<INvChangeSelectWrapper>;

  /**
   * @internal
   */
  @Event({
    eventName: 'nvDidChange',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) nvDidChange!: EventEmitter<SelectChangeDetail>;



  //
  // Watchers
  //

  @Watch('filterText')
  filterList() {
    requestAnimationFrame(() => {
      const query = 'nv-select-option-wrapper';
      const items = this.el.querySelectorAll(query) as NodeListOf<HTMLNvSelectOptionElement>;
      if (items.length > 0) {
        let count = 0;
        items.forEach((item) => item.hidden ? count += 1 : null);
        if (count === items.length) this.displayNoDataAvailable = true;
        else this.displayNoDataAvailable = false;
      }
    })
  }

  //
  // Methods
  //

  /**
   * Clear nv-text-field content
   * @intenal
   */
  @Method()
   async clearSearchBox() {
    if (this.filter) {
      const searchbox = this.el.shadowRoot?.querySelector('nv-search-box');
      searchbox?.clear();
    }
  }

  /**
   * Set focus in nv-text-field
   * @intenal
   */
  @Method()
   async setFocus() {
     setTimeout(() => this.searchBoxEl?.setFocus(), 200);
  }

  /**
   * Toggle nv-select
   * @intenal
   */
  @Method()
   async toggle() {
     await this.select.toggle();
  }

  /**
   * Add all options inside the nv-select-wrapper
   * @intenal
   */
  @Method()
  async registerOptions(els: HTMLNvSelectOptionElement[]) {
    requestAnimationFrame(() => {
      const frag = document.createDocumentFragment();
      els.forEach((el, idx) => {
        const id = `${idx}-${el.value}`;
        if (this.values.indexOf(id) === -1) {
          const elCopy = el.cloneNode(true) as HTMLNvSelectOptionElement;
          elCopy.setAttribute('moving', 'true'); 
          elCopy.setAttribute('nv-select-id', id);
          el.setAttribute('nv-select-id', id);
          frag.appendChild(elCopy);
          
          const optionWraper = document.createElement('nv-select-option-wrapper');
          optionWraper.setAttribute('moving', 'true');
          
          const attributes = el.attributes as any;

          Object.keys(attributes).forEach((keyName) => {
            const key = attributes[keyName as keyof typeof el.attributes];
            if (key.name !== 'class') optionWraper.setAttribute(key.name, key.value);
          })
          
          optionWraper.setAttribute('value', el.value);
          optionWraper.append(...elCopy.childNodes);
          elCopy.replaceWith(optionWraper);
          optionWraper.setAttribute('moving', 'false');
          this.values.push(id);
        }
      })
  
      this.values;
      this.el.appendChild(frag);
    });
  }

  /**
   * Handle nvSelectOptionSelect event from nv-select-option
   * @intenal
   */
  @Method()
  async registerSelectItem(el: HTMLNvSelectOptionElement) {
    el.addEventListener('nvSelectOptionSelect', this.nvSelectOptionSelectHandler.bind(this));
  }

  /**
   * Register nv-select
   * @intenal
   */
  @Method()
  async registerSelect(el: HTMLNvSelectElement) {
    this.select = el;
  }

  /**
   * Remove nvSelectOptionSelect event handle from nv-select-option
   * @intenal
   */
  @Method()
  async unregisterSelectItem(el: HTMLNvSelectOptionElement) {
    el.removeEventListener('nvSelectOptionSelect', this.nvSelectOptionSelectHandler.bind(this));
    const selectId = el.getAttribute('nv-select-id');
    if (!selectId) return;

    const idx = this.values.indexOf(selectId);
    this.values.splice(idx, 1);

    requestAnimationFrame(() => {
      this.el.querySelector(`nv-select-option-wrapper[value="${el.value}"]`)?.remove();
      this.getValues.bind(this)()
    });
  }

  private nvSelectOptionSelectHandler = (event: CustomEvent<SelectOptionSelectedDetail>) => {
    if (this.multiple) {
      this.nvDidChange.emit({
        option: event.detail.option,
        multiple: true,
      });
      this.getValues();
      return;
    }

    this.nvDidChange.emit({
      option: event.detail.option,
      multiple: false,
    });
    this.getValues();
  }

  private getValues(): void {
    const selectedValues: SelectedValues[] = [];
    this.el.querySelectorAll('nv-select-option-wrapper').forEach((option) => {
      console.info('OPTION ', option.value);
      if (option.selected) {
        selectedValues.push({
          label: option.label ? option.label : option.innerText,
          value: option.value || undefined,
        });
      }
    });

    this.nvChange.emit({ value: selectedValues });
  }

  private handleNvInput = (ev: CustomEvent<InputChangeEventDetail>): void => {
    ev.preventDefault();
    ev.stopImmediatePropagation();
    const { detail: { value } } = ev;

    if (this.timer) clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.filterText = value || '';
      
      this.nvInputChange.emit({ value });

      if (this.timer) clearTimeout(this.timer);
      this.timer = null;
    }, this.debounce);
  }

  render() {
    return (
      <Host
        class={{
          'nv-select-wrapper': true,
          'nv-select-align-items-bottom': this.listAlignBottom,
        }}
      >
        <div>
          <div class='nv-select-items'>
            <div class='nv-w-100'>
              {
                this.filter &&
                <nv-search-box
                  class="nv-w-100"
                  ref={ el => this.searchBoxEl = el }
                  loading={this.loading}
                  onNvChange={this.handleNvInput}
                />
              }
              <div class='nv-separator' />
            </div>
            <div class='nv-w-100'>
              <slot />
              {
                this.filter && 
                <nv-select-option-wrapper
                  class="no-data-msg"
                  hidden={!this.displayNoDataAvailable}
                  disabled
                >
                  { this.noDataAvailableLabel }
                </nv-select-option-wrapper>
              }
            </div>
          </div>
        </div>
      </Host>
    );
  }
}
