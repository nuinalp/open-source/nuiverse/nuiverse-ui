# nv-dialog-action



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                                  | Type                                                                          | Default     |
| ------------- | -------------- | -------------------------------------------- | ----------------------------------------------------------------------------- | ----------- |
| `color`       | `color`        | The color for the confirm button             | `"danger" \| "primary" \| "secondary" \| "success" \| "warning" \| undefined` | `undefined` |
| `headerTitle` | `header-title` | The header title for the dialog              | `boolean`                                                                     | `false`     |
| `textCancel`  | `text-cancel`  | The cancel button text for this dialog       | `string \| undefined`                                                         | `undefined` |
| `textConfirm` | `text-confirm` | The confirmation button text for this dialog | `string \| undefined`                                                         | `undefined` |
| `type`        | `type`         | The type for the dialog                      | `string`                                                                      | `'confirm'` |


## Methods

### `toggle() => Promise<void>`

Close or open the nv-dialog.

#### Returns

Type: `Promise<void>`




## Dependencies

### Depends on

- [nv-button](../nv-button)

### Graph
```mermaid
graph TD;
  nv-dialog-action --> nv-button
  style nv-dialog-action fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
