# nv-people-picker



<!-- Auto Generated Below -->


## Properties

| Property          | Attribute           | Description                                                                                               | Type       | Default              |
| ----------------- | ------------------- | --------------------------------------------------------------------------------------------------------- | ---------- | -------------------- |
| `debounce`        | `debounce`          | Set the amount of time, in milliseconds, to wait to trigger the nvInputChange event after each keystroke. | `number`   | `0`                  |
| `label`           | `label`             | The lable of input.                                                                                       | `string`   | `''`                 |
| `loading`         | `loading`           | If `true`, the loading will be display                                                                    | `boolean`  | `false`              |
| `notFoundMessage` | `not-found-message` | The text to "No results found" message                                                                    | `string`   | `'No results found'` |
| `suggestionList`  | --                  | The people list to display to user select an user.                                                        | `people[]` | `[]`                 |


## Events

| Event           | Description                                                      | Type                         |
| --------------- | ---------------------------------------------------------------- | ---------------------------- |
| `nvBlur`        | Emitted when the input loses focus.                              | `CustomEvent<FocusEvent>`    |
| `nvChange`      | Emmited when the user select or remove the itens of suggest list | `CustomEvent<nvChange>`      |
| `nvFocus`       | Emitted when the input has focus.                                | `CustomEvent<FocusEvent>`    |
| `nvInputChange` | Emmited when the nvInputChange has changed                       | `CustomEvent<nvInputChange>` |


## Dependencies

### Depends on

- [nv-list](../nv-list)
- [nv-list-item](../nv-list-item)
- [nv-avatar](../nv-avatar)
- [nv-label](../nv-label)
- [nv-icon](../nv-icon)
- [nv-loader](../nv-loader)

### Graph
```mermaid
graph TD;
  nv-people-picker --> nv-list
  nv-people-picker --> nv-list-item
  nv-people-picker --> nv-avatar
  nv-people-picker --> nv-label
  nv-people-picker --> nv-icon
  nv-people-picker --> nv-loader
  style nv-people-picker fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
