export interface people {
  value: any;
  firstName: string;
  lastName?: string;
  primaryText?: string;
  src?: string
}

export interface nvChange {
  value: people[];
}

export interface nvInputChange {
  value: any;
}