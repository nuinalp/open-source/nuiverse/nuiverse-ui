/**
* Copyright (c) 2020 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, Prop, h, Host, State, Watch, Event, EventEmitter, Element } from '@stencil/core';
import { nvChange, nvInputChange, people } from './interface';

@Component({
  tag: 'nv-people-picker',
  styleUrl: './style.scss',
  shadow: true,
})
export class NvPeoplePicker {
  //
  // Reference to the host element.
  //

  @Element() el!: HTMLNvSelectElement;
  inputEl!: HTMLInputElement;
  wrapper!: HTMLDivElement;

  //
  // State
  //
  @State() inputFocus = false;
  @State() peopleName: string = '';
  @State() selectedItems: people[] = [];
  @State() selectedIdx: number[] = [];
  @State() timer!: ReturnType<typeof setTimeout> | null;
  @State() peopleList: people[] = [];

  //
  // Props
  //

  /**
   * The people list to display to user select an user.
   */
  @Prop({ mutable: true }) suggestionList: people[] = [];

  /**
   * Set the amount of time, in milliseconds, to wait to trigger the nvInputChange event after each keystroke.
   */
  @Prop() debounce = 0;

  /**
   * If `true`, the loading will be display
   */
  @Prop({ mutable: true }) loading = false;

  /**
   * The text to "No results found" message
   */
  @Prop({ mutable: true }) notFoundMessage = 'No results found';

  /**
   * The lable of input.
   */
  @Prop() label = '';

  //
  // Watchs
  //
  @Watch('suggestionList')
  handleSuggestionList(newValue: any []) {
    this.updatePeopleList(newValue);
  }

  //
  // Events
  //

  /**
   * Emmited when the nvInputChange has changed
   */
  @Event() nvInputChange!: EventEmitter<nvInputChange>;

  /**
   * Emmited when the user select or remove the itens of suggest list
   */
  @Event() nvChange!: EventEmitter<nvChange>;

  /**
   * Emitted when the input loses focus.
   */
  @Event() nvBlur!: EventEmitter<FocusEvent>;

  /**
   * Emitted when the input has focus.
   */
  @Event() nvFocus!: EventEmitter<FocusEvent>;

  //
  // Lifecycles
  //

  componentWillLoad() {
    this.updatePeopleList();
    this.moveWrapper();
  }


  //
  // Methods
  //

  private updatePeopleList = (newValue: any[] = this.suggestionList) => {
    this.peopleList = newValue.map((data, id) => ({
      ...data,
      id
    }));
  }

  private removeLastItem = (e: KeyboardEvent) => {
    if (e.key === 'Backspace' && this.selectedItems.length > 0 && this.peopleName.length <= 0) {
      const newItems = [...this.selectedItems];
      const newItemsIdx = [...this.selectedIdx];

      newItems.pop();
      newItemsIdx.pop();

      this.selectedItems = newItems;
      this.selectedIdx = newItemsIdx;

      this.nvChange.emit({ value: this.selectedItems });
    }
  }

  private selectItem = (e: MouseEvent, idx: number) => {
    e.preventDefault();

    const peopleByIdx = this.peopleList.find(({ value }) => value === idx);

    const newSelected = [...this.selectedItems];

    if (peopleByIdx) {
      newSelected.push(peopleByIdx);
    }

    const newSelectedIdx = [...this.selectedIdx ];
    newSelectedIdx.push(idx);

    this.selectedItems = newSelected || [];
    this.selectedIdx = newSelectedIdx;
    this.inputEl.value = '';
    this.peopleName = '';
    this.inputEl.focus();
    this.nvChange.emit({ value: this.selectedItems });
  }

  private removeItem = (e: MouseEvent, idx: number) => {
    e.preventDefault();
    const idxSelectedItems = this.selectedItems.map(({ value }) => value).indexOf(idx);
    const idxSelectedIdx = this.selectedIdx.indexOf(idx);

    const newSelected = [...this.selectedItems];
    const newSelectedIdx = [...this.selectedIdx ];
    
    newSelected.splice(idxSelectedItems, 1);
    newSelectedIdx.splice(idxSelectedIdx, 1);

    this.selectedItems = newSelected;
    this.selectedIdx = newSelectedIdx;

    this.nvChange.emit({ value: this.selectedItems });
  }


  private input = (el: Event) => {
    if (this.timer) clearTimeout(this.timer);
    const { value } = el.target as HTMLInputElement;

    this.timer = setTimeout(() => {
      this.peopleName = value;

      this.nvInputChange.emit({ value })

      if (this.timer) clearTimeout(this.timer);
      this.timer = null;
    }, this.debounce);
  }

  private onFocus = (ev: FocusEvent): void => {
    this.inputFocus = true;
    this.nvFocus.emit(ev);
  }

  private onBlur = (ev: FocusEvent): void => {
    this.inputFocus = false;
    this.nvFocus.emit(ev);
  }

  private searchByName = (firstName: string = '', lastName: string = '', value: any) => {
    return this.selectedIdx.indexOf(value) === -1 && ( 
      firstName.match(new RegExp(this.peopleName.toLowerCase(), 'gi')) !== null ||
      lastName.match(new RegExp(this.peopleName.toLowerCase(), 'gi'))) !== null;
  }

  private renderSuggestList = () => {
    if (this.suggestionList.length <= 0) return;
    const search = this.peopleList.filter(({ firstName = '', lastName = '', value }) => this.searchByName(firstName, lastName, value));
    
    return (
      <div
        ref={el => {
          this.wrapper = el!
          this.moveWrapper();
        }}
        class={{
          'nv-people-picker': true,
          'suggest-list': true,
          'is-loading': this.loading,
          'no-search-text': this.peopleName?.length <= 0,
        }}
      >
        <nv-list users>
          { this.peopleList.length > 0 && search.length > 0 ? 
            this.peopleList
            .map(({ firstName, lastName, primaryText, src, value }) => (
              <nv-list-item
                first-name={firstName}
                last-name={lastName}
                primary-text={primaryText}
                onClick={(e) => this.selectItem(e, value)}
                hidden={!this.searchByName(firstName, lastName, value)}
              >
                <nv-avatar slot='end' src={src} text={firstName} />
            </nv-list-item>
            ))
            :
            (
              <nv-list-item>
                <div class='nv-people-picker-not-found'>
                  { this.notFoundMessage }
                </div>
            </nv-list-item>
            )
          }
        </nv-list>
    </div>
    );
  }

  private moveWrapper(): void {
    if (!this.wrapper) return;  
    window.requestAnimationFrame(() => {
      const { top, left } = this.el.getBoundingClientRect();
      const { width, height } = window.getComputedStyle(this.el);
      const { pageXOffset } = window;

      this.wrapper.style.width = width;
      this.wrapper.style.top = `${ top + parseInt(height, 10) }px`;
      this.wrapper.style.left = `${ left + pageXOffset }px`;
      this.wrapper.style.display = 'block';
      
      if (!this.wrapper.getAttribute('moved')) {
        this.wrapper.setAttribute('moved', 'true');
        document.body.appendChild(this.wrapper);
      }
    });
  }

  private renderPersonaList = () => {
    if (this.selectedItems.length <= 0 || this.suggestionList.length <= 0) return;

    return (
      this.selectedItems.map(({ firstName = '', lastName = '', src, value }, idx) => (
        <div class={{
          'persona-picker': true,
          'is-last': idx === this.selectedItems.length - 1,
        }}>
          <div>
            <nv-avatar
              src={src}
              text={firstName}
              size='small'
            />
            <nv-label class='nv-px-1'>{ `${firstName} ${lastName}` }</nv-label>
            <div
              onClick={(e) => this.removeItem(e, value)} 
              class='btn-remove-persona'
            >
              <nv-icon>close</nv-icon>
            </div>
          </div>
        </div>
      ))
    );
  }

  private renderLoading = () => {
    return (
      <div
        class={{
          'loader-people-picker': true,
          'show': this.loading,
        }}
      >
        {
          this.loading && <nv-loader size='small' /> 
        }
      </div>
    );
  }

  private setInputFocus = () => {
    this.inputEl.focus();
  }

  //
  // Render
  //

  render() {

    return (
      <Host
        class={{
          'nv-people-picker': true,
          'nv-people-w-focus': this.inputFocus,
        }}
        onClick={this.setInputFocus}
      >
        <span
          class={{
            'label': true,
            'label-active': this.selectedItems.length > 0 || this.peopleName.length > 0 || this.inputFocus,
          }}
        >
            { this.label }
        </span>
        <div
          class={{
            'base-picker': true,
            'has-label': Boolean(this.label)
          }}
        >
          <div class='people-tokens'>
            { this.renderPersonaList() }
            <input
              ref={el => this.inputEl = el!}
              type='text'
              onFocus={this.onFocus}
              onBlur={this.onBlur}
              onInput={(event) => this.input(event)}
              onKeyDown={this.removeLastItem}
            />
          </div>
        </div>
        { this.renderLoading() }
        { this.renderSuggestList() }
      </Host>
    );
  }
}
