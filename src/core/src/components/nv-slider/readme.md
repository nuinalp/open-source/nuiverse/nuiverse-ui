# nv-slider



<!-- Auto Generated Below -->


## Properties

| Property    | Attribute    | Description                                                                           | Type                                                             | Default     |
| ----------- | ------------ | ------------------------------------------------------------------------------------- | ---------------------------------------------------------------- | ----------- |
| `color`     | `color`      | The color to use from your application color palette.                                 | `"danger" \| "primary" \| "secondary" \| "success" \| "warning"` | `'primary'` |
| `disabled`  | `disabled`   | If `true`, the user cannot interact with the range.                                   | `boolean`                                                        | `false`     |
| `dualKnobs` | `dual-knobs` | If `true`, show two knobs.                                                            | `boolean`                                                        | `false`     |
| `max`       | `max`        | Maximum integer value of the range.                                                   | `number`                                                         | `100`       |
| `min`       | `min`        | Minimum integer value of the range.                                                   | `number`                                                         | `0`         |
| `name`      | `name`       | The name of the control, which is submitted with the form data.                       | `string`                                                         | `''`        |
| `snaps`     | `snaps`      | If true, the knob snaps to tick marks evenly spaced based on the step property value. | `boolean`                                                        | `false`     |
| `step`      | `step`       | Specifies the value granularity.                                                      | `number`                                                         | `1`         |
| `value`     | `value`      | the value of the range.                                                               | `number \| { lower: number; upper: number; }`                    | `0`         |


## Events

| Event      | Description                                  | Type                                  |
| ---------- | -------------------------------------------- | ------------------------------------- |
| `nvChange` | Emitted when the value property has changed. | `CustomEvent<RangeChangeEventDetail>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
