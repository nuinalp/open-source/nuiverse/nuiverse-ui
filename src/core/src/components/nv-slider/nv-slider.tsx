/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  h,
  Host,
  Element,
  State,
  Prop,
  EventEmitter,
  Event,
  Watch,
  Fragment,
} from '@stencil/core';
import {
  KnobName,
  KnobDetail,
  RangeValue,
  RangeChangeEventDetail,
  Color,
} from '../../interface';

@Component({
  tag: 'nv-slider',
  shadow: false,
})
export class NvSlider {
  public noUpdate: boolean = false;
  // private noUpdate = false;
  public rect!: ClientRect;

  /**
   * Reference to the host element
   */
  @Element() el!: HTMLNvSliderElement;

  //
  // State
  //

  @State() public pressedKnob: KnobName;
  @State() public knobA: KnobDetail = { value: 0, posX: 0 };
  @State() public knobB: KnobDetail = { value: 0, posX: 0 };

  /**
   * Used to animate knobs and bars on the click event
   */
  @State() fixPosition: boolean = false;

  //
  // Props
  //

  /**
   * The name of the control, which is submitted with the form data.
   */
  @Prop() name: string = '';

  /**
   * If `true`, show two knobs.
   */
  @Prop() dualKnobs: boolean = false;

  /**
   * Minimum integer value of the range.
   */
  @Prop() min: number = 0;

  /**
   * Maximum integer value of the range.
   */
  @Prop() max: number = 100;

  /**
   * If true, the knob snaps to tick marks evenly spaced based on the step property value.
   */
  @Prop() snaps: boolean = false;

  /**
   * the value of the range.
   */
  @Prop({ mutable: true }) value: RangeValue = 0;
  @Watch('value')
  handleValue() {
    if (!this.disabled) {
      if (!this.noUpdate) {
        this.updateRatio();
      }
    }
  }

  /**
   * Specifies the value granularity.
   */
  @Prop() step: number = 1;


  /**
   * If `true`, the user cannot interact with the range.
   */
  @Prop() disabled: boolean = false;

  /**
   * The color to use from your application color palette.
   */
  @Prop() color: Color = 'primary';

  //
  // Events
  //

  /**
   * Emitted when the value property has changed.
   */
  @Event() nvChange!: EventEmitter<RangeChangeEventDetail>;

  //
  // Lifecycles
  //
  componentDidLoad() {
    this.updateRatio();
  }

  //
  // Mouse and Touch events
  //

  private onStart = (evt: MouseEvent | TouchEvent) => {
    const slider = this.el;
    this.rect = slider.getBoundingClientRect();
    const mouseEvt = evt as MouseEvent;
    const touchEvt = evt as TouchEvent;
    this.fixPosition = true;

    if (!this.dualKnobs) {
      this.pressedKnob = 'A';
    }

    let currentX: number;

     /**
      * change position for the left knob and the bar
      */
    if (evt.type === 'touchstart') {
      currentX =
      touchEvt.targetTouches[0].clientX - this.rect.left;
    } else {
      currentX = mouseEvt.clientX - this.rect.left;
    }

    // Prevent error on the position
    if (Math.sign(currentX) === -1) {
      currentX = 0;
    } else if (currentX > slider.clientWidth) {
      currentX = slider.clientWidth;
    }

    // Get the nearest knob
    const percentX = (currentX / slider.clientWidth) * 100;

    this.pressedKnob =
      !this.dualKnobs ||
      Math.abs(this.knobA.posX - percentX) < Math.abs(this.knobB.posX - percentX)
        ? 'A'
        : 'B';
    this.updatePosition(currentX);

    this.addEvent();
  }

  private onEnd = () => {
    if (!this.pressedKnob) {
      return;
    }
    this.pressedKnob = undefined;
    this.removeEvent();
  }

  private addEvent(): void {
    window.addEventListener('mouseup', this.onEnd);
    window.addEventListener('touchend', this.onEnd);
    window.addEventListener('mousemove', this.onMove);
    window.addEventListener('touchmove', this.onMove);
  }

  private removeEvent(): void {
    window.removeEventListener('mousemove', this.onMove);
    window.removeEventListener('touchmove', this.onMove);
    window.removeEventListener('mouseup', this.onEnd);
    window.removeEventListener('touchend', this.onEnd);
  }

  private onMove = (evt: MouseEvent | TouchEvent) => {
    if (this.fixPosition) {
      this.fixPosition = false;
    }

    const slider = this.el;
    const mouseEvt = evt as MouseEvent;
    const touchEvt = evt as TouchEvent;
    let currentX: number;

     /**
      * change position for left knob and the bar
      */
    if (evt.type === 'touchmove') {
      currentX =
      touchEvt.targetTouches[0].clientX - slider.getBoundingClientRect().left;
    } else {
      currentX = mouseEvt.clientX - slider.getBoundingClientRect().left;
    }

    // Prevent error on the position
    if (Math.sign(currentX) === -1) {
      currentX = 0;
    } else if (currentX > slider.clientWidth) {
      currentX = slider.clientWidth;
    }

    this.updatePosition(currentX);
  }

  private updatePosition(currentX: number): void {
    const slider = this.el;
    let posX = (currentX / slider.clientWidth) * 100;
    const lengthPerStep = 100 / ((this.max - this.min) / this.step);
    const steps = Math.round(posX / lengthPerStep);
    let value = steps * lengthPerStep * (this.max - this.min) * 0.01 + this.min;


    if (value > this.max) {
      value = this.max;
    }

    if (this.step > 1) {
      posX = steps * lengthPerStep;

      if (posX > 100) {
        posX = 100;
      }
    }

    // Store old positions
    const old = { A: this.knobA, B: this.knobB };
    // Update position and value from the pressed knob
    this.pressedKnob === 'A' ? this.knobA = { value, posX } : this.knobB = { value, posX };

    const emmitValue: RangeValue = this.dualKnobs ? {
      lower: Math.min(this.knobA.value, this.knobB.value),
      upper: Math.max(this.knobA.value, this.knobB.value),
    } : this.knobA.value;

    if ((old.A.posX !== this.knobA.posX || old.B.posX !== this.knobB.posX) ||
    (old.A.value !== this.knobA.value || old.B.value !== this.knobB.value)) {
      this.updateValue();
      this.nvChange.emit({ value: emmitValue });
    }
  }

  private updateValue(): void {
    this.noUpdate = true;

    this.value = this.dualKnobs ? {
      lower: Math.min(this.knobA.value, this.knobB.value),
      upper: Math.max(this.knobA.value, this.knobB.value),
    } : this.knobA.value;

    this.noUpdate = false;
  }

  private updateRatio(): void {
    const value         = this.getValue();
    const lengthPerStep = 100 / ((this.max - this.min) / this.step);
    this.fixPosition = true;

    if (this.dualKnobs && typeof value === 'object') {
      let posKnobA  = (value.lower - this.min) / (this.max - this.min) * 100;
      let posKnobB  = (value.upper - this.min) / (this.max - this.min) * 100;
      const stepsA  = Math.round(posKnobA / lengthPerStep);
      const stepsB  = Math.round(posKnobB / lengthPerStep);
      let valueA    = stepsA * lengthPerStep * (this.max - this.min) * 0.01 + this.min;
      let valueB    = stepsB * lengthPerStep * (this.max - this.min) * 0.01 + this.min;

      if (valueA > this.max) {
        valueA = this.max;
      }

      if (valueB > this.max) {
        valueB = this.max;
      }

      if (this.step > 1) {
        posKnobA = stepsA * lengthPerStep;
        posKnobB = stepsB * lengthPerStep;

        if (posKnobA > 100) {
          posKnobA = 100;
        }

        if (posKnobB > 100) {
          posKnobB = 100;
        }
      }

      this.knobA = { value: valueA, posX: posKnobA };
      this.knobB = { value: valueB, posX: posKnobB };
      this.updateValue();
    }

    if (value && typeof value === 'number') {
      let posKnobA  = (value - this.min) / (this.max - this.min) * 100;
      const stepsA  = Math.round(posKnobA / lengthPerStep);
      let valueA    = stepsA * lengthPerStep * (this.max - this.min) * 0.01 + this.min;
      if (valueA > this.max) {
        valueA = this.max;
      }

      if (this.step > 1) {
        posKnobA = stepsA * lengthPerStep;

        if (posKnobA > 100) {
          posKnobA = 100;
        }
      }

      this.knobA = { value: valueA, posX: posKnobA };
      this.updateValue();
    }
  }

  private getValue(): RangeValue {
    const value = this.value || 0;
    if (this.dualKnobs) {
      if (this.dualKnobs && typeof this.value === 'object') {
        return value;
      }

      return {
        lower: 0,
        upper: value as number,
      };
    }

    return value;
  }

  private makeKnob({
    disabled,
    min,
    max,
    knob,
    value,
    pressed,
  }: any): HTMLDivElement {
    let snap: HTMLDivElement | null = null;

    if (this.snaps) {
      snap = (
        <div class={{
          'nv-slider-snap': true,
        }}>
          { knob === 'A' ? this.knobA.value : this.knobB.value }
        </div>
      );
    }

    return (
      <div
        class={{
          'nv-slider-knob-handle': true,
          'nv-slider-knob-a': knob === 'A',
          'nv-slider-knob-b': knob === 'B',
          'nv-slider-knob-pressed': pressed,
          'nv-slider-knob-min': value === min,
          'nv-slider-knob-max': value === max,
        }}
        style={{ left: `${ value }%` }}
        role='slider'
        tabindex={ disabled ? -1 : 0}
        aria-valuemin={ min }
        aria-valuemax={ max }
        aria-disabled={ disabled  ? 'true' : null}
        aria-valuenow={ value }
      >
        { snap }
        <div class='nv-slider-knob'/>
      </div>
    );
  }

  private makeActivebar(): HTMLDivElement {
    const knobA = this.knobA.posX;
    const knobB = this.knobB.posX;

    if (this.dualKnobs) {
      const diffWidth = this.pressedKnob ? '60px' : '30px';
      const diffLeft = this.pressedKnob ? '30px' : '15px';
      const width = `calc(${Math.abs(knobA) < Math.abs(knobB) ?
        `${knobB}% - ${knobA}%` : `${knobA}% - ${knobB}%`} - ${diffWidth})`;
      return (
        <div
          class='nv-slider-bar-active'
          style={{
            width,
            left: Math.abs(knobA) < Math.abs(knobB) ?
            `calc(${ knobA }% + ${diffLeft})` : `calc(${ knobB }% + ${diffLeft})`,
          }}
        >
        </div>
      );
    }

    return (
      <div
        class='nv-slider-bar-active'
        style={{ width: `calc(${ knobA }% - 15px)` }}
      >
      </div>
    );
  }

  private makeSlideBar(): HTMLDivElement {
    if (this.dualKnobs) {
      const knobA = this.knobA.posX;
      const knobB = this.knobB.posX;
      return (
        <Fragment>
          <div
            class='nv-slider-bar'
            style={{
              width: `calc(${knobA}% - 15px)`,
              left: this.pressedKnob === 'A' ? '-15px' : '0px',
            }}
            role='presentation'
          ></div>
          <div
            class='nv-slider-bar'
            style={{
              width: `calc(${100 - knobB}% - 15px)`,
              left: 'unset',
              right: '0px',
            }}
            role='presentation'
          ></div>
        </Fragment>
      );
    }
    return (
      <div
        class='nv-slider-bar'
        style={{ width: `calc(${100 - this.knobA.posX}% - 15px)` }}
        role='presentation'
      ></div>
    );
  }

  private makeTicks(): HTMLDivElement | null {
    if (this.step <= 1) {
      return null;
    }

    const step = this.step;
    const ticks:HTMLDivElement[] = [];
    for (let value = this.min; value <= this.max; value += step) {
      const percentX = ((value - this.min) / (this.max - this.min) * 100);
      const tick = <div
        class={{
          'nv-slider-tick': true,
          'nv-slider-tick-active': this.knobA.posX >= value,
        }}
        style={{ left: `${ percentX }%` }}
      />;
      ticks.push(tick);
    }
    if (!(this.step / 100 % 1 === 0)) {
      // Add the last tick
      ticks.push(
        <div
          class={{
            'nv-slider-tick': true,
            'nv-slider-tick-active': this.knobA.posX === 100,
          }}
          style={{ left: `100%` }}
          ></div>,
      );
    }
    return (
      <div class='nv-slider-ticks'>
        { ticks }
      </div>
    );
  }

  render() {
    const disabled = this.disabled;
    const min = this.min;
    const max = this.max;
    const knobA = this.makeKnob({
      disabled,
      min,
      max,
      knob: 'A',
      pressed: this.pressedKnob === 'A',
      value: this.knobA.posX,
    });
    const activeBar = this.makeActivebar();
    const slideBar = this.makeSlideBar();
    const ticks = this.makeTicks();
    let knobB: HTMLDivElement | null = null;

    if (this.dualKnobs) {
      knobB = this.makeKnob({
        disabled,
        min,
        max,
        knob: 'B',
        pressed: this.pressedKnob === 'B',
        value: this.knobB.posX,
      });
    }

    return (
      <Host
        class={{
          'nv-slider': true,
          'nv-slider-active': this.pressedKnob !== undefined,
          'nv-slider-salt-animation': this.fixPosition,
          'nv-slider-disabled': this.disabled,
          'nv-slider-with-snaps': this.snaps,
          'nv-slider-dual-knobs': this.dualKnobs,
        }}
        onMouseDown={ !this.disabled ? this.onStart : null }
        onTouchStart={ !this.disabled ? this.onStart : null }
      >
        <slot name='start'/>
        <div class='nv-slider-bars'>
          { slideBar }
          { activeBar }
          { ticks }
        </div>
        { knobA }
        { knobB }
        <slot name='end'/>
      </Host>
    );
  }
}
