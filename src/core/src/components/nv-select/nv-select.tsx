/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  h,
  Host,
  Prop,
  State,
  Method,
  Event,
  EventEmitter,
  Element,
  // Watch,
} from '@stencil/core';
import {
  InputChangeEventDetail,
  SelectedValues,
  SelectEventChangeDetail,
} from '../../interface';
import { INvChangeSelectWrapper } from '../nv-select-wrapper/interface';
import { nvInputChange } from './interface';

@Component({
  tag: 'nv-select',
  styleUrl: './style.scss',
  shadow: true,
})
export class NvSelect {
  //
  // Reference to the host element.
  //

  @Element() el!: HTMLNvSelectElement;
  wrapper!: HTMLNvSelectWrapperElement;
  options: HTMLNvSelectOptionElement[] = [];
  selectEl!: HTMLElement;

  //
  // State
  //

  @State() open: boolean = false;
  @State() selectedValues: SelectedValues[] = [];
  
  elWidth: number = 0;
  elHeight: number = 0;
  elLeft: number = 0;
  elBottom: number = 0;
  listAlignBottom: boolean = false;
  
  //
  // Props
  //

  /**
   * The text of the label for the select.
   */
  @Prop() label?: string | undefined;

  /**
   * The name of the input, which is submitted with the form data.
   */
  @Prop() name?: string | undefined;

  /**
   * The value of the select.
   */
  @Prop({ mutable: true }) value?: string | string[] | undefined | SelectedValues[];

  /**
   * If `true`, the user cannot interact with the select.
   */
  @Prop() disabled: boolean = false;

  /**
   * If `true`, the select can accept multiple values.
   */
  @Prop() multiple: boolean = false;

  /**
   * If `true`, the select will have a naked style.
   */
  @Prop() naked: boolean = false;

  /**
   * If `true`, the select will have a search box to find by label attribute in nv-select-option items.
   */
  @Prop() filter: boolean = false;

  /**
   * If `true`, the loading will be display
   */
  @Prop({ mutable: true }) loading = false;

  /**
   * The text for the no results label in filter.
   */
  @Prop({ mutable: true }) noDataAvailableLabel: string = 'No data available';

  /**
   * Set the amount of time, in milliseconds, to wait to trigger the nvInputChange event after each keystroke.
   */
  @Prop() debounce = 0;

  /**
   * If `true`, You can define a custom selected content to nv-select, using the custom-selected slot.
   */
  @Prop() customSelectedValue = false;

  //
  // Lifecycles
  //
  componentDidLoad() {
    this.options = Array.from(this.el.querySelectorAll('nv-select-option'));
    this.wrapper.registerSelect(this.el);
    this.wrapper.registerOptions(this.options);

    if (this.value && typeof this.value === 'string') {
      this.initSelect().then(() => {
        this.options.forEach((option: HTMLNvSelectOptionElement): void => {
          if (option.value === this.value) {
            option.selected = true;
            this.getValues();
          }
        });
      });
    }

    const nvSelectEl = this.el.shadowRoot?.querySelector<HTMLDivElement>('.nv-select');

    if (nvSelectEl) {
      this.selectEl = nvSelectEl;
    }
    
    const selectEl = nvSelectEl?.querySelector('.nv-select-el');
    selectEl?.addEventListener('transitionend', this.watchEvents.bind(this))
    const { height } = window.getComputedStyle(this.el);
    this.elHeight = parseInt(height, 10);
    this.fixListPos();
  }

  componentDidUpdate() {
    this.options = Array.from(this.el.querySelectorAll('nv-select-option'));
    this.wrapper.registerOptions(this.options);
  }

  //
  // Events
  //

  /**
   * Emitted when the value has changed.
   */
  @Event({
    eventName: 'nvChange',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) nvChange!: EventEmitter<SelectEventChangeDetail>;

  /**
   * Emmited when the nvInputChange has changed
   */
  @Event() nvInputChange!: EventEmitter<nvInputChange>;

  //
  // Methods
  //

  /**
   * Close or open the select.
   */
  @Method()
  async toggle() {
    if (!this.disabled) {
      if (this.open) {
        this.fixListPos();
        this.moveWrapper();
        this.removeEvent();
        if (this.filter) this.wrapper.clearSearchBox();
        this.open = false;
        this.el.classList.remove('nv-select-opening')
        return;
      }

      this.open = true;
      this.fixListPos();
      this.moveWrapper();
      this.addEvent();

      if (this.filter) this.wrapper.setFocus();

    }
  }

  private onClick = (e: Event): void => {
    e.preventDefault();
    e.stopImmediatePropagation();

    if (!this.open) {
      this.toggle();
    }
  }

  private addEvent (): void {
    if (!this.disabled) {
      window.addEventListener('keydown', this.onKeyDown);
      window.addEventListener('mousedown', this.onStart);
    }
  }

  private removeEvent(): void {
    if (!this.disabled) {
      window.removeEventListener('keydown', this.onKeyDown);
      window.removeEventListener('mousedown', this.onStart);
    }
  }

  private onKeyDown = (e: KeyboardEvent): void => {
    if (e.key === 'Escape') {
      this.toggle();
    }
  }

  private onStart = (e: MouseEvent): void  => {
    const el = e.target as HTMLElement;
    if (!el.className.match(/nv-select-items|nv-select-item|nv-search-box/)) {
      this.toggle();
    }
  }

  private getValues(): void {
    const selectedValues: SelectedValues[] = [];
    const value: string[] = [];

    this.options.forEach((option) => {
      if (option.selected) {
        selectedValues.push({
          label: option.label ? option.label : option.innerText,
          value: option.value || undefined,
        });
        value.push(option.value || undefined);
      }
    });

    this.selectedValues = selectedValues;
    this.value = value;
    this.nvChange.emit({ value });
  }

  private makeNativeSelect() {
    const options: HTMLOptionElement[] = [];
    if (this.name) {
      this.selectedValues.forEach((values) => {
        options.push(<option value={ values.value } selected={ true }/>);
      });

      return (
        <select name={ this.name } hidden disabled={ this.disabled }>
          { options }
        </select>
      );
    }
  }

  private async initSelect(): Promise<void> {
    // wait for all selectOptions to be ready.
    await Promise.all(this.options.map(item => item.componentOnReady()));
  }

  private getValue(): string {
    const selectedValues = this.selectedValues;
    let value: string = '';
    if (selectedValues.length > 0) {
      value = [].map.call(selectedValues, (content: SelectedValues) => {
        return content.label;
      }).join(', ');
    }
    return value;
  }

  private fixListPos(): void {
    // get bottom position to display the select-option in the correct position.
    const { bottom, left } = this.el.getBoundingClientRect();
    const { width } = window.getComputedStyle(this.el);
    const winHeight = window.innerHeight;

    this.elLeft = left;
    this.elBottom = bottom;
    this.elWidth = parseInt(width, 10);

    if ((bottom + this.elHeight) >= winHeight) {
      this.listAlignBottom = true;
    } else {
      this.listAlignBottom = false;
    }
  }

  private moveWrapper(): void {
    window.requestAnimationFrame(() => {
      const { top, left } = this.el.getBoundingClientRect();
      const { width, height } = window.getComputedStyle(this.el);
      const { pageXOffset, pageYOffset } = window;
      
      if (this.open) {
        const items = this.wrapper.querySelectorAll('nv-select-option-wrapper');
        items.forEach((item) => {
          item.setAttribute('moving', 'true');
        });

        requestAnimationFrame(() => {
          items.forEach((item) => item.setAttribute('moving', 'false'));
        });

        this.wrapper.style.width = width;
        this.wrapper.style.height = height;
        this.wrapper.style.top = `${ top + pageYOffset }px`;
        this.wrapper.style.left = `${ left + pageXOffset }px`;
        this.wrapper.style.display = 'block';
        document.body.appendChild(this.wrapper);
        window.requestAnimationFrame(() => {
          setTimeout(() => (this.wrapper.classList.add('nv-select-items-opened')), 0);
        });
        return;
      }

      this.wrapper.classList.remove('nv-select-items-opened');

      window.requestAnimationFrame(() => {
        setTimeout(
          () => {
            const items = this.wrapper.querySelectorAll('nv-select-option-wrapper');
            
            items.forEach((item) => {
              item.setAttribute('moving', 'true');
            });

            requestAnimationFrame(() => {
              items.forEach((item) => item.setAttribute('moving', 'false'));
            });

            this.wrapper.style.width = 'unset';
            this.wrapper.style.height = 'unset';
            this.wrapper.style.top = 'unset';
            this.wrapper.style.left = 'unset';
            this.wrapper.style.display = 'none';

            this.el.shadowRoot?.querySelector('.nv-select-el')?.appendChild(this.wrapper);
          },
          0);
      });
    });
  }

  private handleNvInput = ({ detail: { value } }: CustomEvent<InputChangeEventDetail>): void => {    
    this.nvInputChange.emit({ value });
  }

  private handleNvChange = ({ detail: { value } }: CustomEvent<INvChangeSelectWrapper>): void => {
    console.info('handleNvChange ', value);
    if (Array.isArray(value)) {
      const formatedValues = [ ...(value).map(({ value }) => value) ].filter((value) => value !== undefined);
      this.nvChange.emit({ value: formatedValues });
      this.value = formatedValues;
      this.selectedValues = value;
    }
  }

  private watchEvents(evt: any): void {
    if (evt.type === 'transitionend') {
      if (this.el.classList.contains('nv-select-opening')) {
        this.el.classList.remove('nv-select-opening');
      }
    }
  }

  render() {
    const selectedValue = this.getValue();
    const nativeSelect = this.makeNativeSelect();

    return (
      <Host
        ref={ el => this.selectEl = el! }
        class={{
          'nv-select': true,
          'nv-select-opened': this.open,
          'nv-select-closed': !this.open,
          'nv-select-disabled': this.disabled,
          'nv-select-w-label': this.label !== undefined,
          'nv-select-naked': this.naked,
          'nv-select-w-value': this.selectedValues.length > 0,
        }}
      >
        <div class='nv-select-el' onClick={ this.onClick }>
          <span
            class={{
              'nv-select-label': true,
              'nv-select-label-active': this.selectedValues.length > 0,
            }}
          >{ this.label }</span>
          {
            this.customSelectedValue ? 
              <div class='nv-custom-selected-value'>
                <slot name="custom-selected-value" />
              </div>
            :
            <span class='nv-select-selected-value'>
              { selectedValue }
            </span>
          }

          {/* Select icon */}
          <svg width='30' height='30' viewBox='0 0 30 30' class='nv-select-icon'>
            <g transform='translate(0 -289.06)'>
              <path d='m30 289.06v30h-30z'/>
            </g>
          </svg>
          <nv-select-wrapper
            ref={el => this.wrapper = el!}
            onNvInputChange={this.handleNvInput}
            filter={this.filter}
            loading={this.loading}
            noDataAvailableLabel={this.noDataAvailableLabel}
            multiple={this.multiple}
            debounce={this.debounce}
            onNvChange={this.handleNvChange}
            listAlignBottom={this.listAlignBottom}
          />
          <div hidden>
            <slot />
          </div>
        </div>
        { nativeSelect }
      </Host>
    );
  }
}
