/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

export interface SelectEventChangeDetail {
  value: string | string[];
}

export interface SelectOptionSelectedDetail {
  option: HTMLNvSelectOptionElement;
}

export interface SelectChangeDetail {
  option: HTMLNvSelectOptionElement;
  multiple?: boolean;
}

export interface SelectedValues {
  value: string | undefined;
  label: string;
}
