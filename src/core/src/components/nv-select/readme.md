# nv-select



<!-- Auto Generated Below -->


## Properties

| Property               | Attribute                 | Description                                                                                               | Type                                                  | Default               |
| ---------------------- | ------------------------- | --------------------------------------------------------------------------------------------------------- | ----------------------------------------------------- | --------------------- |
| `customSelectedValue`  | `custom-selected-value`   | If `true`, You can define a custom selected content to nv-select, using the custom-selected slot.         | `boolean`                                             | `false`               |
| `debounce`             | `debounce`                | Set the amount of time, in milliseconds, to wait to trigger the nvInputChange event after each keystroke. | `number`                                              | `0`                   |
| `disabled`             | `disabled`                | If `true`, the user cannot interact with the select.                                                      | `boolean`                                             | `false`               |
| `filter`               | `filter`                  | If `true`, the select will have a search box to find by label attribute in nv-select-option items.        | `boolean`                                             | `false`               |
| `label`                | `label`                   | The text of the label for the select.                                                                     | `string \| undefined`                                 | `undefined`           |
| `loading`              | `loading`                 | If `true`, the loading will be display                                                                    | `boolean`                                             | `false`               |
| `multiple`             | `multiple`                | If `true`, the select can accept multiple values.                                                         | `boolean`                                             | `false`               |
| `naked`                | `naked`                   | If `true`, the select will have a naked style.                                                            | `boolean`                                             | `false`               |
| `name`                 | `name`                    | The name of the input, which is submitted with the form data.                                             | `string \| undefined`                                 | `undefined`           |
| `noDataAvailableLabel` | `no-data-available-label` | The text for the no results label in filter.                                                              | `string`                                              | `'No data available'` |
| `value`                | `value`                   | The value of the select.                                                                                  | `SelectedValues[] \| string \| string[] \| undefined` | `undefined`           |


## Events

| Event           | Description                                | Type                                   |
| --------------- | ------------------------------------------ | -------------------------------------- |
| `nvChange`      | Emitted when the value has changed.        | `CustomEvent<SelectEventChangeDetail>` |
| `nvInputChange` | Emmited when the nvInputChange has changed | `CustomEvent<nvInputChange>`           |


## Methods

### `toggle() => Promise<void>`

Close or open the select.

#### Returns

Type: `Promise<void>`




## Dependencies

### Depends on

- [nv-select-wrapper](../nv-select-wrapper)

### Graph
```mermaid
graph TD;
  nv-select --> nv-select-wrapper
  nv-select-wrapper --> nv-select-option-wrapper
  nv-select-wrapper --> nv-search-box
  nv-search-box --> nv-loader
  style nv-select fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
