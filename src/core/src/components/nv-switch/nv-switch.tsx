/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, Prop, h, Host, Event, EventEmitter, Watch } from '@stencil/core';
import { SwicthChangeEventDetail } from '../../interface';
@Component({
  tag: 'nv-switch',
  styleUrl: './style.scss',
  shadow: true,
})
export class NvSwitch {
  //
  // Props
  //

  /**
   * If `true`, the user cannot interact with the Swicth.
   */
  @Prop() disabled: boolean = false;

  /**
   * If `true`, the switch is checked.
   */
  @Prop({ mutable: true }) checked: boolean = false;

  /**
   * The value of the switch does not mean if it's checked or not, use the `checked`
   * property for that.
   *
   * The value of a switch is analogous to the value of a `<input type="checkbox">`,
   * it's only used when the switch participates in a native `<form>`.
   */
  @Prop() value?: string | null = 'on';

  //
  // Events
  //

  /**
   * Emitted when the value of the input is changed.
   */
  @Event() nvChange!: EventEmitter<SwicthChangeEventDetail>;

  /**
   * Emitted when the press the enter.
   */
  @Event() nvEnter!: EventEmitter<SwicthChangeEventDetail>;

  /**
   * Emitted when the input loses focus.
   */
  @Event() nvBlur!: EventEmitter<FocusEvent>;

  /**
   * Emitted when the input has focus.
   */
  @Event() nvFocus!: EventEmitter<FocusEvent>;

  //
  // Watchers
  //

  @Watch('checked')
  checkedChanged(isChecked: boolean) {
    this.nvChange.emit({
      checked: isChecked,
      value: this.value,
    });
  }

  //
  // Methods
  //

  private onFocus = (ev: FocusEvent): void => {
    this.nvFocus.emit(ev);
  }

  private onBlur = (ev: FocusEvent): void => {
    this.nvFocus.emit(ev);
  }

  private updateSwitch = (e: Event) => {
    e.preventDefault();
    if (!this.disabled) this.checked = !this.checked;
  }

  render() {
    return (
      <Host
        class={{
          'nv-switch': true,
          'nv-switch-disabled': this.disabled,
        }}
        onClick={ this.updateSwitch }
      >
        <label>
          <input
            type='checkbox'
            checked={ this.checked }
            disabled={ this.disabled }
            onFocus={this.onFocus}
            onBlur={this.onBlur}
          />
          <span class='nv-slider round'></span>
        </label>
      </Host>
    );
  }
}
