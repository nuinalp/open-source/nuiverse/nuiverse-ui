# nv-switch



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description                                                                                                                                                                                                                                           | Type                          | Default |
| ---------- | ---------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------- | ------- |
| `checked`  | `checked`  | If `true`, the switch is checked.                                                                                                                                                                                                                     | `boolean`                     | `false` |
| `disabled` | `disabled` | If `true`, the user cannot interact with the Swicth.                                                                                                                                                                                                  | `boolean`                     | `false` |
| `value`    | `value`    | The value of the switch does not mean if it's checked or not, use the `checked` property for that.  The value of a switch is analogous to the value of a `<input type="checkbox">`, it's only used when the switch participates in a native `<form>`. | `null \| string \| undefined` | `'on'`  |


## Events

| Event      | Description                                     | Type                                   |
| ---------- | ----------------------------------------------- | -------------------------------------- |
| `nvBlur`   | Emitted when the input loses focus.             | `CustomEvent<FocusEvent>`              |
| `nvChange` | Emitted when the value of the input is changed. | `CustomEvent<SwicthChangeEventDetail>` |
| `nvEnter`  | Emitted when the press the enter.               | `CustomEvent<SwicthChangeEventDetail>` |
| `nvFocus`  | Emitted when the input has focus.               | `CustomEvent<FocusEvent>`              |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
