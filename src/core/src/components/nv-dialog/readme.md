# nv-dialog



<!-- Auto Generated Below -->


## Methods

### `toggle() => Promise<void>`

Close or open the nv-dialog.

#### Returns

Type: `Promise<void>`




----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
