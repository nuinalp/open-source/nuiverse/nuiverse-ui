# nv-tab-button



<!-- Auto Generated Below -->


## Properties

| Property           | Attribute  | Description                                                                                                                              | Type      | Default     |
| ------------------ | ---------- | ---------------------------------------------------------------------------------------------------------------------------------------- | --------- | ----------- |
| `disabled`         | `disabled` | If `true`, the user cannot interact with the tab button.                                                                                 | `boolean` | `false`     |
| `selected`         | `selected` | The selected tab component                                                                                                               | `boolean` | `false`     |
| `tab` _(required)_ | `tab`      | A tab id must be provided for each `nv-tab`. It's used internally to reference the selected tab or by the router to switch between them. | `string`  | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
