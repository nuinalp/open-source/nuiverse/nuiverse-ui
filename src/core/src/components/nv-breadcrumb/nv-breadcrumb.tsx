/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Prop } from '@stencil/core';

@Component({
  tag: 'nv-breadcrumb',
  shadow: false,
})
export class NvBreadcrumb {
  /**
   * The href for the `a` element, this is used to add a
   * link for the Breadcrumb elements.
   */
  @Prop() href!: string;

  private makeItem(): HTMLElement | null {
    if (this.href) {
      return (
        <a class='nv-bc-link' href={this.href}>
          <slot />
        </a>
      );
    }

    return <slot />;
  }

  render() {
    const item = this.makeItem();
    const current: string | null = !this.href ? 'page' : null;
    return (
      <li class='nv-breadcrumb-item' aria-current={ current }>
        { item }
      </li>
    );
  }
}
