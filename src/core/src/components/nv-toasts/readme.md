# nv-toasts



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description                 | Type                                                                                              | Default          |
| ---------- | ---------- | --------------------------- | ------------------------------------------------------------------------------------------------- | ---------------- |
| `position` | `position` | The position of the toasts. | `"bottom-center" \| "bottom-left" \| "bottom-right" \| "top-center" \| "top-left" \| "top-right"` | `'bottom-right'` |


## Dependencies

### Used by

 - [nv-app](../nv-app)

### Graph
```mermaid
graph TD;
  nv-app --> nv-toasts
  style nv-toasts fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
