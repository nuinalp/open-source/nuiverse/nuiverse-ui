/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  h,
  Element,
  Prop,
  Host,
} from '@stencil/core';

@Component({
  tag: 'nv-toasts',
  styleUrl: './style.scss',
  shadow: true,
})
export class NvToasts {
  /**
   * Reference to the host element
   */
  @Element() el!: HTMLElement;

  /**
   * The position of the toasts.
   */
  @Prop() position: 'top-left' | 'top-right' | 'top-center' |
  'bottom-left' | 'bottom-right' | 'bottom-center' = 'bottom-right';

  render() {
    return (
      <Host>
        <div
          class={{
            'nv-toasts': true,
            'nv-toast-top-left': this.position === 'top-left' ? true : false,
            'nv-toast-top-right': this.position === 'top-right' ? true : false,
            'nv-toast-top-center': this.position === 'top-center' ? true : false,
            'nv-toast-bottom-left': this.position === 'bottom-left' ? true : false,
            'nv-toast-bottom-right': this.position === 'bottom-right' ? true : false,
            'nv-toast-bottom-center': this.position === 'bottom-center' ? true : false,
          }}
        >
          <slot />
        </div>
      </Host>
    );
  }
}
