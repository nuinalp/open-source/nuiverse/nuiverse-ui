# nv-tooltip



<!-- Auto Generated Below -->


## Properties

| Property            | Attribute  | Description                 | Type                                     | Default     |
| ------------------- | ---------- | --------------------------- | ---------------------------------------- | ----------- |
| `position`          | `position` | The position of the tooltip | `"bottom" \| "left" \| "right" \| "top"` | `'top'`     |
| `text` _(required)_ | `text`     | The text for this tooltip.  | `string`                                 | `undefined` |


## Dependencies

### Used by

 - [nv-code](../nv-code)
 - [nv-code-source](../nv-code-source)

### Graph
```mermaid
graph TD;
  nv-code --> nv-tooltip
  nv-code-source --> nv-tooltip
  style nv-tooltip fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
