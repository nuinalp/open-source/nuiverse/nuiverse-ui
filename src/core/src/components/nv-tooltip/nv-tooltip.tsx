/**
* Copyright (c) 2019 - 2021 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  Prop,
  h,
  Element,
  Host,
  State,
}                                 from '@stencil/core';
import { runNextAnimationFrame }  from '../../utils';
@Component({
  tag: 'nv-tooltip',
  styleUrl: './style.scss',
  shadow: true,
})
export class NvTooltip {
  id!: string;

  /**
   * Reference to the host element
   */
  @Element() el!: HTMLNvTooltipElement;

  //
  // State
  //

  @State() tooltipEl!: HTMLDivElement;
  @State() initialPosition: 'top' | 'bottom' | 'left' | 'right' = 'top';

  /**
   * The position of the tooltip
   */
  @Prop() position: 'top' | 'bottom' | 'left' | 'right' = 'top';

  /**
   * The text for this tooltip.
   */
  @Prop() text!: string;

  constructor() {
    this.id = `tooltip-id-${new Date().getTime()}`;
  }

  componentDidLoad() {
    this.initialPosition = this.position;
  }

  //
  // Computed
  //

  private mouseEnter = () => {
    runNextAnimationFrame(() => {
      this.tooltipEl.classList.remove('nv-tooltip-hidden');
      this.tooltipEl.classList.add('nv-tooltip-display');
    });
  }

  private mouseLeave = () => {
    runNextAnimationFrame(() => {
      this.tooltipEl.classList.add('nv-tooltip-hidden');
      this.tooltipEl.classList.remove('nv-tooltip-display');
    });
  }

  render() {
    return (
      <Host
        onMouseEnter={ this.mouseEnter }
        onMouseLeave={ this.mouseLeave }
        class={{
          'nv-tooltip-container': true,
        }}
      >
        <div>
          <slot />
        </div>
        <div
          ref={ el => this.tooltipEl = el! }
          class={{
            'nv-tooltip': true,
            'nv-tooltip-hidden': true,
            'nv-tooltip-top': this.initialPosition === 'top' ? true : false,
            'nv-tooltip-bottom': this.initialPosition === 'bottom' ? true : false,
            'nv-tooltip-left': this.initialPosition === 'left' ? true : false,
            'nv-tooltip-right': this.initialPosition === 'right' ? true : false,
            [this.id]: true,
          }}
        >
          { this.text }
        </div>
      </Host>
    );
  }
}
