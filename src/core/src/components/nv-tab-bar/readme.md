# nv-tab-bar



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                | Type                  | Default     |
| ------------- | -------------- | -------------------------- | --------------------- | ----------- |
| `selectedTab` | `selected-tab` | The selected tab component | `string \| undefined` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
