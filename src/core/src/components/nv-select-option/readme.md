# nv-select-option



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description                                                                 | Type                  | Default     |
| ---------- | ---------- | --------------------------------------------------------------------------- | --------------------- | ----------- |
| `disabled` | `disabled` | If `true`, the user cannot interact with the select option.                 | `boolean`             | `false`     |
| `hidden`   | `hidden`   | The text for the select label.                                              | `boolean`             | `false`     |
| `label`    | `label`    | The text for the select label.                                              | `string \| undefined` | `undefined` |
| `moving`   | `moving`   | If `true`, the unregisterSelectItem's nv-select-wrapper will be not called. | `boolean`             | `false`     |
| `selected` | `selected` | If `true`, the select option is selected.                                   | `boolean`             | `false`     |
| `value`    | `value`    | The text value of the option.                                               | `any`                 | `undefined` |


## Events

| Event                  | Description                           | Type                                      |
| ---------------------- | ------------------------------------- | ----------------------------------------- |
| `nvSelectOptionSelect` | Emitted when this option is selected. | `CustomEvent<SelectOptionSelectedDetail>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
