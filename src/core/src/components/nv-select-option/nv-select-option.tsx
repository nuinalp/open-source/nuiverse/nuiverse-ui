/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Host, Event, EventEmitter, Element, Prop } from '@stencil/core';
import { SelectOptionSelectedDetail } from '../../interface';

@Component({
  tag: 'nv-select-option',
  shadow: true,
})
export class NvSelectOption {
  //
  // Reference to the host element.
  //

  @Element() el!: HTMLNvSelectOptionElement;
  wrapper: HTMLNvSelectWrapperElement | null = null;
  select: HTMLNvSelectElement | null | undefined = null;
  elCopy!: any;

  //
  // Props
  //

  /**
   * The text value of the option.
   */
  @Prop({ mutable: true }) value: any;

  /**
   * If `true`, the select option is selected.
   */
  @Prop({ mutable: true }) selected: boolean = false;

  /**
   * If `true`, the user cannot interact with the select option.
   */
  @Prop({ mutable: true }) disabled: boolean = false;

  /**
   * The text for the select label.
   */
  @Prop({ mutable: true }) label: string | undefined;

  /**
   * The text for the select label.
   */
  @Prop({ mutable: true }) hidden: boolean = false;

  /**
   * If `true`, the unregisterSelectItem's nv-select-wrapper will be not called.
   * @intenal
   */
  @Prop({ mutable: true }) moving: boolean = false;

  //
  // Events
  //

  /**
   * Emitted when this option is selected.
   * @intenal
   */
  @Event({
    bubbles: true,
    composed: true,
  }) nvSelectOptionSelect!: EventEmitter<SelectOptionSelectedDetail>;

  //
  // Lifecycles
  //

  componentDidLoad() {
    this.select = this.el.closest('nv-select');
    this.wrapper = this.select ? this.select.shadowRoot?.querySelector<HTMLNvSelectWrapperElement>('nv-select-wrapper') || null : null;
  }

  disconnectedCallback() {
    if (this.wrapper) {
      this.wrapper.unregisterSelectItem(this.el)
    }
  }

  render() {
    return (
      <Host>
        <div>
          <slot />
        </div>
      </Host>
    );
  }
}
