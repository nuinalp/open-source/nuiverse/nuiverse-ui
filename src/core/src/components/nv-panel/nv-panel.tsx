// Packages
import { Component, h, Host, Method, State, Element, Event, EventEmitter } from '@stencil/core';

// Utils
import { runNextAnimationFrame } from '../../utils/runNextAnimationFrame';

// Types
import { INvPanelStateEventDetail } from './types';

@Component({
  tag: 'nv-panel',
  styleUrl: 'nv-panel.scss',
  shadow: true,
})
export class NvPanel {

  // --------------------------------------------
  // State
  // --------------------------------------------

  /**
   * Reference to the host element
   */
  @Element() el!: HTMLNvPanelElement;

  /**
   * Reference to the host element
   */
  container!: HTMLDivElement;

  /**
   * If `true`, the el will be displayed.
   */
  @State() show = false;


  // --------------------------------------------
  // Events
  // --------------------------------------------

  /**
   * Emitted when the panel is opened or closed.
   */
   @Event() nvState!: EventEmitter<INvPanelStateEventDetail>;

  // --------------------------------------------
  // Methods
  // --------------------------------------------

  public watchEvents(evt: any): void {
    const el = this.container;
    if (evt.type !== 'transitionend') return;

    // We only process 'opacity' because all states have this
    // change.
    if (evt.propertyName !== 'opacity') {
      return;
    }

    if (el.classList.contains('nv-panel-opening')) {
      this.nvState.emit({ state: 'opened' });
      el.classList.add('nv-panel-opened');
      el.classList.remove('nv-panel-opening');
    }  else if (el.classList.contains('nv-panel-flying')) {
      el.classList.remove('nv-panel-flying');
      this.toggle();
    }

    // go back to initial state after hiding
    if (el.classList.contains('nv-panel-hiding') || el.classList.contains('nv-panel-closing')) {
      el.classList.remove('nv-panel-opened');
      el.classList.remove('nv-panel-opening');
      el.classList.remove('nv-panel-closing');
      el.classList.remove('nv-panel-flying');
      el.classList.remove('nv-panel-hiding');
      el.classList.add('nv-panel-closed');
      this.el.style.display = 'none';
      this.nvState.emit({ state: 'closed' });
    }
  }

  private open() {
    const el = this.container;
    this.el.style.display = 'block';

    // Wait a frame once display is no longer "none", to establish the animation
    runNextAnimationFrame(() => {
      // no operation during flying and hiding
      if (el.classList.contains('nv-panel-flying') ||
          el.classList.contains('nv-panel-hiding')) {
        return;
      }

      el.classList.remove('nv-panel-closed');
      el.classList.remove('nv-panel-closing');
      el.classList.add('nv-panel-opening');
      this.show = true;
    });
  }

  /**
   * Close the panel
   */
   private close() {
    this.container?.classList.add('nv-panel-closing');
    this.show = false;
  }

  /**
   * Close or open the nv-panel.
   */
  @Method()
  async toggle() {
    if (this.show) return this.close();
    this.open();
  }

  // --------------------------------------------
  // Lifecycles
  // --------------------------------------------

  componentDidLoad() {
    this.container?.addEventListener('transitionend', this.watchEvents.bind(this));
  }

  render() {
    return ( 
      <Host style={{ display: 'none' }}>
        <div class="nv-panel-container" ref={el => this.container = el!}>
          <div
            class="lateral-bar"
          >
            <slot />
          </div>
        </div>
      </Host>
    );
  }
}
