import { newE2EPage } from '@stencil/core/testing';

describe('nv-panel', () => {
  it('renders', async () => {
    const page = await newE2EPage();

    await page.setContent('<nv-panel></nv-panel>');
    const element = await page.find('nv-panel');
    expect(element).toHaveClass('hydrated');
  });

  it('open and close', async () => {
    const page = await newE2EPage();
    await page.setContent('<nv-panel></nv-panel>');

    const element = await page.find('nv-panel');

    // Open the panel
    await element.callMethod('toggle');
    await page.waitForChanges();
    
    const container = await page.find('nv-panel >>> .nv-panel-container')
    expect(container).toHaveClass('nv-panel-opening');
    await page.waitForChanges();
    
    
    await page.waitForChanges();
    expect(await page.find('nv-panel >>> .nv-panel-container'))
    
    // Close panel
    await element.callMethod('toggle');
    await page.waitForChanges();

    expect((await page.find('nv-panel >>> .nv-panel-container'))).toHaveClass('nv-panel-closing');
  });
});
