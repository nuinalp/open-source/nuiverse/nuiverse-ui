export interface INvPanelStateEventDetail {
  state: 'opened' | 'closed';
}