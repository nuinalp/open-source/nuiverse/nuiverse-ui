# my-component



<!-- Auto Generated Below -->


## Events

| Event     | Description                                 | Type                                    |
| --------- | ------------------------------------------- | --------------------------------------- |
| `nvState` | Emitted when the panel is opened or closed. | `CustomEvent<INvPanelStateEventDetail>` |


## Methods

### `toggle() => Promise<void>`

Close or open the nv-panel.

#### Returns

Type: `Promise<void>`




----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
