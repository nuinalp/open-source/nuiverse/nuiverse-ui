import { newSpecPage } from '@stencil/core/testing';
import { NvPanel } from './nv-panel';

describe('nv-panel', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [NvPanel],
      html: '<nv-panel></nv-panel>',
    });
    expect(root).toEqualHtml(`
      <nv-panel style="display: none">
        <mock:shadow-root>
          <div class="nv-panel-container">
            <div class="lateral-bar">
              <slot></slot>
            </div>
          </div>
        </mock:shadow-root>
      </nv-panel>
    `);
  });

  it('renders with slot content', async () => {
    const { root } = await newSpecPage({
      components: [NvPanel],
      html: `<nv-panel><span>Hi</span></nv-panel>`,
    });
    expect(root).toEqualHtml(`
      <nv-panel style="display: none">
        <mock:shadow-root>
          <div class="nv-panel-container">
            <div class="lateral-bar">
              <slot></slot>
            </div>
          </div>
        </mock:shadow-root>
        <span>Hi</span>
      </nv-panel>
    `);
  });
});
