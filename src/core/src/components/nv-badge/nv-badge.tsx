/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, Prop, h, Host } from '@stencil/core';
import { Color }                    from '../../interface';

@Component({
  tag: 'nv-badge',
  shadow: false,
})
export class NvBadge {

  /**
   * The color for the badge.
   */
  @Prop() color?: Color;

  /**
   * if `true`, the badge has a pill style.
   */
  @Prop() pill: boolean = false;

  render() {
    return (
      <Host class={{
        'nv-badge': true,
        'nv-badge-pill': this.pill,
      }}
      >
        <slot />
      </Host>
    );
  }
}
