# nv-badge



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                            | Type                                                                          | Default     |
| -------- | --------- | -------------------------------------- | ----------------------------------------------------------------------------- | ----------- |
| `color`  | `color`   | The color for the badge.               | `"danger" \| "primary" \| "secondary" \| "success" \| "warning" \| undefined` | `undefined` |
| `pill`   | `pill`    | if `true`, the badge has a pill style. | `boolean`                                                                     | `false`     |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
