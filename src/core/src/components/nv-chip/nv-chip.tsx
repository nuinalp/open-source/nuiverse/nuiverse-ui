/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Element, Prop, Host } from '@stencil/core';
// import Ripple                          from '../../utils/ripple';

@Component({
  tag: 'nv-chip',
  shadow: false,
})
export class NvChip {
  /**
   * Reference to the host element
   */
  @Element() el!: HTMLNvChipElement;

  /**
   * If `true` the chip will have a rounded style
   */
  @Prop() rounded: boolean = false;

  //
  // Lifecyles
  //

  componentDidLoad() {
    // Ripple(this.el);
  }

  render() {
    return (
      <Host class={{
        'nv-chip': true,
        'nv-chip-rounded': this.rounded,
      }}>
        <div>
          <slot />
        </div>
      </Host>
    );
  }
}
