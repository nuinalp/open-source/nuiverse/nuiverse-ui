# nv-chip



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description                                  | Type      | Default |
| --------- | --------- | -------------------------------------------- | --------- | ------- |
| `rounded` | `rounded` | If `true` the chip will have a rounded style | `boolean` | `false` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
