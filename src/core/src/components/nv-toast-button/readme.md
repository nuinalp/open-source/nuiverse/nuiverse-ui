# nv-toast-button



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description               | Type      | Default |
| ---------- | ---------- | ------------------------- | --------- | ------- |
| `disabled` | `disabled` | The button enable status. | `boolean` | `false` |


## Dependencies

### Used by

 - [nv-toast](../nv-toast)

### Graph
```mermaid
graph TD;
  nv-toast --> nv-toast-button
  style nv-toast-button fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
