/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  h,
  Prop,
  Host,
} from '@stencil/core';

@Component({
  tag: 'nv-toast-button',
  styleUrl: './style.scss',
  shadow: true,
})
export class NvToastButton {
  public progress!: HTMLDivElement;

  /**
   * The button enable status.
   */
  @Prop() disabled: boolean = false;


  render() {
    return (
      <Host>
        <button
          class={{
            'nv-toast-button': true,
            'nv-toast-disabled': this.disabled,
          }}
        >
          <slot />
        </button>
      </Host>
    );
  }
}
