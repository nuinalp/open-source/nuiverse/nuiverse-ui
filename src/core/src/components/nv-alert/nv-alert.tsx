/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, Prop, h, Host } from '@stencil/core';
import { Color }                    from '../../interface';

@Component({
  tag: 'nv-alert',
  shadow: false,
})
export class NvAlert {
   /**
   * The color of the alert.
   */
  @Prop() color?: Color = 'primary';

  render() {
    return (
      <Host class={{ 'nv-alert': true }}>
        <slot />
      </Host>
    );
  }
}
