# nv-alert



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description             | Type                                                                          | Default     |
| -------- | --------- | ----------------------- | ----------------------------------------------------------------------------- | ----------- |
| `color`  | `color`   | The color of the alert. | `"danger" \| "primary" \| "secondary" \| "success" \| "warning" \| undefined` | `'primary'` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
