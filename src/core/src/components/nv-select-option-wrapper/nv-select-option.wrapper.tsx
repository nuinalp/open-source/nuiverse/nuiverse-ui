/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Host, Event, EventEmitter, Element, Prop } from '@stencil/core';
import { SelectOptionSelectedDetail, SelectChangeDetail } from '../../interface';
import { nvInputChange } from '../nv-select/interface';

@Component({
  tag: 'nv-select-option-wrapper',
  styleUrl: './style.scss',
  shadow: true,
})
export class NvSelectOptionWrapper {
  //
  // Reference to the host element.
  //

  @Element() el!: HTMLNvSelectOptionElement;
  wrapper!: HTMLNvSelectWrapperElement;
  select!: HTMLNvSelectElement;

  //
  // Props
  //

  /**
   * @internal
   */
  @Prop({ mutable: true }) value: any;

  /**
   * @internal
   */
  @Prop({ mutable: true }) selected: boolean = false;

  /**
   * @internal
   */
  @Prop({ mutable: true }) disabled: boolean = false;

  /**
   * @internal
   */
  @Prop({ mutable: true }) label?: string;

  /**
   * @internal
   */
  @Prop({ mutable: true }) hidden: boolean = false;

  /**
   * @intenal
   */
  @Prop({ mutable: true }) moving: boolean = false;

  //
  // Events
  //

  /**
   * Emitted when this option is selected.
   * @intenal
   */
  @Event({
    bubbles: true,
    composed: true,
  }) nvSelectOptionSelect!: EventEmitter<SelectOptionSelectedDetail>;

  //
  // Lifecycles
  //

  componentDidLoad() {
    const wrapper = this.el.closest('nv-select-wrapper');

    if (wrapper)  {
      this.wrapper = wrapper;
      if (wrapper.filter && this.label)
        this.wrapper.addEventListener('nvInputChange', this.handleNvInputChange.bind(this))
  
      this.wrapper.addEventListener('nvDidChange', this.handleNvDidChange.bind(this));
      this.wrapper.registerSelectItem(this.el);
    }
  }

  //
  // Methods
  //

  private onClick = (e: MouseEvent | TouchEvent): void => {
    e.preventDefault();
    e.stopImmediatePropagation();

    if (!this.wrapper.multiple) this.wrapper.toggle();

    if (this.selected) {
      this.selected = false;
      this.nvSelectOptionSelect.emit({
        option: this.el,
      });
      return;
    }
    this.selected = true;
    this.nvSelectOptionSelect.emit({
      option: this.el,
    });
  }

  public handleNvDidChange = (event: CustomEvent<SelectChangeDetail>) => {
    if (this.el !== event.detail.option && !event.detail.multiple) {
      this.selected = false;
    }
  }

  private handleNvInputChange({ detail: { value } }: CustomEvent<nvInputChange>) {
    if ((value as string).length > 0) {
      const find = (this.label?.toLocaleLowerCase() || '').match(new RegExp(value.toLocaleLowerCase(), 'gi'));
      if (!find) this.hidden = true;
      else this.hidden = false;
      return;
    } 
    this.hidden = false;
  }

  render() {
    return (
      <Host
        class={{
          'nv-select-item': true,
          'nv-select-item-selected': this.selected,
          'nv-select-item-disabled': this.disabled,
          'nv-select-item-hidden': this.hidden,
        }}
        onMouseDown={ !this.disabled ? this.onClick : null }
      >
        <div>
          <slot/>
        </div>
      </Host>
    );
  }
}
