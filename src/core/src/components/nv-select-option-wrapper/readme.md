# nv-select-option-wrapper



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type      | Default |
| -------- | --------- | ----------- | --------- | ------- |
| `moving` | `moving`  |             | `boolean` | `false` |


## Events

| Event                  | Description                           | Type                                      |
| ---------------------- | ------------------------------------- | ----------------------------------------- |
| `nvSelectOptionSelect` | Emitted when this option is selected. | `CustomEvent<SelectOptionSelectedDetail>` |


## Dependencies

### Used by

 - [nv-select-wrapper](../nv-select-wrapper)

### Graph
```mermaid
graph TD;
  nv-select-wrapper --> nv-select-option-wrapper
  style nv-select-option-wrapper fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
