/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, Prop, h, Host, Element } from '@stencil/core';
@Component({
  tag: 'nv-embed',
  shadow: false,
})
export class NvEmbed {
  //
  // Reference to the Host Element.
  //

  @Element() el!: HTMLNvEmbedElement;

  /**
   * Aspect ratios can be customized with modifier classes.
   */
  @Prop() aspectRatio?: '21by9' | '16by9' | '4by3' | '1by1' = '16by9';

  //
  // Lifecycles
  //

  componentDidLoad() {
    const el = this.el.firstElementChild;
    el?.classList.add('nv-embed-responsive-item');
  }

  render() {
    return (
      <Host class={{
        'nv-embed-responsive': true,
        'nv-embed-responsive-21by9': this.aspectRatio === '21by9',
        'nv-embed-responsive-16by9': this.aspectRatio === '16by9',
        'nv-embed-responsive-4by3': this.aspectRatio === '4by3',
        'nv-embed-responsive-1by1': this.aspectRatio === '1by1',
      }}>
        <slot></slot>
      </Host>
    );
  }
}
