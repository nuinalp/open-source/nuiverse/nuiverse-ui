# nv-embed



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                                            | Type                                                  | Default   |
| ------------- | -------------- | ------------------------------------------------------ | ----------------------------------------------------- | --------- |
| `aspectRatio` | `aspect-ratio` | Aspect ratios can be customized with modifier classes. | `"16by9" \| "1by1" \| "21by9" \| "4by3" \| undefined` | `'16by9'` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
