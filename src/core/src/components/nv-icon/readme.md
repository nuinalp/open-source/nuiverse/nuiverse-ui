# nv-icon



<!-- Auto Generated Below -->


## Dependencies

### Used by

 - [nv-date-picker](../nv-date-picker)
 - [nv-people-picker](../nv-people-picker)

### Graph
```mermaid
graph TD;
  nv-date-picker --> nv-icon
  nv-people-picker --> nv-icon
  style nv-icon fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
