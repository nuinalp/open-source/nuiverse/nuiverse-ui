/**
* Copyright (c) 2020 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Host, Element, State, Prop } from '@stencil/core';

@Component({
  tag: 'nv-list-column',
  shadow: true,
  styleUrl: './style.scss',
})
export class NvListColumn {
  /**
   * Reference to the host element
   */
  @Element() el!: HTMLNvListColumnElement;

  //
  // State
  //

  @State() flex = false;

  //
  // Props
  //

  /**
   * If `true`, the column width will the content's width.
   */
  @Prop() autoSize: boolean = false;


  //
  // Lifecycles
  //

  componentWillLoad() {
    if (this.el.style.width) {
      this.flex = true;
    }
  }

  render() {
    return (
      <Host
        class={{
          'nv-details-list-column': true,
          'nv-details-list-auto-size': this.autoSize,
        }}
      >
        <div>
          <slot />
        </div>
      </Host>
    );
  }
}
