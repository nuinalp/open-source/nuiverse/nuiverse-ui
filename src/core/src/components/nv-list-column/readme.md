# nv-list-column



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute   | Description                                           | Type      | Default |
| ---------- | ----------- | ----------------------------------------------------- | --------- | ------- |
| `autoSize` | `auto-size` | If `true`, the column width will the content's width. | `boolean` | `false` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
