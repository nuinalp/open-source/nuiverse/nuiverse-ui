# nv-code-source



<!-- Auto Generated Below -->


## Properties

| Property    | Attribute    | Description                                | Type                  | Default     |
| ----------- | ------------ | ------------------------------------------ | --------------------- | ----------- |
| `copyLabel` | `copy-label` | The label for the show source button.      | `string \| undefined` | `undefined` |
| `language`  | `language`   | The name of the language, used by Prismjs. | `string \| undefined` | `undefined` |


## Dependencies

### Depends on

- [nv-tooltip](../nv-tooltip)
- [nv-button](../nv-button)

### Graph
```mermaid
graph TD;
  nv-code-source --> nv-tooltip
  nv-code-source --> nv-button
  style nv-code-source fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
