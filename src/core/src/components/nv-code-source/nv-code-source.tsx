/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Host, Prop, State } from '@stencil/core';
import Prism from 'prismjs';
import Copy from 'clipboard-copy';

@Component({
  tag: 'nv-code-source',
  shadow: false,
})
export class NvCodeSource {

  @State() code!: HTMLElement;
  @State() content!: string;

  //
  // Props
  //

  /**
   * The name of the language, used by Prismjs.
   */
  @Prop() language: string | undefined;

  /**
   * The label for the show source button.
   */
  @Prop() copyLabel: string | undefined;

  componentDidLoad() {
    if (this.language &&
      (this.language === 'javascript' ||
      this.language === 'css' ||
      this.language === 'html') && this.code.textContent) {
      this.content = this.code.textContent;
      Prism.highlightElement(this.code);
      return;
    }

    // Load other languages
    if (this.language) {
      const script = document.createElement('script');
      script.onload = async () => {
        if (this.language === 'typescript') {
          script.setAttribute('nv-code-prism-loaded', this.language);
        }
        Prism.highlightElement(this.code);
      };

      script.src = `https://unpkg.com/prismjs@latest/components/prism-${this.language}.min.js`;
      script.setAttribute('nv-code-prism', this.language);
      script.defer = true;

      document.head.appendChild(script);
    }
  }

  private onClick = () => {
    Copy(this.content);
  }

  render() {
    return (
      <Host class={{
        'nv-code-source': true,
      }}>
        <div class='nv-btn-copy-container'>
          <nv-tooltip
            position='top'
            text={ this.copyLabel !== undefined ? this.copyLabel : 'Copy code' }>
            <nv-button class='nv-code-btn nv-code-btn-copy' text onClick={ this.onClick }>
              <svg width='30' height='30' viewBox='0 0 30 30'>
                <g transform='translate(0 -289.06)'>
                  <path
                    transform='translate(0 289.06)'
                    // tslint:disable-next-line: max-line-length
                    d='m9.4805 3-6.4805 6.5254v11.809c0 0.92333 0.73905 1.666 1.6562 1.666h5.3438v-10.299l7.0605-7.1113 0.58789-0.58984h0.25195v-0.33398c0-0.92333-0.73905-1.666-1.6562-1.666zm9 4-6.4805 6.5254v11.809c0 0.92333 0.73905 1.666 1.6562 1.666h11.588c0.9172 0 1.6562-0.74269 1.6562-1.666v-16.668c0-0.92333-0.73905-1.666-1.6562-1.666z'/>
                </g>
              </svg>
            </nv-button>
          </nv-tooltip>
        </div>
        <pre class={{
          'nv-code-source': true,
          [`language-${this.language}`]: this.language !== undefined,
        }}>
          <code ref={ el => this.code = el! }>
            <slot></slot>
          </code>
        </pre>
      </Host>
    );
  }
}
