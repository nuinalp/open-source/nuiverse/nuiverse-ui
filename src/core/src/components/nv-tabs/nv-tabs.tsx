/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  h,
  Host,
  Element,
  State,
  Method,
  Event,
  EventEmitter,
  Listen,
} from '@stencil/core';
import {
  NvTabButtonClickEventDetail,
} from './nv-tab-interface';

@Component({
  tag: 'nv-tabs',
  styleUrl: './style.scss',
  shadow: true,
})
export class NvTabs {
  /**
   * If true, the tab component is completely loaded and can be the initial select tab
   */
  private start: boolean = false;
  public leavingTab?: HTMLNvTabElement;

  /**
   * Reference to the host element
   */
  @Element() el!: HTMLNvTabsElement;

  // -------------------------------------------------
  // State
  // -------------------------------------------------

  @State() tabs: HTMLNvTabElement[] = [];
  @State() selectedTab?: HTMLNvTabElement;
  @State() previousSelectedTab?: HTMLNvTabElement;

  // -------------------------------------------------
  // Events
  // -------------------------------------------------

  /**
   * Emitted when the navigation will load a component.
   * @internal
   */
  @Event() nvNavWillLoad!: EventEmitter<void>;

  /**
   * Emitted when the navigation is about to transition to a new component.
   * @intenal
   */
  @Event({ bubbles: false }) nvTabsWillChange!: EventEmitter<{tab: string}>;

  /**
   * Emitted when the navigation has finished transitioning to a new component.
   */
  @Event({ bubbles: false }) nvTabsDidChange!: EventEmitter<{tab: string}>;

  @Listen('nvPrevTabDidChange')
  handleNvPrevTabDidChange() {
    this.selectedTab?.setActive();
  }

  // -------------------------------------------------
  // Lifecycles
  // -------------------------------------------------

  componentWillLoad() {
    requestAnimationFrame(() => {
      this.tabs = Array.from(this.el.querySelectorAll('nv-tab'));
      this.initSelect().then(() => {
        this.nvNavWillLoad.emit();
        // this.componentWillUpdate();
      });
    })
  }

  disconnectedCallback() {
    this.tabs.length = 0;
    this.selectedTab = this.leavingTab = undefined;
  }

  componentWillUpdate() {
    const tabBar = this.el.querySelector('nv-tab-bar');
    if (tabBar) {
      const tab = this.selectedTab ? this.selectedTab.tab : undefined;
      tabBar.selectedTab = tab;
      this.selectedTab?.setActive();
    }
  }

  //
  // Watchs
  //

  @Listen('nvTabsWillChange')
  protected handleNvTabsWillChange() {
    if (!this.previousSelectedTab || this.previousSelectedTab.tab === this.selectedTab?.tab) {
      return;
    }
    const topBar = this.el.querySelector<HTMLNvTabBarElement>('nv-tab-bar');
    if (topBar && this.selectedTab) {
      topBar.setAttribute('selected-tab', this.selectedTab.tab);
    }
    this.previousSelectedTab.removeActive().then(() => {
      this.previousSelectedTab = undefined;
    });
  }

  // -------------------------------------------------
  // Methods
  // -------------------------------------------------

  private onTabClicked = (ev: CustomEvent<NvTabButtonClickEventDetail>) => {
    const { tab } = ev.detail;
    const selectedTab = this.tabs.find(t => t.tab === tab);
    if (selectedTab) {
      this.select(selectedTab);
    }
  }

  /**
   * Select a tab by the value of its `tab` property or an element reference.
   *
   * @param tab The tab instance to select.
   * If passed a string, it should be the value of the tab's `tab` property.
   */
  @Method()
  async select(tab: string | HTMLNvTabElement): Promise<boolean> {
    const selectedTab = await this.getTab(tab);
    if (this.previousSelectedTab && (this.previousSelectedTab.tab === selectedTab?.tab)) {
      return false;
    }
    // Set current tab for the hidding.
    this.previousSelectedTab = this.selectedTab;

    if (selectedTab) {
      await this.setActive(selectedTab);
    }

    if (!this.start) {
      selectedTab?.setActive();
      this.start = true;
    }
    return true;
  }

  /**
   * Get a specific tab by the value of its `tab` property or an element reference.
   *
   * @param tab The tab instance to select.
   * If passed a string, it should be the value of the tab's `tab` property.
   * @intenal
   */
  @Method()
  async getTab(tab: string | HTMLNvTabElement): Promise<HTMLNvTabElement | null> {
    if (typeof tab !== 'string') return tab;

    const tabEl = this.tabs.find(t => t.tab === tab) || this.el.querySelector<HTMLNvTabElement>(`nv-tab[tab="${tab}"]`);

    if (!tabEl) {
      console.error(`tab with id: "${tabEl}" does not exist`);
    }
    return tabEl;
  }

  private setActive(selectedTab: HTMLNvTabElement) {
    if (selectedTab) {
      this.leavingTab = this.selectedTab;
      this.selectedTab = selectedTab;
      this.nvTabsWillChange.emit({ tab: selectedTab.tab });
    };
  }

  private async initSelect(): Promise<void> {
    return new Promise((resolve) => {
      requestAnimationFrame(async () => {
        // wait for all tabs to be ready
        await Promise.all(this.tabs.map(tab => tab.componentOnReady()));
    
        await this.select(this.tabs[0]);

        resolve();
      });
    })
  }
  
  /**
   * Add the tab in `this.tabs`, to select and show.
   * @param tab - The id of tab or the nv-tab html element.
   */
  @Method()
  async registerTab(tab: HTMLNvTabElement) {
    if (!this.tabs.includes(tab)) this.tabs.push(tab);

    if (tab?.active && !this.selectedTab) {
      this.select(tab);
      tab.setActive();
    }
  }

  render() {
    return (
      <Host
        onNvTabButtonClick={this.onTabClicked}
      >
        <slot name='top' />
        <slot />
        <slot name='bottom' />
      </Host>
    );
  }
}
