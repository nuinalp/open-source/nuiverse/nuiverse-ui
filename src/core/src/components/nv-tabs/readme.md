# nv-tabs



<!-- Auto Generated Below -->


## Events

| Event              | Description                                                                | Type                            |
| ------------------ | -------------------------------------------------------------------------- | ------------------------------- |
| `nvTabsDidChange`  | Emitted when the navigation has finished transitioning to a new component. | `CustomEvent<{ tab: string; }>` |
| `nvTabsWillChange` | Emitted when the navigation is about to transition to a new component.     | `CustomEvent<{ tab: string; }>` |


## Methods

### `getTab(tab: string | HTMLNvTabElement) => Promise<HTMLNvTabElement | null>`

Get a specific tab by the value of its `tab` property or an element reference.

#### Parameters

| Name  | Type                         | Description                                                                                         |
| ----- | ---------------------------- | --------------------------------------------------------------------------------------------------- |
| `tab` | `string \| HTMLNvTabElement` | The tab instance to select. If passed a string, it should be the value of the tab's `tab` property. |

#### Returns

Type: `Promise<HTMLNvTabElement | null>`



### `registerTab(tab: HTMLNvTabElement) => Promise<void>`

Add the tab in `this.tabs`, to select and show.

#### Parameters

| Name  | Type               | Description                                 |
| ----- | ------------------ | ------------------------------------------- |
| `tab` | `HTMLNvTabElement` | - The id of tab or the nv-tab html element. |

#### Returns

Type: `Promise<void>`



### `select(tab: string | HTMLNvTabElement) => Promise<boolean>`

Select a tab by the value of its `tab` property or an element reference.

#### Parameters

| Name  | Type                         | Description                                                                                         |
| ----- | ---------------------------- | --------------------------------------------------------------------------------------------------- |
| `tab` | `string \| HTMLNvTabElement` | The tab instance to select. If passed a string, it should be the value of the tab's `tab` property. |

#### Returns

Type: `Promise<boolean>`




----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
