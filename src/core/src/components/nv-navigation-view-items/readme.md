# nv-navigation-view-items



<!-- Auto Generated Below -->


## Properties

| Property     | Attribute     | Description                           | Type                  | Default     |
| ------------ | ------------- | ------------------------------------- | --------------------- | ----------- |
| `textHeader` | `text-header` | The text header for navigation items. | `string \| undefined` | `undefined` |


## Events

| Event                     | Description                                                                                   | Type                                   |
| ------------------------- | --------------------------------------------------------------------------------------------- | -------------------------------------- |
| `nvNavViewItemsDidChange` | Emitted when a item is selected, this is used to remove the active property from other items. | `CustomEvent<NvNavViewItemsDidChange>` |


## Dependencies

### Depends on

- [nv-navigation-view-header](../nv-navigation-view-header)

### Graph
```mermaid
graph TD;
  nv-navigation-view-items --> nv-navigation-view-header
  style nv-navigation-view-items fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
