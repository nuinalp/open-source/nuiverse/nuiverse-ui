/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, Prop, h, Host, Element, State, Event, EventEmitter } from '@stencil/core';
import { UploadEventDetail } from '../../interface';

@Component({
  tag: 'nv-upload',
  shadow: false,
})
export class NvUpload {
  private input!: HTMLInputElement;
  private progressLists: HTMLNvProgressElement[] = [];
  public nvList!: HTMLNvListElement;
  private uploads: any[] = [];

  //
  // State
  //

  @State() highlightEvent: boolean = false;
  @State() filesToDo: number = 0;
  @State() filesDone: number = 0;
  @State() listFiles: any[] = [];
  @State() listItens: HTMLNvListItemElement[] = [];
  @State() update: boolean = false;


  /**
   * Reference to the host element.
   */
  @Element() el!: HTMLNvUploadElement;

  /**
   * If `true`, the user can add a custom message element with an action click.
   */
  @Prop() customMessage: boolean = false;

  /**
   * If `true`, the upload will support multiple files upload.
   */

  @Prop() multiple: boolean = false;

  /**
   * If `true`, the user cannot drag files for the upload.
   */

  @Prop() nodrop: boolean = false;

  /**
   * If `false`, the list of the files will be hidden.
   */
  @Prop() list?: boolean = true;

  /**
   * Determines the url of the server where the request is made.
   */
  @Prop() action?: string | undefined;

  /**
   * The name of the input, which is submitted with the form data.
   */
  @Prop() name?: string | undefined;

  /**
   * If `true`, the upload will be a compact style.
   */
  @Prop() compact: boolean = false;

  /**
   * The type of the supported files for the upload.
   */
  @Prop() accept?: string = 'all';

  /**
   * If `true`, the user cannot interact with the upload.
   */
  @Prop() disabled: boolean = false;

  //
  // Events
  //

  /**
   * Emitted when the file is successfully uploaded.
   */
  @Event() nvOnSuccess!: EventEmitter<UploadEventDetail>;

  /**
   * Emitted when an error occurred while the file is uploaded.
   */
  @Event() nvOnError!: EventEmitter<UploadEventDetail>;

  /**
   * Emitted when a file is deleted from the list.
   */
  @Event() nvOnDelete!: EventEmitter<File>;

  //
  // Lifecyles
  //

  componentDidLoad() {
    if (!this.disabled) {
      const actionEl = this.el.querySelector('.nv-upload-action') as HTMLElement;

      if (actionEl) {
        actionEl.onclick = this.openDialog;
      }

      if (!this.nodrop) {
        // Prepare and apply events for drag and drop.
        this.prepare();
      }
    }
  }

  componentDidUpdate() {
    if (!this.update) {
      this.update = true;
      [...(this.uploads)].forEach((func: any) => {
        func();
      });
    }
  }

  public prepare(): void {
    // Apply the events
    ['dragenter', 'dragover', 'dragleave', 'drop'].forEach((eventName: string) => {
      this.el.addEventListener(eventName, this.preventDefaults, false);
    });

    // Apply highlight events
    ['dragenter', 'dragover'].forEach((eventName: string) => {
      this.el.addEventListener(eventName, this.highlight, false);
    });

    ['dragleave', 'drop'].forEach((eventName: string) => {
      this.el.addEventListener(eventName, this.unhighlight, false);
    });

    this.el.addEventListener('drop', this.handleDrop, false);
  }

  public highlight = (): void => {
    this.highlightEvent = true;
  }

  public unhighlight = (): void => {
    this.highlightEvent = false;
  }

  public handleDrop = (e: DragEvent): void => {
    const dt = e.dataTransfer;
    const files = dt?.files;

    if (files) {
      this.handleFiles(files);
    }
  }

  public preventDefaults = (e: Event): void => {
    e.preventDefault();
    e.stopPropagation();
  }

  public handleFiles = (files: FileList): void => {
    this.update = false;
    // Create a initial progress.
    this.initProgress(files);
    // Upload files.
    this.listFiles.push(...files);
    ([...(this.listFiles)]).forEach((file, i) => {
      this.uploads.push(
        ()  => this.uploadFile(file, i),
      );
    });
    this.input.value = '';
  }

  public initProgress (files: FileList): void {
    this.filesToDo = files.length;
  }

  public progressDone = (i: number, e: Event, file: File): void => {
    this.filesDone += 1;
    this.listFiles[i]['nv-send'] = true;
    this.nvOnSuccess.emit({
      file,
      XMLHttpRequest: e.srcElement,
    });
  }

  public progressError = (e: Event, file: File): void => {
    this.nvOnError.emit({
      file,
      XMLHttpRequest: e.srcElement,
    });
  }

  public updateProgress = (fileNumber: number, percent: number): void => {
    const progress = this.progressLists[fileNumber];
    if (progress) {
      progress.value = Math.round(percent);
    }
  }

  public uploadFile = (file: File, i: number): void => {
    // Check if this file is not sent yet...
    if (!('nv-send' in file)) {
      const action = this.action;
      const xhr = new XMLHttpRequest();
      const formData = new FormData();

      if (!action || !this.name) {
        return;
      }

      xhr.open('POST', action, true);

      // Add progress tracker
      xhr.upload.addEventListener('progress', (e) => {
        this.updateProgress(i, (e.loaded * 100.0 / e.total) || 100);
      });

      xhr.addEventListener('readystatechange', (e: Event) => {
        if (xhr.readyState === 4 && xhr.status === 200) {
          this.progressDone(i, e, file);
        } else if (xhr.readyState === 4 && xhr.status !== 200) {
          this.progressError(e, file);
        }
      });

      formData.append(this.name, file);
      xhr.send(formData);
    } else {
      this.updateProgress(i, 100);
    }
  }


  private makeIcon(): SVGAElement {
    return (
      <svg class='nv-upload-icon' viewBox='0 0 30 30'>
        <path d='m4.6172 6c-0.36628 0.6326-0.67266
         1.301-0.91992 2h22.605c-0.24726-0.699-0.55364-1.3674-0.91992-2h-20.766zm10.465
        5.7598c-0.7698 0-1.5396 0.29155-2.1211 0.87305l-6.9609 6.9609c0.43664
        0.50589 0.90744 0.97935 1.4219 1.4062l6.9531-6.9531c0.404-0.40397
        1.0101-0.40397 1.4141 0l6.9531 6.9531c0.5142-0.42662 0.98544-0.9006
        1.4219-1.4062l-6.9609-6.9609c-0.5815-0.5815-1.3513-0.87305-2.1211-0.87305z'/>
      </svg>
    );
  }

  private makeMessage(): HTMLDivElement {
    if (!this.customMessage) {
      if (!this.compact) {
        return (
          <div class='nv-text-center nv-my-3'>
            <h3><strong class='nv-upload-action'>Choose a file</strong> or drag it here.</h3>
          </div>
        );
      }

      return (
        <div class='nv-text-center nv-my-3'>
          <h4><strong class='nv-upload-action'>Choose a file</strong> or drag it here.</h4>
        </div>
      );
    }

    return <slot></slot>;
  }

  private makeTickIcon(): SVGElement {
    return (
      <svg class='nv-tick-icon-upload' viewBox='0 0 30 30'>
      <g transform='translate(0 -283.15)'>
        <path
          transform='translate(0 283.15)'
          d='m27 6.0195-15.932 18.988-8.0684-6.7695v2.6094l6.7832
          5.6914c0.83553 0.70109 2.1173 0.58944 2.8184-0.24609l14.398-17.16v-3.1133z'
        />
      </g>
      </svg>
    );
  }


  private makeList() {
    const list: HTMLNvListItemElement[] = [];
    const nvProgress: HTMLNvProgressElement[] = [];
    let tickIcon: SVGElement | null;
    let removeBtn: HTMLNvButtonElement | null;

    this.listFiles.forEach((file, i) => {
      if (!('nv-send' in file)) {
        removeBtn = (
          <nv-button
            onClick={ () => this.removeItem(i) }
            circle
            size='small'
            text
          >X</nv-button>
        );
      } else {
        tickIcon = this.makeTickIcon();
      }

      list.push((
        <nv-list-item textTitle={ file.name }>
          <div class='nv-upload-progress-container'>
            <div class='nv-upload-progress'>
              <nv-progress
                ref={ el => nvProgress.push(el!) }
                id={ `nv-upload-${file.name}` } value={ 0 }></nv-progress>
            </div>
            <div class='nv-upload-actions'>
              { removeBtn }
              { tickIcon }
            </div>
          </div>
        </nv-list-item>
      ));
    });

    this.progressLists = nvProgress || [];
    this.listItens = list || [];

    if (this.list) {
      return (
        <div class='nv-list-upload'>
          <nv-list ref={ el => this.nvList = el! }>
            { this.listItens }
          </nv-list>
        </div>
      );
    }
  }

  private removeItem (i: number): void {
    this.nvOnDelete.emit(this.listFiles[i]);
    this.listFiles.splice(i, 1);
    this.progressLists.splice(i, 1);
    this.listItens = this.listItens.filter(item => item !== this.listItens[i]);
  }

  private openDialog = (): void => {
    this.input.click();
  }

  private onChange = (e: Event): void => {
    const input = e.target as HTMLInputElement;
    this.handleFiles(input.files!);
  }

  render() {
    const icon = this.makeIcon();
    const message = this.makeMessage();
    const list = this.makeList();
    return (
      <Host class={{
        'nv-upload': true,
        'nv-upload-nodrop': this.nodrop,
        'nv-upload-highlight': this.highlightEvent,
        'nv-upload-compact': this.compact,
        'nv-upload-disabled': this.disabled,
      }}>
        <div class='nv-upload-container'>
          <input
            type='file'
            onChange={ this.onChange }
            ref={ input => this.input = input! }
            multiple={ this.multiple }
            accept={ this.accept }
          />
          { icon }
          {/* Message */}
          { message }
        </div>
        { list }
      </Host>
    );
  }
}
