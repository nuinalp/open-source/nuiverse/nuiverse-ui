# nv-upload



<!-- Auto Generated Below -->


## Properties

| Property        | Attribute        | Description                                                                | Type                   | Default     |
| --------------- | ---------------- | -------------------------------------------------------------------------- | ---------------------- | ----------- |
| `accept`        | `accept`         | The type of the supported files for the upload.                            | `string \| undefined`  | `'all'`     |
| `action`        | `action`         | Determines the url of the server where the request is made.                | `string \| undefined`  | `undefined` |
| `compact`       | `compact`        | If `true`, the upload will be a compact style.                             | `boolean`              | `false`     |
| `customMessage` | `custom-message` | If `true`, the user can add a custom message element with an action click. | `boolean`              | `false`     |
| `disabled`      | `disabled`       | If `true`, the user cannot interact with the upload.                       | `boolean`              | `false`     |
| `list`          | `list`           | If `false`, the list of the files will be hidden.                          | `boolean \| undefined` | `true`      |
| `multiple`      | `multiple`       | If `true`, the upload will support multiple files upload.                  | `boolean`              | `false`     |
| `name`          | `name`           | The name of the input, which is submitted with the form data.              | `string \| undefined`  | `undefined` |
| `nodrop`        | `nodrop`         | If `true`, the user cannot drag files for the upload.                      | `boolean`              | `false`     |


## Events

| Event         | Description                                                | Type                             |
| ------------- | ---------------------------------------------------------- | -------------------------------- |
| `nvOnDelete`  | Emitted when a file is deleted from the list.              | `CustomEvent<File>`              |
| `nvOnError`   | Emitted when an error occurred while the file is uploaded. | `CustomEvent<UploadEventDetail>` |
| `nvOnSuccess` | Emitted when the file is successfully uploaded.            | `CustomEvent<UploadEventDetail>` |


## Dependencies

### Depends on

- [nv-button](../nv-button)
- [nv-list-item](../nv-list-item)
- [nv-progress](../nv-progress)
- [nv-list](../nv-list)

### Graph
```mermaid
graph TD;
  nv-upload --> nv-button
  nv-upload --> nv-list-item
  nv-upload --> nv-progress
  nv-upload --> nv-list
  style nv-upload fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
