/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, Prop, h, Watch, State, Host, Method } from '@stencil/core';
import { Color } from '../../interface';

@Component({
  tag: 'nv-progress',
  shadow: false,
})
export class NvProgress {

  //
  // State
  //

  /**
   * The progress for the circle progressbar
   */
  @State() dashoffset:number = 339.292;

  /**
   * The value of the progress bar.
   */
  @Prop({ reflect: true }) value!: number;
  @Watch('value')
  handleValue(value: number) {
    if (value !== undefined) {
      this.calcProgress();
    }
  }

  /**
   * The progress bar color.
   */
  @Prop() color?: Color = 'primary';

  /**
   * If `true`, the progress bar will have a indeterminate animation.
   */
  @Prop() indeterminate: boolean = false;

  /**
   * If `true`, the progress bar will have a rounded <style className=""></style>
   */
  @Prop() rounded: boolean = false;

  /**
   * If `true`, the progress bar will have a circle style.
   */
  @Prop() circle: boolean = false;

  /**
   * If `true`, the progress bar will have a semi circle style.
   */
  @Prop() semiCircle: boolean = false;

  /**
   * If `true`, the progress bar will have a square style.
   */
  @Prop() square: boolean = false;

  /**
   * The label for the progress bar.
   */
  @Prop() label: boolean = false;

  //
  // Lifecycles
  //

  componentWillLoad() {
    this.calcProgress();
  }

  //
  // Methods
  //

  /**
   * Call this method to apply a new value.
   */
  @Method()
  async progress(newProgress: number): Promise<void> {
    if (newProgress) {
      this.value = newProgress;
    }
  }

  private calcProgress(): void {
    const radius: number = 54;
    const circumference: number = 2 * Math.PI * radius;
    const progress: number = this.value / 100;
    let dashoffset: number = circumference * (1 - progress);

    if (this.semiCircle) {
      dashoffset = circumference - (169 - (dashoffset / 2));
    }

    if (this.square) {
      dashoffset = 400 * (1 - progress);
    }

    this.dashoffset = dashoffset;
  }

  private makeProgressBar(): HTMLDivElement | null {
    if (this.circle) {
      return (
        <div
            class={{ 'nv-progressbar': true }}
          >
          <svg width='100%' height='100%' viewBox='0 0 120 120'>
            <circle
              cx='60'
              cy='60'
              r='54'
              fill='none'
              stroke='#e6e6e6'
              stroke-width='12'
            />
            <circle
              class='circle-progressbar-svg'
              cx='60'
              cy='60'
              r='54'
              fill='none'
              stroke-width='12'
              stroke-dasharray='339.292'
              stroke-dashoffset={ this.dashoffset }
            />
          </svg>
          { this.makeLabel() }
        </div>
      );
    }

    if (this.semiCircle) {
      return (
        <div
            class={{
              'nv-progressbar': true,
              'nv-progressbar-primary': this.color === 'primary',
              'nv-progressbar-secondary': this.color === 'secondary',
              'nv-progressbar-warning': this.color === 'warning',
              'nv-progressbar-danger': this.color === 'danger',
              'nv-progressbar-success': this.color === 'success',
            }}
          >
          <svg width='100%' height='100%' viewBox='0 0 120 120'>
            <circle
              cx='60'
              cy='60'
              r='54'
              fill='none'
              stroke='#e6e6e6'
              stroke-width='12'
              stroke-dasharray='339.292'
              stroke-dashoffset='169'
            />
            <circle
              class='circle-progressbar-svg'
              cx='60'
              cy='60'
              r='54'
              fill='none'
              stroke-width='12'
              stroke-dasharray='339.292'
              stroke-dashoffset={ this.dashoffset }
            />
          </svg>
          { this.makeLabel() }
        </div>
      );
    }

    if (this.square) {
      return (
        <div
            class={{
              'nv-progressbar': true,
              'nv-progressbar-primary': this.color === 'primary',
              'nv-progressbar-secondary': this.color === 'secondary',
              'nv-progressbar-warning': this.color === 'warning',
              'nv-progressbar-danger': this.color === 'danger',
              'nv-progressbar-success': this.color === 'success',
            }}
          >
          <svg width='100%' height='100%'>
            <g transform='translate(0,-270.54164)'>
              <path
                class='square-progressbar-svg'
                fill='none'
                stroke-width='12'
                stroke-dasharray='400'
                stroke-dashoffset={ this.dashoffset }
                d='m 49.999967,270.6664 49.875345,-7e-5 v 99.75063 H 0.12468834 V 270.6664 Z'/>
            </g>
          </svg>
          { this.makeLabel() }
        </div>
      );
    }

    return (
      <div
      role='progressbar'
      aria-valuenow={ this.value }
      aria-valuemin='0'
      aria-valuemax='100'
      class={{
        'nv-progressbar': true,
        'nv-progressbar-primary': this.color === 'primary',
        'nv-progressbar-secondary': this.color === 'secondary',
        'nv-progressbar-warning': this.color === 'warning',
        'nv-progressbar-danger': this.color === 'danger',
        'nv-progressbar-success': this.color === 'success',
        'nv-progressbar-indeterminate': this.indeterminate,
      }}
      style={{ width: !this.circle || !this.semiCircle ? `${this.value}%` : '0' }}
    ></div>
    );
  }

  private makeLabel() {
    if  (this.label) {
      return <span class='nv-progressbar-label'>{ this.value }%</span>;
    }
  }

  render() {
    const progressBar = this.makeProgressBar();
    return (
      <Host
        class={{
          'nv-progress': true,
          'nv-progress-rounded': this.rounded,
          'nv-progress-circle': this.circle,
          'nv-progress-semi-circle': this.semiCircle,
          'nv-progress-square': this.square,
          'nv-progressbar-completed': this.value >= 100,
        }}
      >
        { progressBar }
      </Host>
    );
  }
}
