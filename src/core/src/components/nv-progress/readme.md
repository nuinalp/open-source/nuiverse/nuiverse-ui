# nv-progress



<!-- Auto Generated Below -->


## Properties

| Property             | Attribute       | Description                                                                  | Type                                                                          | Default     |
| -------------------- | --------------- | ---------------------------------------------------------------------------- | ----------------------------------------------------------------------------- | ----------- |
| `circle`             | `circle`        | If `true`, the progress bar will have a circle style.                        | `boolean`                                                                     | `false`     |
| `color`              | `color`         | The progress bar color.                                                      | `"danger" \| "primary" \| "secondary" \| "success" \| "warning" \| undefined` | `'primary'` |
| `indeterminate`      | `indeterminate` | If `true`, the progress bar will have a indeterminate animation.             | `boolean`                                                                     | `false`     |
| `label`              | `label`         | The label for the progress bar.                                              | `boolean`                                                                     | `false`     |
| `rounded`            | `rounded`       | If `true`, the progress bar will have a rounded <style className=""></style> | `boolean`                                                                     | `false`     |
| `semiCircle`         | `semi-circle`   | If `true`, the progress bar will have a semi circle style.                   | `boolean`                                                                     | `false`     |
| `square`             | `square`        | If `true`, the progress bar will have a square style.                        | `boolean`                                                                     | `false`     |
| `value` _(required)_ | `value`         | The value of the progress bar.                                               | `number`                                                                      | `undefined` |


## Methods

### `progress(newProgress: number) => Promise<void>`

Call this method to apply a new value.

#### Parameters

| Name          | Type     | Description |
| ------------- | -------- | ----------- |
| `newProgress` | `number` |             |

#### Returns

Type: `Promise<void>`




## Dependencies

### Used by

 - [nv-upload](../nv-upload)

### Graph
```mermaid
graph TD;
  nv-upload --> nv-progress
  style nv-progress fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
