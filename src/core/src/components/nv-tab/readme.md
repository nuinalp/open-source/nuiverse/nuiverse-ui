# nv-tab



<!-- Auto Generated Below -->


## Properties

| Property           | Attribute | Description                                                                                                                              | Type     | Default     |
| ------------------ | --------- | ---------------------------------------------------------------------------------------------------------------------------------------- | -------- | ----------- |
| `tab` _(required)_ | `tab`     | A tab id must be provided for each `nv-tab`. It's used internally to reference the selected tab or by the router to switch between them. | `string` | `undefined` |


## Methods

### `removeActive() => Promise<void>`

Remove the active component of the tab.

#### Returns

Type: `Promise<void>`



### `setActive() => Promise<void>`

Set the active component for the tab

#### Returns

Type: `Promise<void>`




----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
