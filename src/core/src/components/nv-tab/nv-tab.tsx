/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  h,
  Host,
  Prop,
  Element,
  Method,
  Event,
  EventEmitter,
}                                from '@stencil/core';
import { runNextAnimationFrame } from '../../utils';

@Component({
  tag: 'nv-tab',
  styleUrl: './style.scss',
  shadow: true,
})
export class NvTab {
  public rootTab!: HTMLNvTabsElement;
  /**
   * Reference to the host element.
   */
  @Element() el!: HTMLNvTabElement;

  /** @internal */
  @Prop({ mutable: true }) active = false;

  /**
   * A tab id must be provided for each `nv-tab`. It's used internally to reference
   * the selected tab or by the router to switch between them.
   */
  @Prop() tab!: string;

  //
  // Events
  //

  /**
   * Emitted when the navigation from previuos tab has finished.
   * @internal
   */
  @Event() nvPrevTabDidChange!: EventEmitter<{tab: string}>;

  handleAnimation = (evt: any) => {
    const tab = this.el.shadowRoot?.querySelector<HTMLDivElement>('.nv-tab');

    if (tab && evt.type === 'animationend') {

      if (tab.classList.contains('nv-displaying-tab')) {
        tab.classList.add('nv-tab-displayed');
        tab.classList.remove('nv-displaying-tab');
      }

      // go back to initial state after hiding
      if (tab.classList.contains('nv-hiding-tab')) {
        tab.classList.remove('nv-tab-displayed');
        tab.classList.remove('nv-displaying-tab');
        tab.classList.remove('nv-hiding-tab');
        this.nvPrevTabDidChange.emit({
          tab: this.tab,
        });
        this.active = false;
      }
    }
  }

  // --------------------------------------------
  // Lificycles
  // --------------------------------------------
  componentWillLoad() {
    this.rootTab = this.el.parentElement as HTMLNvTabsElement;
  }

  componentDidLoad() {
    this.el.shadowRoot
      ?.querySelector('.nv-tab')
      ?.addEventListener('animationend', this.handleAnimation);

    this.rootTab.registerTab(this.el);
  }

  //
  // Methods
  //

  /**
   * Set the active component for the tab
   */
  @Method()
  async setActive(): Promise<void> {
    this.active = true;
    const tab = this.el.shadowRoot?.querySelector<HTMLNvTabElement>('.nv-tab');

    if (!tab) {
      return;
    }

    // Wait a frame once display is no longer "none", to establish basis for the animation
    runNextAnimationFrame(() => {
      // no operation during displaying and hiding
      if (tab.classList.contains('nv-displaying-tab') ||
      tab.classList.contains('nv-hiding-tab')) {
        return;
      }

      tab.classList.add('nv-displaying-tab');
    });
  }

  /**
   * Remove the active component of the tab.
   */
  @Method()
  async removeActive(): Promise<void> {
    this.el.shadowRoot?.querySelector('.nv-tab')?.classList.add('nv-hiding-tab');
  }

  /**
   * Register this tab in nv-tabs, as some frameworks do not render all components at the same time, like Vuejs.
   * So we need to tell nv-tabs that this component is already rendered.
   * @internal
   * @returns 
   */

  registerInTabRoot() {
    this.rootTab.registerTab(this.el);
  }

  render() {
    return (
      <Host
        role='tabpanel'
        aria-hidden={!this.active ? 'true' : null}
        aria-labelledby={`tab-button-${this.tab}`}
      >
        <div
          class={{
            'nv-tab': true,
            'nv-tab-hidden': !this.active,
          }}
        >
          <div class='nv-tab-content'>
            <slot />
          </div>
        </div>
      </Host>
    );
  }
}
