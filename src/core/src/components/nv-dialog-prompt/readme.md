# nv-dialog-prompt



<!-- Auto Generated Below -->


## Properties

| Property         | Attribute          | Description                                                            | Type                                                                          | Default     |
| ---------------- | ------------------ | ---------------------------------------------------------------------- | ----------------------------------------------------------------------------- | ----------- |
| `autoSelect`     | `auto-select`      | If `true`, the text field's text is selected after opening the dialog. | `boolean \| undefined`                                                        | `true`      |
| `color`          | `color`            | The color for the confirmation button.                                 | `"danger" \| "primary" \| "secondary" \| "success" \| "warning" \| undefined` | `undefined` |
| `label`          | `label`            | The label for the text field.                                          | `string \| undefined`                                                         | `undefined` |
| `textCancel`     | `text-cancel`      | The cancel button text for this dialog.                                | `string \| undefined`                                                         | `undefined` |
| `textConfirm`    | `text-confirm`     | The confirmation button text for this dialog.                          | `string \| undefined`                                                         | `undefined` |
| `textFieldValue` | `text-field-value` | The initial value for the text field.                                  | `string \| undefined`                                                         | `undefined` |


## Methods

### `toggle() => Promise<void>`

Close or open the nv-dialog.

#### Returns

Type: `Promise<void>`




## Dependencies

### Depends on

- [nv-button](../nv-button)
- [nv-text-field](../nv-text-field)

### Graph
```mermaid
graph TD;
  nv-dialog-prompt --> nv-button
  nv-dialog-prompt --> nv-text-field
  nv-text-field --> nv-button
  style nv-dialog-prompt fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
