/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Element, Prop, Method, Host, State, Fragment } from '@stencil/core';
import { Color } from '../../interface';

@Component({
  tag: 'nv-dialog-prompt',
  shadow: false,
})
export class NvDialogPrompt {
  /**
   * Reference this dialog element
   */
  textField!: HTMLNvTextFieldElement;
  promptValue!: string;

  /**
   * Reference to the host element
   */
  @Element() el!: HTMLElement;

  //
  // State
  //

  /**
   * If `true`, the dialog is displayed.
   */
  @State() show: boolean = false;

  //
  // Props
  //

  /**
   * The confirmation button text for this dialog.
   */
  @Prop() textConfirm?: string;

  /**
   * The cancel button text for this dialog.
   */
  @Prop() textCancel?: string;

  /**
   * The color for the confirmation button.
   */
  @Prop() color?: Color;

  /**
   * The label for the text field.
   */
  @Prop() label?: string;

  /**
   * The initial value for the text field.
   */
  @Prop() textFieldValue?: string;

  /**
   * If `true`, the text field's text is selected after opening the dialog.
   */
  @Prop() autoSelect?: boolean = true;


  //
  // Methods
  //

  /**
   * Close the dialog
   */
  private close() {
    const dialog = this.el;
    dialog.classList.add('closing');
    this.show = false;
  }

  /**
   * Close or open the nv-dialog.
   */
  @Method()
  async toggle() {
    if (this.show) {
      this.close();
      return;
    }

    this.open();
  }

  private open() {
    const dialog = this.el;
    dialog.style.display = 'flex';

    // Wait a frame once display is no longer "none", to establish the animation
    this.runNextAnimationFrame(() => {
      // no operation during flying and hiding
      if (dialog.classList.contains('flying') ||
          dialog.classList.contains('hiding')) {
        return;
      }

      // If we get focus when we are closing the group, we need to cancel the closing
      // state.
      dialog.classList.remove('closed');
      dialog.classList.remove('closing');
      dialog.classList.add('opening');
      this.show = true;
    });
  }

  //
  // Lifecycles
  //
  componentDidLoad() {
    const dialog = this.el;
    if (dialog) {
      // Add Watch events
      dialog.addEventListener('transitionend', (e: Event) => this.watchEvents(e));
    }
  }

  private watchEvents(evt: any): void {
    const dialog = this.el;
    if (evt.type === 'transitionend') {

      // We only process 'opacity' because all states have this
      // change.
      if (evt.propertyName !== 'opacity') {
        return;
      }

      if (dialog.classList.contains('opening')) {
        dialog.classList.add('opened');
        dialog.classList.remove('opening');

        if (this.textFieldValue || this.promptValue) {
          this.textField.selectText();
        }

      }  else if (dialog.classList.contains('flying')) {
        dialog.classList.remove('flying');
        this.open();
      }

      // go back to initial state after hiding
      if (dialog.classList.contains('hiding') || dialog.classList.contains('closing')) {
        dialog.classList.remove('opened');
        dialog.classList.remove('opening');
        dialog.classList.remove('closing');
        dialog.classList.remove('flying');
        dialog.classList.remove('hiding');
        dialog.classList.add('closed');
        dialog.style.display = 'none';
      }
    }
  }

  //
  // Utils
  //

  private runNextAnimationFrame(callback: () => void) {
    requestAnimationFrame(() => {
      setTimeout(callback, 0);
    });
  }

  private makeFooter(): HTMLElement | null {
    return (
      <Fragment>
        {/* Cancel */}
        <nv-button
          text
          disabled-ripple
          onClick={ () => this.close() }>
            <span>{ this.textCancel }</span>
        </nv-button>

        {/* Confirm */}
        <nv-button
          text
          disabled-ripple
          color={this.color}
          onClick={ () => this.close() }>
            <span>{ this.textConfirm }</span>
        </nv-button>
        <div class='nv-dialog-footer-divider'/>
      </Fragment>
    );
  }

  render() {
    const footer = this.makeFooter();
    return (
      <Host
        class={{
          'nv-dialog': true,
          'nv-dialog-prompt': true,
          closed: !this.show,
        }}
        style={{ display: !this.show ? 'none' : 'unset' }}
        >
          <div class='nv-dialog-background' onClick={ () => this.close() }></div>
          <div class='nv-dialog-container'>

            <section class='nv-dialog-body'>
              <nv-text-field
                ref={ el => this.textField = el! }
                label={ this.label }
                onInput={ node => this.promptValue = (node.target as HTMLInputElement).value }
                value={ this.textFieldValue }
              >
              </nv-text-field>
            </section>

            <footer class='nv-dialog-footer'>
              { footer }
            </footer>
        </div>
      </Host>
    );
  }
}
