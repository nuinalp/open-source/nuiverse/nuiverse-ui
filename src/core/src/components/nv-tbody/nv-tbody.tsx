/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Host } from '@stencil/core';
@Component({
  tag: 'nv-tbody',
  shadow: false,
})
export class NvTbody {
  render() {
    return (
      <Host>
        <slot></slot>
      </Host>
    );
  }
}
