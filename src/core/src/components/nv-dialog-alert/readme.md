# nv-dialog-alert



<!-- Auto Generated Below -->


## Properties

| Property                   | Attribute      | Description                                  | Type                                                             | Default     |
| -------------------------- | -------------- | -------------------------------------------- | ---------------------------------------------------------------- | ----------- |
| `color` _(required)_       | `color`        | The color for the confirmation button.       | `"danger" \| "primary" \| "secondary" \| "success" \| "warning"` | `undefined` |
| `textConfirm` _(required)_ | `text-confirm` | The confirmation button text for this dialog | `string`                                                         | `undefined` |


## Methods

### `toggle() => Promise<void>`

Close or open the nv-dialog.

#### Returns

Type: `Promise<void>`




## Dependencies

### Depends on

- [nv-button](../nv-button)

### Graph
```mermaid
graph TD;
  nv-dialog-alert --> nv-button
  style nv-dialog-alert fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
