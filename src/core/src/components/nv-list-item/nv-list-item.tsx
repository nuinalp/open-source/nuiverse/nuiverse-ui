/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Prop, Element, Host, State } from '@stencil/core';

@Component({
  tag: 'nv-list-item',
  styleUrl: './style.scss',
  shadow: true,
})
export class NvListItem {
  /**
   * Reference to the host element
   */
  @Element() el!: HTMLNvListItemElement;

  //
  // State
  //

  @State() displayStartSlot = false;
  @State() displayEndSlot = false;

  //
  // Props
  //

  /**
   * The text title for the list item.
   */
  @Prop() textTitle?: string;

  /**
   * The subtitle text's text for the list item.
   */
  @Prop() textSubtitle?: string;

  /**
   * The first name used by **user** list.
   */
  @Prop() firstName?: string;

  /**
   * The last name used by **user** list.
   */
  @Prop() lastName?: string;

  /**
   * The primaryText name used by **user** list.
   */
  @Prop() primaryText?: string;

  /**
   * The subject text used by **email** list.
   */
  @Prop() textSubject?: string;

  /**
   * The message text used by **email** list
   */
  @Prop() textMessage?: string;

  /**
   * A stamp text for the message, used by **email** list.
   */
  @Prop() textStamp?: string;

  /**
   * If `true`, a small rounded element, will be displayed on the left of the list
   * Used by **email** list.
   */
  @Prop() unread: boolean = false;

  /**
   * If `true`, the list-item will be hidden.
   */
  @Prop({ mutable: true }) hidden: boolean = false;

  //
  // Lifecycles
  //

  componentDidLoad() {
    this.displayStartSlot = ((this.el.shadowRoot?.querySelector('slot[name="start"]') as HTMLSlotElement).assignedElements()).length > 0;
    this.displayEndSlot = ((this.el.shadowRoot?.querySelector('slot[name="end"]') as HTMLSlotElement).assignedElements()).length > 0;
  }

  private makeTitles(): HTMLElement | null {
    const time = this.makeTime();

    if (this.textTitle || this.textSubtitle) {
      return (
        <div class='nv-list-title'>
          <div class='nv-list-title-area'>
            <div class='nv-list-title'>{ this.textTitle }</div>
            <div class='nv-list-sub-title'>{ this.textSubtitle }</div>
          </div>
          { time }
        </div>
      );
    }

    if (this.firstName || this.lastName) {
      return (
        <div class='nv-list-title'>
          <div class="nv-list-title-area">
            <span class='nv-list-first-name'>{ this.firstName }</span>
            <span class='nv-list-last-name'> { this.lastName }</span>
          </div>
          { time }
        </div>
      );
    }

    return null
  }

  private makePrimaryText(): HTMLElement | null {
    if (this.primaryText) {
      return <span class='nv-list-primary-text'>{ this.primaryText }</span>;
    }

    return null;
  }

  private makeSubject(): HTMLElement | null {
    if (this.textSubject) {
      return <small class='nv-list-subject'>{ this.textSubject }</small>;
    }

    return null;
  }

  private makeMessage(): HTMLElement | null {
    if (this.textMessage) {
      return <small class='nv-list-message'>{ this.textMessage }</small>;
    }

    return null;
  }

  private makeTime(): HTMLElement | null {
    if (this.textStamp) {
      return <time>{ this.textStamp }</time>;
    }

    return null;
  }

  private makeAvatar(): HTMLElement | null {
    return <div class='nv-avatar-slot'><slot name='avatar'/></div>;
  }

  private makeUnread(): HTMLElement | null {
    if (this.unread) {
      return <div class='unread-email-badge'></div>;
    }

    return null;
  }

  render() {
    const title = this.makeTitles();
    const primaryText = this.makePrimaryText();
    const subject = this.makeSubject();
    const textMessage = this.makeMessage();
    const avatar = this.makeAvatar();
    const unread = this.makeUnread();

    return (
      <Host
        class={{
          'nv-list-item': true,
          'nv-list-item-hidden': this.hidden,
          'nv-list-email-unread': this.unread,
        }}
      >
        { unread }
        <div class={{
          'nv-list-item-container': true,
          'display-start-slot': this.displayStartSlot,
          'display-end-slot': this.displayEndSlot,
          'nv-p-1': true,
          }}>
          <div>
            <div class='nv-list-slot-start'>
              <slot name='start'></slot>
            </div>
            <header class='nv-list-title-container'>
              { title }
              { primaryText }
              { subject }
              { textMessage }
              <slot name="header" />
            </header>
            <div class='nv-list-slot-end'>
              { avatar }
              <slot name='end'></slot>
            </div>
          </div>
          <slot />
        </div>
      </Host>
    );
  }
}
