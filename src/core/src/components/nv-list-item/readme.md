# nv-list-item



<!-- Auto Generated Below -->


## Properties

| Property       | Attribute       | Description                                                                                           | Type                  | Default     |
| -------------- | --------------- | ----------------------------------------------------------------------------------------------------- | --------------------- | ----------- |
| `firstName`    | `first-name`    | The first name used by **user** list.                                                                 | `string \| undefined` | `undefined` |
| `hidden`       | `hidden`        | If `true`, the list-item will be hidden.                                                              | `boolean`             | `false`     |
| `lastName`     | `last-name`     | The last name used by **user** list.                                                                  | `string \| undefined` | `undefined` |
| `primaryText`  | `primary-text`  | The primaryText name used by **user** list.                                                           | `string \| undefined` | `undefined` |
| `textMessage`  | `text-message`  | The message text used by **email** list                                                               | `string \| undefined` | `undefined` |
| `textStamp`    | `text-stamp`    | A stamp text for the message, used by **email** list.                                                 | `string \| undefined` | `undefined` |
| `textSubject`  | `text-subject`  | The subject text used by **email** list.                                                              | `string \| undefined` | `undefined` |
| `textSubtitle` | `text-subtitle` | The subtitle text's text for the list item.                                                           | `string \| undefined` | `undefined` |
| `textTitle`    | `text-title`    | The text title for the list item.                                                                     | `string \| undefined` | `undefined` |
| `unread`       | `unread`        | If `true`, a small rounded element, will be displayed on the left of the list Used by **email** list. | `boolean`             | `false`     |


## Dependencies

### Used by

 - [nv-people-picker](../nv-people-picker)
 - [nv-upload](../nv-upload)

### Graph
```mermaid
graph TD;
  nv-people-picker --> nv-list-item
  nv-upload --> nv-list-item
  style nv-list-item fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
