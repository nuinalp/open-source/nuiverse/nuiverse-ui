# nv-loader



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                        | Type                                                                          | Default     |
| -------- | --------- | -------------------------------------------------- | ----------------------------------------------------------------------------- | ----------- |
| `color`  | `color`   | The color of the loader.                           | `"danger" \| "primary" \| "secondary" \| "success" \| "warning" \| undefined` | `'primary'` |
| `paused` | `paused`  | If `true`, the snipper's animation will be paused. | `boolean`                                                                     | `false`     |
| `size`   | `size`    | The size of the loader.                            | `"big" \| "large" \| "medium" \| "small" \| undefined`                        | `'medium'`  |


## Dependencies

### Used by

 - [nv-people-picker](../nv-people-picker)
 - [nv-search-box](../nv-search-box)

### Graph
```mermaid
graph TD;
  nv-people-picker --> nv-loader
  nv-search-box --> nv-loader
  style nv-loader fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
