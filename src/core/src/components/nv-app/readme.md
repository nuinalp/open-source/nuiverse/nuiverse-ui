# nv-app



<!-- Auto Generated Below -->


## Properties

| Property        | Attribute        | Description                 | Type                                                                                              | Default          |
| --------------- | ---------------- | --------------------------- | ------------------------------------------------------------------------------------------------- | ---------------- |
| `theme`         | `theme`          | The theme of app            | `"dark" \| "light"`                                                                               | `'light'`        |
| `toastPosition` | `toast-position` | The position of the toasts. | `"bottom-center" \| "bottom-left" \| "bottom-right" \| "top-center" \| "top-left" \| "top-right"` | `'bottom-right'` |


## Dependencies

### Depends on

- [nv-toasts](../nv-toasts)

### Graph
```mermaid
graph TD;
  nv-app --> nv-toasts
  style nv-app fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
