/**
* Copyright (c) 2020 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Prop, Host } from '@stencil/core';

@Component({
  tag: 'nv-app',
  shadow: false,
})
export class NvApp {

  /**
   * The theme of app
   */
  @Prop() theme: 'dark' | 'light' = 'light';

  /**
   * The position of the toasts.
   */
  @Prop() toastPosition: 'top-left' | 'top-right' | 'top-center' |
  'bottom-left' | 'bottom-right' | 'bottom-center' = 'bottom-right';

  render() {
    return (
      <Host class='nv-app' data-theme={this.theme}>
        <nv-toasts position={this.toastPosition}/>
        <slot />
      </Host>
    );
  }
}
