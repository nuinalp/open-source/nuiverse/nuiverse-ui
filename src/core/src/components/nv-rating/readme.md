# nv-rating



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                                                                                                           | Type                                                                          | Default     |
| ------------- | -------------- | --------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------- | ----------- |
| `color`       | `color`        | The color of the stars.                                                                                               | `"danger" \| "primary" \| "secondary" \| "success" \| "warning" \| undefined` | `undefined` |
| `generalRate` | `general-rate` | If `true`, the user cannot interact with the rating and the rating accepts float numbers to display a general rating. | `boolean`                                                                     | `false`     |
| `rate`        | `rate`         | The rate of the rating.                                                                                               | `number`                                                                      | `0`         |
| `readonly`    | `readonly`     | If `true`, the user cannot interact with the rating.                                                                  | `boolean`                                                                     | `false`     |
| `stars`       | `stars`        | The number of stars.                                                                                                  | `number`                                                                      | `5`         |


## Events

| Event      | Description                                      | Type                                   |
| ---------- | ------------------------------------------------ | -------------------------------------- |
| `nvChange` | Emitted when the value of the rating is changed. | `CustomEvent<RatingChangeEventDetail>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
