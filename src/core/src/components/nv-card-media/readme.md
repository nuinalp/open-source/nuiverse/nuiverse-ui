# nv-card-media



<!-- Auto Generated Below -->


## Properties

| Property                  | Attribute     | Description                           | Type                 | Default     |
| ------------------------- | ------------- | ------------------------------------- | -------------------- | ----------- |
| `alt` _(required)_        | `alt`         | The alt of the media                  | `string`             | `undefined` |
| `src` _(required)_        | `src`         | The src of the media                  | `string`             | `undefined` |
| `titleMedia` _(required)_ | `title-media` | The title of the media                | `string`             | `undefined` |
| `type`                    | `type`        | The type of the media, Video or image | `"image" \| "video"` | `'image'`   |


## Dependencies

### Used by

 - [nv-card](../nv-card)

### Graph
```mermaid
graph TD;
  nv-card --> nv-card-media
  style nv-card-media fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
