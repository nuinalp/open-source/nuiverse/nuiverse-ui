# nv-radio-group



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                                    | Type                  | Default     |
| -------- | --------- | -------------------------------------------------------------- | --------------------- | ----------- |
| `name`   | `name`    | The name of the control, which is submitted with the form data | `string \| undefined` | `undefined` |
| `value`  | `value`   | The value of the radio group.                                  | `string \| undefined` | `undefined` |


## Events

| Event      | Description                                            | Type                                       |
| ---------- | ------------------------------------------------------ | ------------------------------------------ |
| `nvChange` | Emitted when the value of the radion group is changed. | `CustomEvent<RadioGroupChangeEventDetail>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
