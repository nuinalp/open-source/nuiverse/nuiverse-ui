/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, Prop, h, Host, Element, Watch, Event, EventEmitter } from '@stencil/core';
import { RadioGroupChangeEventDetail } from '../nv-radio/radio';

@Component({
  tag: 'nv-radio-group',
  shadow: true,
})
export class NvRadioGroup {
  /**
   * Reference to the Host element.
   */
  @Element() el!: HTMLNvRadioGroupElement;

  /**
   * The name of the control, which is submitted with the form data
   */
  @Prop() name?: string;
  @Watch('name')
  handleName() {
    this.addName();
  }

  /**
   * The value of the radio group.
   */
  @Prop({ mutable: true }) value?: string;
  @Watch('value')
  handleValue(value: any) {
    this.updateRadios();
    this.nvChange.emit({ value, name: this.name });
  }

  //
  // Events
  //

  /**
   * Emitted when the value of the radion group is changed.
   */
  @Event() nvChange!: EventEmitter<RadioGroupChangeEventDetail>;

  //
  // Lifecyles
  //
  componentDidLoad() {
    this.addName();
  }

  //
  // Methods
  //

  private async updateRadios(): Promise<void> {
    const radios: NodeListOf<HTMLNvRadioElement> = this.el.querySelectorAll('nv-radio');
    radios.forEach((r: HTMLNvRadioElement) => {
      if (r.value !== this.value) {
        r.checked = false;
      }
    });
  }

  private async addName() {
    const radios: NodeListOf<HTMLNvRadioElement> = this.el.querySelectorAll('nv-radio');
    if (this.name) {
      radios.forEach((r: HTMLNvRadioElement) => {
        r.name = this.name;
      });
    }
  }

  render() {
    return (
      <Host
        role='radiogroup'
        class={{
          'nv-radio-group' : true,
        }}
      >
        <slot />
      </Host>
    );
  }
}
