/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Host, Element, Prop } from '@stencil/core';
@Component({
  tag: 'nv-code',
  shadow: false,
})
export class NvCode {

  //
  // Reference to the host.
  //

  @Element() el!: HTMLNvCodeElement;

  //
  // Props
  //

  /**
   * The label for the show source button.
   */
  @Prop() showCodeLabel: string | undefined;

  /**
   * If `true`, the view source is displayed.
   */
  @Prop() viewSource: boolean = false;

  /**
   * The title for the code component.
   */

  @Prop() codeTitle?: string;

  componentDidLoad() {
    const codeSource = this.el.querySelector('nv-code-source');
    if (codeSource) {
      const codePreview = this.el.querySelector('.nv-code-preview');
      codePreview?.appendChild(codeSource);
    }
  }

  private onClick = () => {
    if (this.viewSource) {
      this.viewSource = false;
      return;
    }

    this.viewSource = true;
  }

  render() {
    return (
      <Host class={{
        'nv-code': true,
        'nv-code-view-source': this.viewSource,
      }}>
        <header class='nv-d-flex nv-justify-content-between nv-align-items-center'>
          <h5>{ this.codeTitle }</h5>
          <nv-tooltip
            position='top'
            text={ this.showCodeLabel !== undefined ? this.showCodeLabel : 'View source' }>
            <nv-button class='nv-code-btn' text onClick={ this.onClick }>
            <svg width='30' height='30' version='1.1' viewBox='0 0 30 30'>
              <g transform='translate(0 -289.06)'>
                <g aria-label='{}'>
                <path
                  transform='translate(0 289.06)'
                  // tslint:disable-next-line: max-line-length
                  d='m11.779 3.1172c-4.4624 0-6.1607 1.1881-5.7969 4.4863l0.36523 3.4668c0.19402 1.8189-1.2862 2.377-3.3477 2.377v3.1055c2.0857 0 3.5417 0.55806 3.3477 2.377l-0.36523 3.4922c-0.33953 3.2498 1.3345 4.4609 5.7969 4.4609v-2.668c-1.4794 0-2.3771-0.31474-2.2559-1.5273l0.36328-3.5898c0.29102-2.7405-1.1628-3.7096-3.7578-4.0977 2.4495-0.36378 4.0488-1.3814 3.7578-4.0977l-0.36328-3.5898c-0.12126-1.2126 0.75224-1.5273 2.2559-1.5273v-2.668zm6.4414 0v2.668c1.4794 0 2.3771 0.31474 2.2559 1.5273l-0.36328 3.5898c-0.29102 2.7405 1.1882 3.7096 3.7832 4.0977-2.4737 0.36378-4.0742 1.3814-3.7832 4.0977l0.36328 3.5898c0.12126 1.2126-0.75224 1.5273-2.2559 1.5273v2.668c4.4624 0 6.1607-1.1881 5.7969-4.4863l-0.36523-3.4668c-0.19402-1.8189 1.3105-2.377 3.3477-2.377v-3.1055c-2.0614 0-3.5417-0.55805-3.3477-2.377l0.36523-3.4922c0.33953-3.2498-1.3345-4.4609-5.7969-4.4609z'/>
                </g>
              </g>
              </svg>

            </nv-button>
          </nv-tooltip>
        </header>
        <div class='nv-code-preview'></div>
        <slot/>
      </Host>
    );
  }
}
