# nv-code



<!-- Auto Generated Below -->


## Properties

| Property        | Attribute         | Description                              | Type                  | Default     |
| --------------- | ----------------- | ---------------------------------------- | --------------------- | ----------- |
| `codeTitle`     | `code-title`      | The title for the code component.        | `string \| undefined` | `undefined` |
| `showCodeLabel` | `show-code-label` | The label for the show source button.    | `string \| undefined` | `undefined` |
| `viewSource`    | `view-source`     | If `true`, the view source is displayed. | `boolean`             | `false`     |


## Dependencies

### Depends on

- [nv-tooltip](../nv-tooltip)
- [nv-button](../nv-button)

### Graph
```mermaid
graph TD;
  nv-code --> nv-tooltip
  nv-code --> nv-button
  style nv-code fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
