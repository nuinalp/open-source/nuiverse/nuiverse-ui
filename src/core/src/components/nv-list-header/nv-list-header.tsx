/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Host, Prop } from '@stencil/core';

@Component({
  tag: 'nv-list-header',
  styleUrl: './style.scss',
  shadow: true,
})
export class NvListHeader {

  //
  // Props
  //

  /**
   * If `true`, the header will have a sticky effect.
   * Don't use this directly, it is only to internal use.
   */
  @Prop() stickyHeader: boolean = false;

  render() {
    return (
      <Host
        class={{
          'nv-list-header': true,
          'is-sticky': this.stickyHeader,
        }}
      >
        <nv-sub-header><slot /></nv-sub-header>
      </Host>
    );
  }
}
