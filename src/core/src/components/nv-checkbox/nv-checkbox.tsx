/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  Prop,
  h,
  Event,
  EventEmitter,
  Watch,
  Host,
} from '@stencil/core';
import {
  CheckboxChangeEventDetail,
} from '../../interface';
@Component({
  tag: 'nv-checkbox',
  styleUrl: './style.scss',
  shadow: true,
})
export class NvCheckbox {
  el!: HTMLInputElement;

  //
  // Props
  //

  /**
   * If `true`, the user cannot interact with the checkbox.
   */
  @Prop() disabled: boolean = false;

   /**
   * If `true` the checkbox is selected.
   */
  @Prop({ mutable: true }) checked: boolean = false;

  /**
   * If `true` the checkbox will have a indeterminate visual.
   */
  @Prop() indeterminate: boolean = false;

  /**
   * The name of the control, which is submitted with the form data
   */
  @Prop() name!: string;

  /**
   * The value of the control, which is submitted with the form data.
   */
  @Prop() value!: string | undefined;

  //
  // Events
  //

  /**
   * Emitted when the value of the input is changed.
   */
  @Event() nvChange!: EventEmitter<CheckboxChangeEventDetail>;

  /**
   * Emitted when the input loses focus.
   */
  @Event() nvBlur!: EventEmitter<FocusEvent>;

  /**
   * Emitted when the input has focus.
   */
  @Event() nvFocus!: EventEmitter<FocusEvent>;

  //
  // Watch
  //

  @Watch('checked')
  handleChecked(isChecked: boolean) {
    if (isChecked && !this.disabled) {
      this.nvChange.emit({
        checked: this.checked,
        value: this.value,
      });
    }
  }

  //
  // Methods
  //

  private updateCheck = () => {
    if (!this.disabled) {
      const newChecked = !this.checked;
      this.checked = newChecked;
      this.el.checked = newChecked;
  
      this.nvChange.emit({
        checked: newChecked,
        value: this.value,
      });
    }
  }

  private onFocus = (ev: FocusEvent): void => {
    this.nvFocus.emit(ev);
  }

  private onBlur = (ev: FocusEvent): void => {
    this.nvFocus.emit(ev);
  }

  private makeIcon(): SVGElement {
    if (this.indeterminate) {
      return (
        <svg
          class={{
            'nv-checkbox-icon': true,
            checked : this.checked || this.indeterminate,
          }}
          viewBox='0 0 30 30'
          >
          <g transform='translate(0 -289.06)'>
            <path
              transform='translate(0 289.06)'
              d='m4.4668 14c-0.030724 0.32965-0.050781 0.66211-0.050781
              1s0.020057 0.67035 0.050781 1h21.066c0.030725-0.32965
              0.050781-0.66211 0.050781-1s-0.020056-0.67035-0.050781-1h-21.066z'
            />
          </g>
        </svg>
      );
    }

    return (
      <svg
        class={{
          'nv-checkbox-icon': true,
          checked : this.checked || this.indeterminate,
        }}
      viewBox='0 0 30 30'
      >
        <g transform='translate(0 -283.15)'>
          <path
            transform='translate(0 283.15)'
            d='m27 6.0195-15.932 18.988-8.0684-6.7695v2.6094l6.7832
            5.6914c0.83553 0.70109 2.1173 0.58944 2.8184-0.24609l14.398-17.16v-3.1133z'
          />
        </g>
      </svg>
    );
  }

  render() {
    const icon = this.makeIcon();
    return (
      <Host
        class={{
          'nv-checkbox' : true,
          'nv-checkbox-disabled' : this.disabled,
          'nv-checkbox-indeterminate': this.indeterminate,
        }}
      onClick={ this.updateCheck }
      >
        <div
          class={{
            'nv-checkbox-container': true,
            'nv-checkbox-checked' : this.checked || this.indeterminate,
          }}
        >
          <input
            onFocus={this.onFocus}
            onBlur={this.onBlur}
            ref={ el => this.el = el! }
            type='checkbox'
            name={ this.name }
            value={ this.value }
            checked={ this.checked }
          />
          { icon }
        </div>
      </Host>
    );
  }
}
