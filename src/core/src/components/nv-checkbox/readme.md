# nv-checkbox



<!-- Auto Generated Below -->


## Properties

| Property             | Attribute       | Description                                                      | Type                  | Default     |
| -------------------- | --------------- | ---------------------------------------------------------------- | --------------------- | ----------- |
| `checked`            | `checked`       | If `true` the checkbox is selected.                              | `boolean`             | `false`     |
| `disabled`           | `disabled`      | If `true`, the user cannot interact with the checkbox.           | `boolean`             | `false`     |
| `indeterminate`      | `indeterminate` | If `true` the checkbox will have a indeterminate visual.         | `boolean`             | `false`     |
| `name` _(required)_  | `name`          | The name of the control, which is submitted with the form data   | `string`              | `undefined` |
| `value` _(required)_ | `value`         | The value of the control, which is submitted with the form data. | `string \| undefined` | `undefined` |


## Events

| Event      | Description                                     | Type                                     |
| ---------- | ----------------------------------------------- | ---------------------------------------- |
| `nvBlur`   | Emitted when the input loses focus.             | `CustomEvent<FocusEvent>`                |
| `nvChange` | Emitted when the value of the input is changed. | `CustomEvent<CheckboxChangeEventDetail>` |
| `nvFocus`  | Emitted when the input has focus.               | `CustomEvent<FocusEvent>`                |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
