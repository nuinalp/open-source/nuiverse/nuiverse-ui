/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  h,
  Element,
  Prop,
  Host,
  Method,
  State,
}                                  from '@stencil/core';
import { runNextAnimationFrame }   from '../../utils';

@Component({
  tag: 'nv-toast',
  styleUrl: './style.scss',
  shadow: true,
})
export class NvToast {
  public progress!: HTMLDivElement;
  /**
   * Reference to the host element
   */
  @Element() el!: HTMLElement;

  //
  // State
  //

  @State() hovered: boolean = false;
  @State() closed: boolean = false;
  @State() hasIcon: boolean = false;

  /**
   * The text header for the toast.
   */

  @Prop() header?: string;

  /**
   * If `true`, the toast will display the close button.
   */

  @Prop() showClose: boolean = false;

  /**
   * If `true`, the toast will display the actions.
   */

  @Prop() showActions: boolean = false;

  /**
   * The display duration in milliseconds of the toast.
   */

  @Prop() duration: number = 4000;

  /**
   * If `true`, after the `duration`, the toast will be closed.
   */

  @Prop() autoClose: boolean = true;

  /**
   * If `true`, the toast will be closed on click.
   */

  @Prop() closeOnClick: boolean = true;

  /**
   * The timer to close the toast.
   */
  timer!: ReturnType<typeof setTimeout> | null;

  //
  // Lifecycles
  //

  componentDidLoad() {
    this.progress
      ?.addEventListener('animationend', this.handleAnimEnd);
    this.el.shadowRoot
      ?.querySelector('.nv-toast')
      ?.addEventListener<any>('transitionend', this.handleTranEnd);
    this.hasIcon = ((this.el.shadowRoot?.querySelector('slot[name="icon"]') as HTMLSlotElement)
      .assignedNodes()).length > 0;
  }

  //
  // Methods
  //

  /**
   * Close the toast.
   */
  @Method()
  async close(): Promise<void> {
    const el = this.el.shadowRoot?.querySelector('.nv-toast');
    el?.classList.add('nv-toast-hiding');
    el?.classList.remove('nv-toast-opened');
  }

  /**
   * @internal
   */
  @Method()
  async destroy(el: HTMLDivElement): Promise<void> {
    el.remove();
  }

  private applySize(): void {
    const el = this.el.shadowRoot?.querySelector('.nv-toast') as HTMLDivElement;
    const content = this.el.shadowRoot?.querySelector('content') as HTMLDivElement;
    const actions = this.el.shadowRoot?.querySelector('.nv-toast-actions') as HTMLDivElement;

    el.style.width = `${this.el.offsetWidth}px`;

    if (content) content.style.minWidth = `${content.offsetWidth}px`;
    if (actions) actions.style.minWidth = `${actions.offsetWidth}px`;
  }

  /**
   * Open the toast.
   */
  @Method()
  async open(): Promise<void> {
    runNextAnimationFrame(() => {
      this.closed = true;
      const el = this.el.shadowRoot?.querySelector('.nv-toast');
      el?.classList.remove('nv-toast-closed');
      el?.classList.remove('nv-toast-closing');

      // Wait a frame once display is no longer "none", to establish basis for the animation
      runNextAnimationFrame(() => {
        this.applySize();

        // no operation during flying and hiding
        if (el?.classList.contains('nv-toast-flying') ||
            el?.classList.contains('nv-toast-hiding')) {
          return;
        }

        el?.classList.add('nv-toast-flying');
      });
    });
  }

  private renderCloseIcon(): HTMLDivElement | null {
    return (
      <nv-toast-button>
        <svg width='30' height='30' version='1.1' viewBox='0 0 30 30'>
          <g transform='translate(0 -289.06)'>
            <path
              transform='translate(0 289.06)'
              d='m7.2891 5.875c-0.51103 0.43066-0.98384 0.90263-1.416 1.4121l7.7129
              7.7129-7.7129 7.7129c0.43217 0.50948 0.90498 0.98145 1.416 1.4121l7.7109-7.7109
              7.7109 7.7109c0.51103-0.43066 0.98384-0.90263 1.416-1.4121l-7.7129-7.7129
              7.7129-7.7129c-0.43217-0.50948-0.90498-0.98145-1.416-1.4121l-7.7109 7.7109z'
              stroke-width='1.1583'/>
          </g>
        </svg>
      </nv-toast-button>
    );
  }

  /**
   * Close the toast
   */
  private closeClick = () => {
    if (this.closeOnClick) {
      this.close();
    }
  }

  private onHover = () => {
    this.hovered = true;
    this.resetTimer();
  }

  private onLeave = () => {
    this.hovered = false;
    this.setTimer();
  }

  /**
   * Set a timer to auto close the toast
   */
  private setTimer = () => {
    if (!this.hovered && this.autoClose) {
      this.timer = setTimeout(() => !this.hovered && this.close(), this.duration);
    }
  }

  /**
   * Stop the Timeout to not close the toast
   */
  private resetTimer = (): void => {
    if (this.timer) {
      clearTimeout(this.timer);
    }
  };


  /**
   * Used to detect animation's end and close the toast.
   */
  protected handleAnimEnd = () => {
    if (this.hovered) {
      return;
    }
    this.close();
  }

  /**
   * Used to detect the transition's end for the display toast.
   */
  protected handleTranEnd = (evt: TransitionEvent) => {
    const target = evt.target as HTMLDivElement;

    if (target && evt.type === 'transitionend') {
      if (target.classList.contains('nv-toast-flying')) {
        requestAnimationFrame(() => {
          target.classList.remove('nv-toast-flying');
          target.classList.add('nv-toast-opened');
          this.setTimer();
        });
      }

      // go back to the initial state after hiding
      if (target.classList.contains('nv-toast-hiding') ||
      target.classList.contains('nv-toast-closing')) {
        requestAnimationFrame(() => {
          target.classList.remove('nv-toast-opened');
          target.classList.remove('nv-toast-opening');
          target.classList.remove('nv-toast-closing');
          target.classList.remove('nv-toast-flying');
          target.classList.remove('nv-toast-hiding');
          target.classList.add('nv-toast-closed');
          this.destroy(target);
        });
      }
    }
  }

  private renderDefaultIcon() {
    if (!this.hasIcon) {
      return (
          <svg width='30' height='30'  viewBox='0 0 30 30' >
            <g>
              <path d='m21.896 5.0566-0.875 1.7832c1.6067 0.78762 2.9115 2.1908 3.5391
              4.0254 0.62757 1.8346
              0.4556 3.7449-0.33203 5.3516l1.7832 0.87305c1.0148-2.07 1.2439-4.5252
              0.44141-6.8711-0.8025-2.3459-2.4866-4.1473-4.5566-5.1621zm-5.0625
              0.86914-13.561 9.7617c-0.21453 1.2458
              0.048882 2.3909 0.63867 3.4668l3.4395-0.02734 1.4844 5.4336 4.082 0.0059-1.4141-5.4395
              9.2812-0.41406c-0.56661-3.3569-1.6327-6.4518-3.9512-12.787zm3.7422 1.8242-0.87695
              1.7852c0.9173 0.44968 1.664 1.2501 2.0234 2.3008 0.35943 1.0507 0.26026 2.1413-0.18945
              3.0586l1.7852 0.875c0.67683-1.3806 0.82928-3.018
              0.29492-4.5801-0.53436-1.5621-1.6565-2.7626-3.0371-3.4395z'/>
            </g>
          </svg>
      );
    }
  }

  render() {
    const closeIcon = this.renderCloseIcon();
    const defaultIcon = this.renderDefaultIcon();

    return (
      <Host>
        <div
          class={{
            'nv-toast': true,
            'nv-toast-closed': !this.closed,
            'nv-toast-hovered' : this.hovered,
          }}
          onClick={ this.closeClick }
          onMouseEnter={ this.onHover }
          onMouseLeave={ this.onLeave }
        >
          <div class='nv-toast-icon'>
            {defaultIcon}
            <slot name='icon' />
          </div>
          <content>
            <h6>{ this.header }</h6>
            <slot />
          </content>
          {
            (this.showActions || this.showClose) &&
            <div class='nv-toast-actions'>
              <slot name='actions' />
              {this.showClose && closeIcon}
            </div>
          }
        </div>
      </Host>
    );
  }
}
