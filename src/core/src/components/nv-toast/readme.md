# nv-toast



<!-- Auto Generated Below -->


## Properties

| Property       | Attribute        | Description                                                | Type                  | Default     |
| -------------- | ---------------- | ---------------------------------------------------------- | --------------------- | ----------- |
| `autoClose`    | `auto-close`     | If `true`, after the `duration`, the toast will be closed. | `boolean`             | `true`      |
| `closeOnClick` | `close-on-click` | If `true`, the toast will be closed on click.              | `boolean`             | `true`      |
| `duration`     | `duration`       | The display duration in milliseconds of the toast.         | `number`              | `4000`      |
| `header`       | `header`         | The text header for the toast.                             | `string \| undefined` | `undefined` |
| `showActions`  | `show-actions`   | If `true`, the toast will display the actions.             | `boolean`             | `false`     |
| `showClose`    | `show-close`     | If `true`, the toast will display the close button.        | `boolean`             | `false`     |


## Methods

### `close() => Promise<void>`

Close the toast.

#### Returns

Type: `Promise<void>`



### `open() => Promise<void>`

Open the toast.

#### Returns

Type: `Promise<void>`




## Dependencies

### Depends on

- [nv-toast-button](../nv-toast-button)

### Graph
```mermaid
graph TD;
  nv-toast --> nv-toast-button
  style nv-toast fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
