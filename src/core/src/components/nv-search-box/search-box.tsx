/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, Prop, Event, EventEmitter, Element, State, h, Host, Method } from '@stencil/core';
import { InputChangeEventDetail } from '../../interface';
@Component({
  tag: 'nv-search-box',
  styleUrl: './style.scss',
  shadow: true,
})
export class NvSearchBox {

  input!: HTMLInputElement;
  ripple!: HTMLDivElement;

  /**
   * Reference to the host element
   */
  @Element() el!: HTMLNvTextFieldElement;

  //
  // State
  //

  @State() inputFocus: boolean = false;
  @State() rippleAnimFinish: boolean = false;
  @State() timer!: ReturnType<typeof setTimeout> | null | null;

  //
  // Props
  //

  /**
   * The name of the input, which is submitted with the form data.
   */
  @Prop() name?: string | undefined;

  /**
   * The label for the input.
   */
  @Prop() label?: string | undefined;

  /**
   * The initial value for the input.
   */
  @Prop() value?: string;

  /**
   * If `true`, the text-field will have a naked style.
   */
  @Prop() naked: boolean = false;

  /**
   * Set the amount of time, in milliseconds, to wait to trigger the nvInputChangeChange event after each keystroke.
   */
  @Prop() debounce = 0;

  /**
   * If `true`, the loading will be display
   */
  @Prop({ mutable: true }) loading = false;

  //
  // Events
  //

  /**
   * Emitted when the value of the input is changed.
   */
  @Event() nvChange!: EventEmitter<InputChangeEventDetail>;

  /**
   * Emitted when the input loses focus.
   */
   @Event() nvBlur!: EventEmitter<FocusEvent>;

   /**
    * Emitted when the input has focus.
    */
   @Event() nvFocus!: EventEmitter<FocusEvent>;
 
  /**
   * Emitted when the press the enter.
   */
  @Event({
    bubbles: true,
    composed: true,
  }) nvEnter!: EventEmitter<InputChangeEventDetail>;

  //
  // Lifecycles
  //

  componentDidLoad() {
    this.ripple.addEventListener('transitionend', (e: Event) => this.watchEvents(e));
  }

  //
  // Public methods
  //

  /**
   * Set focus in the input
   */
  @Method()
  async setFocus(): Promise<void> {
    const input = this.el.shadowRoot?.querySelector('input');
    if (input) input.focus();
  }

  /**
   * Clear input content.
   */
  @Method()
  async clear(): Promise<void> {
    const input = this.el.shadowRoot?.querySelector('input');
    if (input) {
      input.value = '';
      this.nvChange.emit({ value: '' });
    }
  }

  //
  // Private methods
  //

  private makeLabel() {

    let activeStatus: boolean = false;
    if (this.label) {

      if (this.inputFocus || this.value || this.inputFocus && !this.value) {
        activeStatus = true;
      }

      return (
        <span class={{
          ['nv-search-box-label']: true,
          active: activeStatus,
        }}>
          { this.label }
        </span>
      );
    }
  }

  private makeInput(): HTMLInputElement {
    return (
      <input
        type='input'
        onFocus={ (ev) => this.onFocus(ev) }
        onBlur={ (ev) => this.onBlur(ev) }
        onInput={ event => this.handleChange(event)}
        onKeyDown={ event => this.handleKeyDown(event) }
        value={ this.value }
        ref={ el => this.input = el! }
      />
    );
  }

  private handleChange(event: any): void {
    const newValue = event.target.value;
    if (this.timer) {
      if (this.timer) clearTimeout(this.timer);
    }

    this.timer = setTimeout(() => {
      this.value = newValue;
      this.nvChange.emit({ value: newValue });

      if (this.timer) {
        if (this.timer) clearTimeout(this.timer);
      }

      this.timer = null;
    }, Number(this.debounce));
  }

  private onFocus = (ev: FocusEvent): void => {
    this.inputFocus = true;
    this.nvFocus.emit(ev);
  }

  private onBlur = (ev: FocusEvent): void => {
    this.inputFocus = false;
    this.rippleAnimFinish = false;
    this.nvBlur.emit(ev);
  }

  private handleKeyDown(event: KeyboardEvent): void {
    if (event.key === 'Enter') {
      this.nvEnter.emit({ value: this.value });
    }
  }

  private makeIcon(): SVGElement {
    return (
      <svg
        width='30'
        height='30'
        viewBox='0 0 30 30'
        class={{
          'nv-search-box-icon': true,
          'show-icon': !this.loading,
        }}
      >
        <g transform='translate(0 -289.06)'>
          <path
            // tslint:disable-next-line: max-line-length
            d='m6.5506 295.61c-0.83968 0.83968-1.5215 1.7759-2.0592 2.769-1.72 3.7014-1.0689 8.245 1.9791 11.293 3.5354 3.5354 9.0833 3.8518 12.993 0.97227l2.8505 2.8505c0.39186-0.30456 0.77514-0.62598 1.1352-0.98609 0.3601-0.3601 0.68152-0.74338 0.98608-1.1352l-2.8505-2.8505c2.8795-3.9097 2.5631-9.4577-0.97227-12.993-3.048-3.048-7.5916-3.6991-11.293-1.9791-0.99313 0.53766-1.9294 1.2195-2.769 2.0592zm1.3341 1.3341c3.1326-3.1326 8.1811-3.1326 11.314 0 3.1326 3.1326 3.1326 8.1811 0 11.314s-8.1811 3.1326-11.314 0c-3.1326-3.1326-3.1326-8.1811 0-11.314z'/>
        </g>
      </svg>
    );
  }

  private watchEvents(evt: any): void {
    if (evt.type === 'transitionend') {

      if (evt.propertyName !== 'width') {
        return;
      }

      if (this.inputFocus) {
        this.rippleAnimFinish = true;
      }
    }
  }

  private renderLoading = () => {
    return (
      <div
        class={{
          'loader-search-box': true,
          'show-icon': this.loading,
        }}
      >
        <nv-loader size='small' paused={!this.loading}/> 
      </div>
    );
  }

  render() {
    const input = this.makeInput();
    const label = this.makeLabel();
    const icon = this.makeIcon();

    return (
      <Host class={{
        'nv-search-box': true,
        'nv-text-field': true,
        'nv-search-box-focus': this.inputFocus,
        'nv-search-box-ripple-finish': this.rippleAnimFinish,
        'nv-text-field-naked': this.naked,
      }}>
        <div class='nv-text-field-container'>
          <div class='nv-text-field-elements'>
            <div class='nv-text-field-ripple-focus' ref={ el => this.ripple = el! }></div>
            {/* Input */}
            { input }

            {/* Label */}
            { label }

            { icon }
            { this.renderLoading() }
          </div>
        </div>
      </Host>
    );
  }
}
