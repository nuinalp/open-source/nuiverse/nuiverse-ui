# nv-search-box



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description                                                                                                     | Type                  | Default     |
| ---------- | ---------- | --------------------------------------------------------------------------------------------------------------- | --------------------- | ----------- |
| `debounce` | `debounce` | Set the amount of time, in milliseconds, to wait to trigger the nvInputChangeChange event after each keystroke. | `number`              | `0`         |
| `label`    | `label`    | The label for the input.                                                                                        | `string \| undefined` | `undefined` |
| `loading`  | `loading`  | If `true`, the loading will be display                                                                          | `boolean`             | `false`     |
| `naked`    | `naked`    | If `true`, the text-field will have a naked style.                                                              | `boolean`             | `false`     |
| `name`     | `name`     | The name of the input, which is submitted with the form data.                                                   | `string \| undefined` | `undefined` |
| `value`    | `value`    | The initial value for the input.                                                                                | `string \| undefined` | `undefined` |


## Events

| Event      | Description                                     | Type                                  |
| ---------- | ----------------------------------------------- | ------------------------------------- |
| `nvBlur`   | Emitted when the input loses focus.             | `CustomEvent<FocusEvent>`             |
| `nvChange` | Emitted when the value of the input is changed. | `CustomEvent<InputChangeEventDetail>` |
| `nvEnter`  | Emitted when the press the enter.               | `CustomEvent<InputChangeEventDetail>` |
| `nvFocus`  | Emitted when the input has focus.               | `CustomEvent<FocusEvent>`             |


## Methods

### `clear() => Promise<void>`

Clear input content.

#### Returns

Type: `Promise<void>`



### `setFocus() => Promise<void>`

Set focus in the input

#### Returns

Type: `Promise<void>`




## Dependencies

### Used by

 - [nv-select-wrapper](../nv-select-wrapper)

### Depends on

- [nv-loader](../nv-loader)

### Graph
```mermaid
graph TD;
  nv-search-box --> nv-loader
  nv-select-wrapper --> nv-search-box
  style nv-search-box fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
