# nv-button



<!-- Auto Generated Below -->


## Properties

| Property         | Attribute          | Description                                                                                               | Type                                                                          | Default     |
| ---------------- | ------------------ | --------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------- | ----------- |
| `block`          | `block`            | If `true`, this property will change the width of the button for full.                                    | `boolean`                                                                     | `false`     |
| `circle`         | `circle`           | If `true`, this property will apply a circle design for the button.                                       | `boolean`                                                                     | `false`     |
| `color`          | `color`            | The button color.                                                                                         | `"danger" \| "primary" \| "secondary" \| "success" \| "warning" \| undefined` | `undefined` |
| `customDropDown` | `custom-drop-down` | If `true` the dropdown will be have a transparent backgroud and borders.                                  | `boolean`                                                                     | `false`     |
| `danger`         | `danger`           | If `true`, the button will be apply the danger theme color in text elements                               | `boolean`                                                                     | `false`     |
| `dataFor`        | `data-for`         | The data's action for the button, this attribute is used to find Nuiverse components and display or hide. | `string \| undefined`                                                         | `undefined` |
| `details`        | `details`          | If `true` the button will be have a list style to use in like a dropdown **Don't use it directly**        | `boolean`                                                                     | `false`     |
| `disabled`       | `disabled`         | The button enable status.                                                                                 | `boolean`                                                                     | `false`     |
| `disabledRipple` | `disabled-ripple`  | The button ripple effect enable status                                                                    | `boolean`                                                                     | `false`     |
| `outline`        | `outline`          | If `true`, this property will apply an outline style for the button.                                      | `boolean`                                                                     | `false`     |
| `primary`        | `primary`          | If `true`, the button will be apply the primary theme color in text elements                              | `boolean`                                                                     | `false`     |
| `size`           | `size`             | This property specifies the size of the button.                                                           | `"big" \| "large" \| "medium" \| "small" \| undefined`                        | `undefined` |
| `text`           | `text`             | The text style                                                                                            | `boolean`                                                                     | `false`     |
| `textColor`      | `text-color`       | The text color of the button.                                                                             | `"danger" \| "primary" \| "secondary" \| "success" \| "warning" \| undefined` | `undefined` |
| `type`           | `type`             | Type of the button.                                                                                       | `"button" \| "reset" \| "submit" \| undefined`                                | `'button'`  |


## Dependencies

### Used by

 - [nv-code](../nv-code)
 - [nv-code-source](../nv-code-source)
 - [nv-dialog-action](../nv-dialog-action)
 - [nv-dialog-alert](../nv-dialog-alert)
 - [nv-dialog-confirm](../nv-dialog-confirm)
 - [nv-dialog-prompt](../nv-dialog-prompt)
 - [nv-text-field](../nv-text-field)
 - [nv-upload](../nv-upload)

### Graph
```mermaid
graph TD;
  nv-code --> nv-button
  nv-code-source --> nv-button
  nv-dialog-action --> nv-button
  nv-dialog-alert --> nv-button
  nv-dialog-confirm --> nv-button
  nv-dialog-prompt --> nv-button
  nv-text-field --> nv-button
  nv-upload --> nv-button
  style nv-button fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
