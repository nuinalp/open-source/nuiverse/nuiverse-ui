/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, Prop, h, Element, Host, State } from '@stencil/core';
import { PredefinedButtonType, PredefinedGlobalSize, Color }  from '../../interface';

interface IcustomElement extends HTMLElement{
  toggle?: () => void;
}

@Component({
  tag: 'nv-button',
  styleUrl: './style.scss',
  shadow: true,
})
export class NvButton {

  /**
   *  Reference to the host element
   */
  @Element() el!: HTMLElement;


  //
  // State
  //

  @State() hasStartSlot: boolean = false;
  @State() hasEndtSlot: boolean = false;
  @State() detailsOpened: boolean = false;
  @State() hasDetails: boolean = false;
  @State() displayStartSlot = false;
  @State() displayEndSlot = false;
  @State() displayDefaultSlot = false;

  //
  // Props
  //

  /**
   * The text color of the button.
   */
  @Prop() textColor?: Color;

  /**
   * If `true`, the button will be apply the primary theme color in text elements
   */
  @Prop() primary: boolean = false;

  /**
   * If `true`, the button will be apply the danger theme color in text elements
   */
  @Prop() danger: boolean = false;

  /**
   * The button color.
   */
  @Prop() color?: Color;

  /**
   * This property specifies the size of the button.
   */
  @Prop() size?: PredefinedGlobalSize;

  /**
   * Type of the button.
   */
  @Prop() type?: PredefinedButtonType = 'button';

  /**
   * The text style
   */
  @Prop() text: boolean = false;

  /**
   * If `true`, this property will change the width of the button for full.
   */
  @Prop() block: boolean = false;

  /**
   * If `true`, this property will apply a circle design for the button.
   */
  @Prop() circle: boolean = false;

  /**
   * If `true`, this property will apply an outline style for the button.
   */
  @Prop() outline: boolean = false;

  /**
   * The button enable status.
   */
  @Prop() disabled: boolean = false;

  /**
   * The button ripple effect enable status
   */
  @Prop() disabledRipple: boolean = false;

  /**
   * The data's action for the button, this attribute is used to find Nuiverse components
   * and display or hide.
   */
  @Prop() dataFor?: string;

  /**
   * If `true` the button will be have a list style to use in like a dropdown
   * **Don't use it directly**
   */
  @Prop() details: boolean = false;

  /**
   * If `true` the dropdown will be have a transparent backgroud and borders.
   */
  @Prop() customDropDown: boolean = false;

  //
  // Lifecycles
  //

  componentDidLoad() {
    this.hasStartSlot = ((this.el?.shadowRoot?.querySelector('slot[name="start"]') as HTMLSlotElement).assignedElements()).length > 0;
    this.hasEndtSlot = ((this.el?.shadowRoot?.querySelector('slot[name="end"]') as HTMLSlotElement).assignedElements()).length > 0;
  }

  private actionData(): void {
    const data = this.dataFor;
    if (data) {
      const element = document.querySelector(data) as IcustomElement;
      if (element) {
  
        // Dialogs
        if (element.tagName.match(/NV-DIALOG/)) {
          if (element && element.toggle) {
            element.toggle();
          }
        }
      }
    }
  }

  private addDetails() {
    const btns = (
      this.el.shadowRoot?.querySelector('slot[name="details"]') as HTMLSlotElement
    ).assignedNodes() as HTMLElement[];

    if (btns.length > 0) {
      this.hasDetails = true;
      btns.forEach((btn) => {
        btn.setAttribute('details', 'true');
      });
    }
  }

  private addEvent (): void {
    if (!this.disabled) {
      window.addEventListener('keydown', this.onKeyDown);
      window.addEventListener('mouseup', this.onStart);
    }
  }

  private removeEvent(): void {
    if (!this.disabled) {
      window.removeEventListener('keydown', this.onKeyDown);
      window.removeEventListener('mouseup', this.onStart);
    }
  }

  private onKeyDown = (e: KeyboardEvent): void => {
    e.preventDefault();

    if (e.key === 'Escape') {
      this.closeDetails();
    }
  }

  private onStart = (e: MouseEvent): void  => {
    e.preventDefault();
    const el = e.target as HTMLElement;
    if (this.el !== el) this.closeDetails();
  }

  private openDetails = (e?: MouseEvent) => {
    e?.preventDefault();
    const el = e?.target as HTMLElement;
    if (this.detailsOpened) return this.closeDetails();
    if (this.detailsOpened || this.el.shadowRoot?.contains(el))  return;

    this.addEvent();
    this.detailsOpened = true;
  }

  private getOnClick() {
    if (this.dataFor) return this.actionData.bind(this);
    if (this.hasDetails) return this.openDetails.bind(this);

    return null;
  }

  private closeDetails = (e?: MouseEvent) => {
    e?.preventDefault();
    e?.stopPropagation();

    this.removeEvent();
    this.detailsOpened = false;
  }

  render() {
    return (
      <Host
        class={{
          'nv-button': true,
          'nv-button-block': this.block || this.details,
          'details-open': this.detailsOpened,
        }}
        onMouseUp={ this.getOnClick() }
      >
        <button
          class={{
            'nv-button-native': true,
            'nv-button-small': this.size === 'small' ? true : false,
            'nv-button-medium': this.size === 'medium' ? true : false,
            'nv-button-large': this.size === 'large' ? true : false,
            'nv-button-big': this.size === 'big' ? true : false,
            'nv-button-primary': this.primary && !this.text,
            'nv-button-text-primary': this.primary && this.text,
            'nv-button-text-danger': this.danger && this.text,
            'nv-button-text': this.text,
            'nv-button-circle': this.circle,
            'nv-button-outline': this.outline,
            'nv-button-disabled': this.disabled,
            'nv-button-start-slot': this.hasStartSlot,
            'nv-button-end-slot': this.hasEndtSlot,
            'nv-button-details': this.details,
          }}
          type={ this.type }
          disabled={this.disabled}
        >
            <div class='nv-button-slot-start'>
              <slot name='start' />
            </div>
            <div class='nv-button-slot-content'>
              <slot />
            </div>
            <div class='nv-button-slot-end'>
              <slot name='end' />
            </div>
          </button>
          <div
            class={{
              'details-slot': true,
              'custom-drop-down': this.customDropDown,
            }}
          >
            <slot name='details' onSlotchange={() => this.addDetails()}/>
          </div>
      </Host>
    );
  }
}
