/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  h,
  Element,
  Prop,
  State,
  Listen,
  Method, 
  Host}          from '@stencil/core';

@Component({
  tag: 'nv-collapse-item',
  shadow: false,
})
export class NvCollapseItem {

  /**
   * Reference to the host element
   */
  @Element() el!: HTMLNvCollapseItemElement;

  //
  // State
  //

  @State() open: boolean                        = false;
  @State() contentScrollHeight: string | number = 0;
  @State() minHeight: string                    = '0px';
  @State() rootEl: HTMLNvCollapseElement | null = null;

  //
  // Lifecycles
  //

  componentWillLoad () {
    this.rootEl = this.el.parentElement?.parentElement as HTMLNvCollapseElement;
  }

  //
  // Props
  //

  /**
   * This props is used by `nv-collapse` to apply an accordion login.
   * @intenal
   */
  @Prop() accordion: boolean  = false;

  /**
   * If `true`, the user cannot interact with the collapse item.
   */
  @Prop() disabled: boolean   = false;

  //
  // Events
  //

  @Listen('resize', { target: 'window' })
  handleResize() {
    this.changeHeight();
  }

  //
  // Methods
  //

  private changeHeight() {
    const height = this.contentScrollHeight;
    if (this.minHeight !== '0px' && height) {
      this.minHeight = `${height}px`;
    }
  }

  /**
   * Call this method to close this collapse.
   */
  @Method()
  async close() {
    this.minHeight = '0px';
    this.open = false;
  }

  /**
   * Call this method to open or close this collapse.
   */
  @Method()
  async toggle() {

    if (this.disabled) {
      return;
    }

    if (this.accordion) {
      this.rootEl?.closeAllItems(this.el);
    }

    const height = this.el.querySelector('.nv-collapse-item-container')?.scrollHeight;

    if (this.minHeight === '0px' && height) {
      this.minHeight = `${height + 32}px`;
      this.minHeight = `${height + 32}px`;
      this.open = true;
    } else {
      this.minHeight = `0px`;
      this.open = false;
    }
  }

  render() {
    return (
      <Host class={{
        'nv-collapse-item': true,
        disabled: this.disabled,
      }}>
        {/* Header */}
        <header onClick={ () => this.toggle() }>
          <div class='header-container nv-h6'>
            <slot name='header'/>
          </div>
        </header>
        {/* Content */}
        <div
          class={{
            'nv-collapse-item-container': true,
            show: this.open,
          }}
          style={{ minHeight: this.minHeight }}>
          <slot name='content' />
        </div>
      </Host>
    );
  }
}
