# nv-collapse-item



<!-- Auto Generated Below -->


## Properties

| Property    | Attribute   | Description                                                      | Type      | Default |
| ----------- | ----------- | ---------------------------------------------------------------- | --------- | ------- |
| `accordion` | `accordion` | This props is used by `nv-collapse` to apply an accordion login. | `boolean` | `false` |
| `disabled`  | `disabled`  | If `true`, the user cannot interact with the collapse item.      | `boolean` | `false` |


## Methods

### `close() => Promise<void>`

Call this method to close this collapse.

#### Returns

Type: `Promise<void>`



### `toggle() => Promise<void>`

Call this method to open or close this collapse.

#### Returns

Type: `Promise<void>`




----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
