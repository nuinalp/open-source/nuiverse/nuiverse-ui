/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, Prop, h, Host, Element, State } from '@stencil/core';
@Component({
  tag: 'nv-table',
  shadow: false,
})
export class NvTable {
  //
  // Reference to the Host element.
  //
  @Element() el!: HTMLNvTableElement;

  //
  // State
  //

  @State() tds: HTMLTableDataCellElement[] = [];

  /**
   * If `true`, the table will apply a striped style.
   */

  @Prop() striped: boolean = false;

  /**
   * If `true`, the table will apply a dark theme.
   */

  @Prop() dark: boolean = false;

  /**
   * If `true`, the table head will apply a dark style.
   */

  @Prop() darkHead: boolean = false;

  /**
   * If `true`, the table head will apply a light style.
   */

  @Prop() lightHead: boolean = false;

  /**
   * If `true`, the table will have apply bordered style.
   */

  @Prop() bordered: boolean = false;

  //
  // Lifecycles
  //

  componentDidLoad() {
    this.makeThead();
    this.makeTbody();
  }

  private makeThead(): void {
    const fragment = document.createDocumentFragment();
    this.el.querySelectorAll('nv-thead').forEach((thead) => {
      const nativeTr = document.createElement('tr');
      const nvTd = thead.querySelectorAll('nv-th');
      nvTd.forEach((nvTd) => {
        const nativeTd: HTMLTableDataCellElement = document.createElement('th');
        nativeTd.appendChild(nvTd);
        nativeTr.appendChild(nativeTd);
      });
      fragment.appendChild(nativeTr);
    });
    this.el.querySelector('table thead')?.appendChild(fragment);
  }

  private makeTbody(): void {
    const fragment = document.createDocumentFragment();
    this.el.querySelectorAll('nv-tr').forEach((thead) => {
      const nativeTr = document.createElement('tr');
      const nvTd = thead.querySelectorAll('nv-td');
      nvTd.forEach((nvTd) => {
        const nativeTd: HTMLTableDataCellElement = document.createElement('td');
        nativeTd.appendChild(nvTd);
        nativeTr.appendChild(nativeTd);
      });
      fragment.appendChild(nativeTr);
    });
    this.el.querySelector('table tbody')?.appendChild(fragment);
  }

  render() {
    return (
      <Host class={{
        'nv-table': true,
        'nv-table-striped': this.striped,
        'nv-table-dark': this.dark,
        'nv-table-thead-dark': this.darkHead,
        'nv-table-thead-light': this.lightHead,
        'nv-table-bordered': this.bordered,
      }}>
        <table>
          <thead></thead>
          <tbody></tbody>
        </table>
        <div class='v-table'>
          <slot></slot>
        </div>
      </Host>
    );
  }
}
