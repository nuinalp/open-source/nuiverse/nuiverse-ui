# nv-table



<!-- Auto Generated Below -->


## Properties

| Property    | Attribute    | Description                                          | Type      | Default |
| ----------- | ------------ | ---------------------------------------------------- | --------- | ------- |
| `bordered`  | `bordered`   | If `true`, the table will have apply bordered style. | `boolean` | `false` |
| `dark`      | `dark`       | If `true`, the table will apply a dark theme.        | `boolean` | `false` |
| `darkHead`  | `dark-head`  | If `true`, the table head will apply a dark style.   | `boolean` | `false` |
| `lightHead` | `light-head` | If `true`, the table head will apply a light style.  | `boolean` | `false` |
| `striped`   | `striped`    | If `true`, the table will apply a striped style.     | `boolean` | `false` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
