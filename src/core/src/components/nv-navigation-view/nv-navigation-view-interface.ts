/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

export interface NvNavViewItemsDidChange {
  item: HTMLNvNavigationViewItemElement;
  items: HTMLNvNavigationViewItemsElement;
}

export interface NvNavViewItemDidChange {
  item: HTMLNvNavigationViewItemElement;
}

export interface NvNavViewMainItemActive {
  el: HTMLNvNavigationViewItemsElement;
}
