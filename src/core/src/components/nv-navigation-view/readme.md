# nv-navigation-view



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description                                 | Type                           | Default     |
| ---------- | ---------- | ------------------------------------------- | ------------------------------ | ----------- |
| `mode`     | `mode`     | The navigation style of the NavigationView. | `"left" \| "top" \| undefined` | `'left'`    |
| `position` | `position` | The position of the NavigationView.         | `"fixed" \| undefined`         | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
