/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  Prop,
  h,
  Host,
  State,
  Element,
  Event,
  EventEmitter,
} from '@stencil/core';
import {
  NvNavViewItemsDidChange,
} from '../../interface';

@Component({
  tag: 'nv-navigation-view',
  shadow: false,
})
export class NvNavigationView {
  headerParent!: HTMLElement | null;
  search!: HTMLNvSearchBoxElement;
  /**
   * Reference to the element host.
   */
  @Element() el!: HTMLNvNavigationViewElement;

  //
  // State
  //

  @State() navItems: HTMLNvNavigationViewItemsElement[] = [];
  @State() nvNavHeader!: HTMLNvNavigationViewHeaderElement | null;
  @State() initialWidth!: number;
  @State() responsive: boolean = false;
  @State() items!: NodeListOf<Element>;
  @State() itemsCenter!: NodeListOf<Element>;
  @State() mnMbActive: boolean = false;
  @State() heightMnMb: number = 0;
  @State() hostHeight: number = 0;

  /**
   * The navigation style of the NavigationView.
   */
  @Prop() mode?: 'top' | 'left' = 'left';

  /**
   * The position of the NavigationView.
   */
  @Prop() position?: 'fixed' | undefined;

  //
  // Lifecycles
  //

  componentDidLoad() {
    const nvNames: string = [
      'nv-navigation-view-header',
      'nv-navigation-view-items',
      'nv-navigation-view-item',
      'nv-navigation-view-separator',
    ].join(', ');

    this.navItems = Array.from(this.el.querySelectorAll(nvNames));

    this.nvNavHeader = this.el.querySelector('nv-navigation-view-header');
    this.initSelect().then(() => {
      this.nvNavViewViewWillLoad.emit();
      this.getDimensions().then(() => {
        this.resize();
        this.getMnMbScrollHeight();
      });
      this.addEvents();
    });

    if (this.position === 'fixed' && this.mode === 'top') {
      const { height } = window.getComputedStyle(this.el);
      const body = document.querySelector('body');
      if (body) {
        body.style.paddingTop = height;
      }
    }

    const headerParent = this.nvNavHeader?.parentElement;

    if (headerParent) {
      this.headerParent = headerParent;
    }


    const search = this.el.querySelector('nv-search-box');
    if (search) {
      this.search = search;
    }
  }

  //
  // Events
  //

  /**
   * Emits when the navigationViewItems starts to load a component.
   * @internal
   */
  @Event() nvNavViewViewWillLoad!: EventEmitter<any>;

  /**
   * Emitted when the NavigationItems is activated.
   * @internal
   */
  @Event({
    composed: true,
    cancelable: true,
    bubbles: true,
  }) nvNavViewItemsActive!: EventEmitter<any>;


  public nvNavViewItemsDidChangeHandler(event: CustomEvent<NvNavViewItemsDidChange>) {
    this.nvNavViewItemsActive.emit({
      items: event.detail.items,
      item: event.detail.item,
    });
  }

  private async initSelect(): Promise<void> {
    // wait for all navItems to be ready.
    await Promise.all(this.navItems.map(item => item.componentOnReady()));
  }

  private makeRightContainer(): HTMLDivElement | null {
    if (this.mode === 'top') {
      return (
        <div class='nv-nav-view-right-content'>
          <slot name='footer'/>
        </div>
      );
    }

    return null;
  }

  private addEvents(): void {
    window.addEventListener('resize', this.resize);
    if (this.items) {
      this.items.forEach((item) => {
        item.addEventListener(
          'nvNavViewItemsDidChange',
          (e: any) =>
            this.nvNavViewItemsDidChangeHandler(e),
        );
      });
    }
  }

  private resize = (): void => {
    const winSize: number = window.innerWidth || 0;
    const menuMb = this.el.querySelector('.nv-navigation-view-mb-menu');

    if (winSize < this.initialWidth && !this.responsive) {
      this.responsive = true;

      this.items.forEach((item: Element) => {
        if (!item.classList.contains('nv-no-move')) {
          menuMb?.appendChild(item);
        }
      });

      // Move the search box
      if (this.search) {
        this.el.querySelector('.nv-nav-view-search-box')?.prepend(this.search);
      }

      this.itemsCenter.forEach((item: Element) => {
        menuMb?.appendChild(item);
      });

      // Move the NavViewHeader to center.
      if (this.nvNavHeader?.centerMb) {
        this.el.querySelector('.nv-nav-view-center-content')?.appendChild(this.nvNavHeader);
      }

      this.getMnMbScrollHeight();
      return;
    }

    if (winSize > this.initialWidth && this.responsive) {
      const leftContent = this.el.querySelector('.nv-nav-view-left-content');
      const centerContent = this.el.querySelector('.nv-nav-view-center-content');

      if (this.nvNavHeader?.centerMb) {
        this.headerParent?.appendChild(this.nvNavHeader);
      }

      this.items.forEach((item: Element) => {
        if (!item.classList.contains('nv-no-move')) {
          leftContent?.appendChild(item);
        }
      });

      this.itemsCenter.forEach((item: Element) => {
        centerContent?.appendChild(item);
      });

      // Move the search box
      if (this.search) {
        this.el.querySelector('.nv-nav-view-right-content')?.prepend(this.search);
      }

      this.responsive = false;
      return;
    }

    this.getMnMbScrollHeight();
  }

  private async getDimensions(): Promise<void> {
    // Elements
    const leftContent = this.el.querySelector<HTMLDivElement>('.nv-nav-view-left-content');
    const rightContent = this.el.querySelector<HTMLDivElement>('.nv-nav-view-right-content');
    const centerContent = this.el.querySelector<HTMLDivElement>('.nv-nav-view-center-content');
    const menuMb = this.el.querySelector<HTMLDivElement>('.nv-navigation-view-mb-menu');
    const styleHost: CSSStyleDeclaration = window.getComputedStyle(this.el);

    // Calc dimensions
    const widthLeftContent: number = leftContent ? leftContent.offsetWidth : 0;
    const widthRightContent: number = rightContent ? rightContent.offsetWidth : 0;
    const widthCenterContent: number = centerContent ? centerContent.offsetWidth : 0;
    const paddingLeft: number = parseInt(styleHost.paddingLeft, 10);
    const paddingRight: number = parseInt(styleHost.paddingRight, 10);
    const paddingCenter: number = parseInt(styleHost.paddingRight, 10);

    // Apply dimensions
    this.items = this.el.querySelectorAll('.nv-nav-view-left-content > *');
    this.itemsCenter = this.el.querySelectorAll('.nv-nav-view-center-content > *');
    this.initialWidth = widthLeftContent +
      widthRightContent +
      widthCenterContent +
      paddingLeft +
      paddingRight +
      paddingCenter;
    this.heightMnMb = menuMb?.scrollHeight || 0;
    this.hostHeight = this.el.offsetHeight;
  }

  private async getMnMbScrollHeight(): Promise<void> {
    if (this.position === 'fixed') {
      const winHeight: number = window.innerHeight || 0;
      this.heightMnMb = winHeight - this.hostHeight;
      return;
    }

    const menuMb = this.el.querySelector('.nv-navigation-view-mb-menu');
    await Promise.all(this.navItems.map(item => item.componentOnReady()));
    this.heightMnMb = menuMb?.scrollHeight || 0;
  }

  private openMnMb = (): void => {
    if (this.heightMnMb <= 0) {
      this.getMnMbScrollHeight().then(() => {
        this.openMnMb();
      });
      return;
    }

    if (this.mnMbActive) {
      this.mnMbActive = false;
      return;
    }
    this.mnMbActive = true;
  }

  render() {
    const rightContainer = this.makeRightContainer();
    return (
      <Host class={{
        'nv-navigation-view': true,
        'nv-navigation-view-left': this.mode === 'left',
        'nv-navigation-view-top': this.mode === 'top',
        'nv-navigation-view-top-responsive': this.mode === 'top' && this.responsive,
        'nv-navigation-view-top-fixed': this.mode === 'top' && this.position === 'fixed',
      }}>
        <div class="nv-nv-view-container">
          <div class='nv-nav-view-left-content'>
            <div
              class='nv-navigation-view-menu nv-no-move'
              onClick={ this.openMnMb }>
              <svg width='30' height='30' viewBox='0 0 30 30'>
                <g transform='translate(0 -289.06)'>
                  <path
                    transform='translate(0 289.06)'
                    d='m3 6v2h24v-2h-24zm0 8v2h24v-2h-24zm0 8v2h24v-2h-24z'
                  />
                </g>
              </svg>
            </div>
            <slot />
          </div>
          <div class='nv-nav-view-center-content'>
            <slot name='center'/>
          </div>
          { rightContainer }
        </div>
        <div
          class={{
            'nv-navigation-view-mb-menu': true,
            'nv-nav-view-mnmb-active': this.mnMbActive,
            'nv-nav-view-mnmb-hidden': !this.mnMbActive,
          }}
          style={{ height: `${this.heightMnMb}px` }}
        >
          <div class='nv-nav-view-search-box'></div>
        </div>
      </Host>
    );
  }
}
