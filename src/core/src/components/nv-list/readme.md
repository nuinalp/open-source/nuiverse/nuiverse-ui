# nv-list



<!-- Auto Generated Below -->


## Properties

| Property       | Attribute       | Description                                               | Type      | Default |
| -------------- | --------------- | --------------------------------------------------------- | --------- | ------- |
| `details`      | `details`       | If `true`, the list will be transformed in a detail list. | `boolean` | `false` |
| `email`        | `email`         | If `true`, the list will have a **email** style.          | `boolean` | `false` |
| `stickyHeader` | `sticky-header` | If `true`, the header will have a sticky effect.          | `boolean` | `false` |
| `users`        | `users`         | If `true`, the list will have a **user** style.           | `boolean` | `false` |


## Events

| Event         | Description                            | Type               |
| ------------- | -------------------------------------- | ------------------ |
| `nvDidUpdate` | Emitted when the component is updated. | `CustomEvent<any>` |


## Dependencies

### Used by

 - [nv-people-picker](../nv-people-picker)
 - [nv-upload](../nv-upload)

### Graph
```mermaid
graph TD;
  nv-people-picker --> nv-list
  nv-upload --> nv-list
  style nv-list fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
