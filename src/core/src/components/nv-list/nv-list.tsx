/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Prop, Event, EventEmitter, Host, Element, Fragment } from '@stencil/core';

@Component({
  tag: 'nv-list',
  shadow: true,
  styleUrl: './style.scss',
})
export class NvList {
  container!: HTMLDivElement;
  nvList!: HTMLElement;
  headerList!: NodeListOf<Element>;

  @Element() el!: HTMLNvListElement;

  /**
   * If `true`, the list will have a **user** style.
   */
  @Prop() users: boolean = false;

  /**
   * If `true`, the list will have a **email** style.
   */
  @Prop() email: boolean = false;

  /**
   * If `true`, the header will have a sticky effect.
   */
  @Prop() stickyHeader: boolean = false;

  /**
   * If `true`, the list will be transformed in a detail list.
   */
  @Prop() details: boolean = false;

  //
  // Events
  //

  /**
   * Emitted when the component is updated.
   */
  @Event() nvDidUpdate!: EventEmitter<any>;

  componentDidLoad() {
    if (this.stickyHeader) {
      requestAnimationFrame(() => {
        const headers = this.el.querySelectorAll('nv-list-header');
        headers.forEach((header) => header.setAttribute('sticky-header', 'true'));
      })
    }
  }

  componentDidUpdate() {
    this.nvDidUpdate.emit();
  }

  render() {
    return (
      <Host
        ref={ (el) => this.nvList = el! }
        class={{
          'nv-list': !this.details,
          'nv-list-users-type': this.users && !this.details,
          'nv-list-email-type': this.email && !this.details,
          'nv-list-sticky-header': this.stickyHeader && !this.details,
          'nv-details-list': this.details,
        }}
      >
        {/* Details */}
        {this.details &&
          <Fragment>
            <div class='nv-details-list-header'>
              <slot name='header'/>
            </div>
            <div class='nv-details-list-body'>
              <slot/>
            </div>
          </Fragment>
        }
        {
          !this.details && <slot />
        }
      </Host>
    );
  }
}
