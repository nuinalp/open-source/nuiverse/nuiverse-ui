/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import {
  Component,
  h,
  Element,
  Prop,
  State,
  Method,
  Event,
  EventEmitter,
  Host,
  Fragment,
} from '@stencil/core';

import {
  TextFieldVariants,
  InputChangeEventDetail,
  Color,
} from '../../interface';

@Component({
  tag: 'nv-text-field',
  styleUrl: './style.scss',
  shadow: true,
})
export class NvTextField {

  input!: HTMLInputElement | HTMLTextAreaElement;

  /**
   * Reference to the host element
   */
  @Element() el!: HTMLNvTextFieldElement;

  //
  // State
  //

  @State() inputFocus: boolean = false;
  @State() valueLength: number = 0;
  @State() prefixWidth: number = 0;
  @State() inputType: 'text' | 'password' | 'email' = 'text';
  @State() labelActive: boolean = false;
  @State() timer!: ReturnType<typeof setTimeout> | null;

  //
  // Props
  //

  /**
   * If `true`, the value of the input is readonly
   */
  @Prop() readonly: boolean = false;

  /**
   * The name of the input, which is submitted with the form data.
   */
  @Prop() name?: string | undefined;

  /**
   * The label for the input.
   */
  @Prop() label?: string | undefined;

  /**
   * The helper text for the input.
   */
  @Prop() helperText?: string | undefined;

  /**
   * The helper text for the input.
   */
  @Prop() errorText?: string | undefined;

  /**
   * The style for the input.
   */
  @Prop() variant?: TextFieldVariants;

  /**
   * The max length for the input.
   */
  @Prop() max?: number | undefined;

  /**
   * The min length for the input.
   */
  @Prop() min: number = 0;

  /**
   * If `true`, the counter is displayed.
   */
  @Prop() counter: boolean = false;

  /**
   * The initial value for the input.
   */
  @Prop({ mutable: true }) value?: string;

  /**
   * The prefix for the input
   */
  @Prop() prefixText?: string;

  /**
   * The suffix for the input
   */
  @Prop() suffixText?: string;

  /**
   * The type of the input.
   */
  @Prop() type: 'text' | 'password' | 'email' = 'text';

  /**
   * If `true`, the text field will be a textarea.
   */
  @Prop() multiline: boolean = false;

  /**
   * `Width` of the `textarea`
   */
  @Prop() width?: number;

  /**
   * `Height` of the `textarea`
   */
  @Prop() height?: number;

  /**
   * If `true` the height element will increase based on the element.
   * Available only for **multiline(textarea)**.
   */
  @Prop() autoGrow: boolean = false;

  /**
   * Specifies the visible width of a text area.
   */
  @Prop() cols?: number | undefined;

  /**
   * The number of visible text lines for the control.
   */
  @Prop() rows?: number | undefined;

  /**
   * If `true`, the textarea cannot be resized.
   */
  @Prop() noResize: boolean = false;

  /**
   * If `true`, the content of the password input will be displayed.
   */

  @Prop() passView: boolean = false;

  /**
   * The color to use from your application's color palette.
   */
  @Prop() color?: Color;

  /**
   * If `true`, will adjust vertical spacing of this and contained components.
   */
  @Prop() dense: boolean = false;

  /**
   * If `true`, the text-field will have a naked style.
   */
  @Prop() naked: boolean = false;

  /**
   * Set the amount of time, in milliseconds, to wait to trigger the nvInputChangeChange event after each keystroke.
   */
   @Prop() debounce = 0;

  /**
   * The text field enable status.
   */
  @Prop() disabled: boolean = false;


  //
  // Lifecyles
  //

  componentDidLoad() {
    if (this.prefixText) {
      const prefix = this.el.querySelector<HTMLElement>('.nv-text-field-prefix');
      this.prefixWidth = prefix?.offsetWidth || 0;
    }

    this.inputType = this.type;

    const labelActive = this.el.querySelector('.nv-txt-field-label-active span');
    const labelContainer = this.el.querySelector('.nv-text-field-label');

    if (labelActive && labelContainer) {
      labelActive.addEventListener<any>('transitionend', (e: TransitionEvent) => this.watchEvents(e));
      labelContainer.addEventListener<any>('transitionend', (e: TransitionEvent) => this.watchEvents(e));
    }
  }

  //
  // Events
  //

  /**
   * Emitted when a keyboard input occurred.
   */
  @Event() nvInputChange!: EventEmitter<InputChangeEventDetail>;

  /**
   * Emitted when the value of the input is changed.
   */
  @Event() nvChange!: EventEmitter<InputChangeEventDetail>;

  /**
   * Emitted when the press the enter.
   */
  @Event() nvEnter!: EventEmitter<InputChangeEventDetail>;

  /**
   * Emitted when the input loses focus.
   */
  @Event() nvBlur!: EventEmitter<FocusEvent>;

  /**
   * Emitted when the input has focus.
   */
  @Event() nvFocus!: EventEmitter<FocusEvent>;

  //
  // Methods
  //

  /**
   * Select all content from the input.
   */
  @Method()
  async selectText() {
    const valueLength = this.input.value.length;
    this.input.focus();
    this.input.setSelectionRange(0, valueLength);
  }

  @Method()
  async setFocus() {
    const input = this.el.shadowRoot?.querySelector('input');
    if (input) input.focus();
  }

  private watchEvents(evt: TransitionEvent): void {
    const el = evt.target as HTMLElement;
    if (evt.type === 'transitionend') {

      // We only process 'opacity' because all states have this
      // change.
      if (evt.propertyName !== 'transform') {
        return;
      }

      if (
        el.classList.contains('nv-txt-small-label') &&
        this.inputFocus &&
        !this.labelActive
      ) {
        this.inputFocus = false;
      }

      if (el.classList.contains('nv-text-field-label') && this.inputFocus) {
        this.labelActive = true;
      }
    }
  }

  private makeLabel() {
    return (
      this.label && <Fragment>
        <div
          class={{
            'nv-txt-field-label': true,
            'nv-txt-field-label-active': this.inputFocus || Boolean(this.value),
          }}
          onClick={() => this.setFocus()}
          >
            <div>
              <span>{ this.label }</span>
            </div>
            <span class="label-fx">{ this.label }</span>
          </div>
      </Fragment>
    );
  }

  private makeBorderBottom() {
    return (
      <div class={{
        'nv-line-ripple': true,
        active: this.inputFocus,
      }}></div>
    );
  }

  private makeHelperText() {
    if (!this.errorText || this.errorText?.length <= 0)
      return <div class='nv-text-field-helper-text'>{ this.helperText }</div>;
  }

  private renderErrorText() {
    if (this.errorText && this.errorText?.length >= 0)
      return <div class='nv-text-field-error-text'>{ this.errorText }</div>;
  }

  private makeCounter() {
    if (this.counter && this.max) {
      return <small class='nv-text-field-counter'>{ this.valueLength } / { this.max }</small>;
    }
  }

  private makePrefix() {
    if (this.prefixText) {
      return <span class='nv-text-field-prefix'>{ this.prefixText }</span>;
    }
  }

  private makeSuffix() {
    if (this.suffixText) {
      return <span class='nv-text-field-suffix'>{ this.suffixText }</span>;
    }
  }

  private sendNvChange(event: any) {
    const newValue = `${this.prefixText || ''}${event.target.value}${this.suffixText || ''}`;
    const el = event.target;

    this.value = newValue;
    this.nvInputChange.emit({ value: newValue });

    if (this.counter) {
      this.valueLength = newValue.length;
    }

    if (this.autoGrow && this.multiline) {
      this.height = el.scrollHeight;
    }

    this.nvChange.emit({ value: newValue });
  }

  private handleChange(event: any) {
    if (this.timer) clearTimeout(this.timer);

    if (!Boolean(this.debounce)) {
      this.sendNvChange(event);
      return;
    }
    
    this.timer = setTimeout(() => {
      if (this.timer) clearTimeout(this.timer);
      this.timer = null;
    }, Number(this.debounce));
  }

  private onFocus = (ev: FocusEvent): void => {
    this.inputFocus = true;
    this.nvFocus.emit(ev);
  }

  private onBlur = (ev: FocusEvent): void => {
    this.inputFocus = false;
    this.nvBlur.emit(ev);
  }

  private handleKeyDown(event: KeyboardEvent): void {
    if (event.key === 'Enter') {
      this.nvEnter.emit({ value: this.value });
    }
  }

  private showPassword = () => {
    if (this.passView) {
      this.passView = false;
      return;
    }

    this.passView = true;
  }

  private makeInput(): HTMLInputElement | HTMLTextAreaElement | null {
    if (this.multiline) {
      return (
        <textarea
          onFocus={this.onFocus}
          onBlur={this.onBlur}
          onInput={ event => this.handleChange(event)}
          maxLength={ this.max }
          minLength={ this.min }
          readonly={ this.readonly }
          cols={ this.cols }
          rows={ this.rows }
          style={{
            height: `${this.height}px`,
            resize: `${this.noResize ? 'none' : null}`,
          }}
          value={ this.value }
          ref={ el => this.input = el! }
          name={ this.name }
        ></textarea>
      );
    }

    return (
      <input
        type={ this.type === 'password' ? (this.passView ? 'text' : 'password') : this.type }
        onFocus={this.onFocus}
        onBlur={this.onBlur}
        disabled={this.disabled}
        onInput={ event => this.handleChange(event)}
        onKeyDown={ event => this.handleKeyDown(event) }
        maxLength={ this.max }
        minLength={ this.min }
        readonly={ this.readonly }
        value={ this.value }
        ref={ el => this.input = el! }
        name={ this.name }
      />
    );
  }

  private makeView() {
    // tslint:disable: max-line-length
    if (this.inputType === 'password') {
      if (!this.passView) {
        return (
          <nv-button
            onClick={ this.showPassword }
            class='nv-txtfield-pass-view-btn'
            circle
            text>
            <svg viewBox='0 0 30 30'>
              <g transform='translate(0 -289.06)'>
                <path
                  transform='translate(0 289.06)'
                  d='m25.607 4.3926-21.215 21.215 1.4141 1.4141 5.8438-5.8438a4
                  4 0 0 0 3.3496 1.8223 4 4 0 0 0 4-4 4 4 0 0 0-1.8203-3.3516l4.2812-4.2812c2.1638
                  1.8324 3.5391 4.5664 3.5391 7.6328h2c0-3.6056-1.6064-6.8373-4.1328-9.0391l4.1543-4.1543-1.4141-1.4141zm-10.607
                  2.6074c-6.6156 0-12 5.3844-12 12h2c0-5.5347 4.4653-10 10-10 0.96443 0 1.8939 0.14328 2.7754 0.39648l1.5684-1.5684c-1.3491-0.52662-2.8107-0.82812-4.3438-0.82812z' />
              </g>
            </svg>
          </nv-button>
        );
      }

      return (
        <nv-button
          onClick={ this.showPassword }
          class='nv-txtfield-pass-view-btn'
          circle
          text
        >
          <svg viewBox='0 0 30 30'>
            <g transform='translate(0 -289.06)'>
              <path
                d='m15 296.06c-6.6156 0-12 5.3844-12
                12h2c0-5.5347 4.4653-10 10-10 5.5347 0
                10 4.4653 10 10h2c0-6.6156-5.3844-12-12-12zm0 8a4 4 0 0 0-4 4 4
                4 0 0 0 4 4 4 4 0 0 0 4-4 4 4 0 0 0-4-4z' />
            </g>
          </svg>
        </nv-button>
      );
    }
  }

  private makeHelperLine() {
    if (this.helperText || this.errorText) {
      const helperText = this.makeHelperText();
      const counter = this.makeCounter();

      return (
        <div class='nv-text-field-helper-line'>
          {/* Helper text */}
          { helperText }

          {/* Error text */}
          { this.renderErrorText() }

          {/* Counter */}
          { counter }
        </div>
      );
    }
  }

  render() {
    const label = this.makeLabel();
    const borderBottom = this.makeBorderBottom();
    const prefix = this.makePrefix();
    const suffix = this.makeSuffix();
    const makeInput = this.makeInput();
    const helperLine = this.makeHelperLine();
    const view = this.makeView();

    return (
      <Host class={{
        'nv-text-field': true,
        'nv-text-field-password': this.type === 'password',
        'nv-text-field-dense': this.dense,
        'nv-text-field-with-label': Boolean(this.label),
        'nv-text-field-focus': this.inputFocus,
        'nv-text-field-display-prefix-sufix': (Boolean(this.value) || this.inputFocus) && (Boolean(this.suffixText) || Boolean(this.prefixText)),
        'nv-txt-field-w-suffix': Boolean(this.suffixText),
        'nv-text-field-naked': this.naked,
        'nv-txt-field-disabled': this.disabled,
      }}>
        <div class='nv-text-field-container'>
          <div class='nv-text-field-elements'>
            {/* The prefix */}
            { prefix }

            {/* Input/Txtarea */}
            { makeInput }

            {/* Suffix */}
            { suffix }

            {/* View password */}
            { view }

            {/* Label */}
            { label }

          </div>
          {/* Line boder */}
          { borderBottom }
        </div>
        {/* Helper line */}
        { helperLine }
      </Host>
    );
  }
}
