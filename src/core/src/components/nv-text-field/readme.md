# nv-text-field



<!-- Auto Generated Below -->


## Properties

| Property     | Attribute     | Description                                                                                                     | Type                                                                          | Default     |
| ------------ | ------------- | --------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------- | ----------- |
| `autoGrow`   | `auto-grow`   | If `true` the height element will increase based on the element. Available only for **multiline(textarea)**.    | `boolean`                                                                     | `false`     |
| `color`      | `color`       | The color to use from your application's color palette.                                                         | `"danger" \| "primary" \| "secondary" \| "success" \| "warning" \| undefined` | `undefined` |
| `cols`       | `cols`        | Specifies the visible width of a text area.                                                                     | `number \| undefined`                                                         | `undefined` |
| `counter`    | `counter`     | If `true`, the counter is displayed.                                                                            | `boolean`                                                                     | `false`     |
| `debounce`   | `debounce`    | Set the amount of time, in milliseconds, to wait to trigger the nvInputChangeChange event after each keystroke. | `number`                                                                      | `0`         |
| `dense`      | `dense`       | If `true`, will adjust vertical spacing of this and contained components.                                       | `boolean`                                                                     | `false`     |
| `disabled`   | `disabled`    | The text field enable status.                                                                                   | `boolean`                                                                     | `false`     |
| `errorText`  | `error-text`  | The helper text for the input.                                                                                  | `string \| undefined`                                                         | `undefined` |
| `height`     | `height`      | `Height` of the `textarea`                                                                                      | `number \| undefined`                                                         | `undefined` |
| `helperText` | `helper-text` | The helper text for the input.                                                                                  | `string \| undefined`                                                         | `undefined` |
| `label`      | `label`       | The label for the input.                                                                                        | `string \| undefined`                                                         | `undefined` |
| `max`        | `max`         | The max length for the input.                                                                                   | `number \| undefined`                                                         | `undefined` |
| `min`        | `min`         | The min length for the input.                                                                                   | `number`                                                                      | `0`         |
| `multiline`  | `multiline`   | If `true`, the text field will be a textarea.                                                                   | `boolean`                                                                     | `false`     |
| `naked`      | `naked`       | If `true`, the text-field will have a naked style.                                                              | `boolean`                                                                     | `false`     |
| `name`       | `name`        | The name of the input, which is submitted with the form data.                                                   | `string \| undefined`                                                         | `undefined` |
| `noResize`   | `no-resize`   | If `true`, the textarea cannot be resized.                                                                      | `boolean`                                                                     | `false`     |
| `passView`   | `pass-view`   | If `true`, the content of the password input will be displayed.                                                 | `boolean`                                                                     | `false`     |
| `prefixText` | `prefix-text` | The prefix for the input                                                                                        | `string \| undefined`                                                         | `undefined` |
| `readonly`   | `readonly`    | If `true`, the value of the input is readonly                                                                   | `boolean`                                                                     | `false`     |
| `rows`       | `rows`        | The number of visible text lines for the control.                                                               | `number \| undefined`                                                         | `undefined` |
| `suffixText` | `suffix-text` | The suffix for the input                                                                                        | `string \| undefined`                                                         | `undefined` |
| `type`       | `type`        | The type of the input.                                                                                          | `"email" \| "password" \| "text"`                                             | `'text'`    |
| `value`      | `value`       | The initial value for the input.                                                                                | `string \| undefined`                                                         | `undefined` |
| `variant`    | `variant`     | The style for the input.                                                                                        | `"outlined" \| "shaped" \| "shaped-filled" \| "shaped-outlined" \| undefined` | `undefined` |
| `width`      | `width`       | `Width` of the `textarea`                                                                                       | `number \| undefined`                                                         | `undefined` |


## Events

| Event           | Description                                     | Type                                  |
| --------------- | ----------------------------------------------- | ------------------------------------- |
| `nvBlur`        | Emitted when the input loses focus.             | `CustomEvent<FocusEvent>`             |
| `nvChange`      | Emitted when the value of the input is changed. | `CustomEvent<InputChangeEventDetail>` |
| `nvEnter`       | Emitted when the press the enter.               | `CustomEvent<InputChangeEventDetail>` |
| `nvFocus`       | Emitted when the input has focus.               | `CustomEvent<FocusEvent>`             |
| `nvInputChange` | Emitted when a keyboard input occurred.         | `CustomEvent<InputChangeEventDetail>` |


## Methods

### `selectText() => Promise<void>`

Select all content from the input.

#### Returns

Type: `Promise<void>`



### `setFocus() => Promise<void>`



#### Returns

Type: `Promise<void>`




## Dependencies

### Used by

 - [nv-dialog-prompt](../nv-dialog-prompt)

### Depends on

- [nv-button](../nv-button)

### Graph
```mermaid
graph TD;
  nv-text-field --> nv-button
  nv-dialog-prompt --> nv-text-field
  style nv-text-field fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
