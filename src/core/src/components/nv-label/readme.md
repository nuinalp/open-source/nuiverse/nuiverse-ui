# nv-label



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description                             | Type                  | Default     |
| ---------- | ---------- | --------------------------------------- | --------------------- | ----------- |
| `disabled` | `disabled` | If `true`, the label will be disabled.  | `boolean`             | `false`     |
| `htmlFor`  | `html-for` | The attribute's html for the label.     | `string \| undefined` | `undefined` |
| `required` | `required` | If `true`, the label will dislay a `*`. | `boolean`             | `false`     |


## Dependencies

### Used by

 - [nv-people-picker](../nv-people-picker)

### Graph
```mermaid
graph TD;
  nv-people-picker --> nv-label
  style nv-label fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
