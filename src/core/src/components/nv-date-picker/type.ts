export type IDay = {
  id: number;
  date: Date;
};
