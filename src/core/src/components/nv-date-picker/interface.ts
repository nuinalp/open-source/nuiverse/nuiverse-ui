import { locales } from './locales';

export type TLocales = typeof locales[number];


export interface DatetimeChangeEventDetail {
  value: string | undefined | null;
}