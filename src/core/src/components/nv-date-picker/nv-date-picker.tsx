/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, Prop, h, Host, State, Element, Event, EventEmitter } from '@stencil/core';
import { DatetimeChangeEventDetail, TLocales } from './interface';
import { DateTime, Info } from 'luxon';
import { IDay } from './type';
import { runNextAnimationFrame } from '../../utils';

@Component({
  tag: 'nv-date-picker',
  styleUrl: './style.scss',
  shadow: true,
})
export class NvDatePicker {
  //
  // Reference to the host element.
  //

  @Element() el!: HTMLNvDatePickerElement;

  //
  // State
  //

  @State() selectedDate: Date = new Date();

  @State() displayYear = false;

  @State() isOpen = false;

  //
  // Props
  //

  /**
   * The text of the label for the select.
   */
  @Prop() label?: string | undefined;

  /**
   * The value of date-picker
   */
  @Prop() value = new Date();

  /**
   * The locale of calendar
   */
  @Prop({ mutable: true }) locale: TLocales = 'en-US';

  /**
   * If `true`, the user cannot interact with the select.
   */
  @Prop() disabled: boolean = false;

  //
  // Events
  //

  /**
   * Emitted when the value has changed.
   */
  @Event({
    eventName: 'nvChange',
    composed: true,
    cancelable: true,
    bubbles: true,
  }) nvChange!: EventEmitter<DatetimeChangeEventDetail>;

  //
  // Lifecycles
  //

  componentWillLoad() {
    if (this.value && this.value !== this.selectedDate) this.selectedDate = this.value;
  }

  //
  // Methods
  //

  private checkSameMonth = (date: Date) => {
    return (
      date.getMonth() === this.selectedDate.getMonth()) &&
      (date.getFullYear() === this.selectedDate.getFullYear()
    );
  }

  private checkCurrentDay = (date: Date) => {
    return DateTime.fromJSDate(date).startOf('day').equals(
      DateTime.fromJSDate(this.selectedDate).startOf('day'),
    );
  }

  private setDay = (date: Date) => {
    this.selectedDate = date;
    this.nvChange.emit({ value: this.selectedDate.toJSON() });
    this.close();
  }

  private monthName = () => {
    const date = DateTime.fromJSDate(this.selectedDate).setLocale(this.locale);
    if (this.displayYear) return date.toFormat('yyyy');

    return date.toFormat('LLLL yyyy');
  }

  private setMonth = (month: number) => {
    const newDate = new Date(this.selectedDate.toJSON());
    newDate.setMonth(month);

    this.selectedDate = newDate;
    this.displayYear = false;
  }

  private updateMonhOrYear(newValue: number) {
    const newDate = new Date(this.selectedDate.toString());

    if (this.displayYear) {
      newDate.setFullYear(newValue);
      this.selectedDate = newDate;
      return;
    }

    newDate.setMonth(newValue);
    this.selectedDate = newDate;
  }

  private checkCurrentMonth = (month: number) => {
    return this.selectedDate.getMonth() === month;
  }

  private setDisplayYear = () => {
    this.displayYear = !this.displayYear;
  }

  private open = (e?: MouseEvent) => {
    const el = e?.target as HTMLElement;
    if (
      this.el.shadowRoot?.contains(el) && this.el.shadowRoot?.querySelector('.date-picker') !== el
    ) return;

    const calendar = this.el.shadowRoot?.querySelector('.calendar');
    if (!this.isOpen) {
      this.addEvent();
      calendar?.classList.add('display');
    }

    runNextAnimationFrame(() => {
      this.isOpen = !this.isOpen;
    });
  }

  private close() {
    this.isOpen = false;
    this.removeEvent();
  }

  private addEvent (): void {
    if (!this.disabled) {
      window.addEventListener('keydown', this.onKeyDown, true);
      window.addEventListener('mousedown', this.onStart, true);
    }
  }

  private removeEvent(): void {
    if (!this.disabled) {
      window.removeEventListener('keydown', this.onKeyDown);
      window.removeEventListener('mousedown', this.onStart);
    }
  }

  private onKeyDown = (e: KeyboardEvent): void => {
    if (e.key === 'Escape') {
      this.close();
    }
  }

  private onStart = (e: MouseEvent): void  => {
    const el = e.target as HTMLElement;
    if (this.el !== el) {
      this.close();
    }
  }

  private renderNavButtons = () => {
    const month = this.selectedDate.getMonth();
    const year = this.selectedDate.getFullYear();
    const yearOrMonth = this.displayYear ? year : month;

    return (
      <div class='nv-d-flex nv-align-itens-center'>
        <div
          onClick={() => this.updateMonhOrYear(yearOrMonth - 1)}
          class='nv-d-flex nv-justify-content-center nv-align-items-center nav-btns'
        >
          <nv-icon>expand-less</nv-icon>
        </div>
        <div
          onClick={() => this.updateMonhOrYear(yearOrMonth + 1)}
          class='nv-d-flex nv-justify-content-center nv-align-items-center nav-btns'
        >
          <nv-icon>expand-more</nv-icon>
        </div>
      </div>
    );
  }

  private calendarGroup<T>(data: T[], limit: number): T[][] {
    return data.reduce<T[][]>((data, day) => {
      const newData: Array<Array<T>> = data || [[]];
      const row = newData.length - 1;

      if (newData[row] && newData[row].length < limit) {
        newData[row].push(day);
        return data;
      }

      newData.push([day]);
      return newData;
    }, [[]]);
  }

  private makeCalendar() {
    if (this.displayYear) return;

    const date = this.selectedDate;
    const shortDays = Info.weekdays('short', { locale: this.locale });

    const days = this.calendarGroup<IDay>(Array.from({ length: 35 }, (_, i) => ({
      id: i,
      date: new Date(date.getFullYear(), date.getMonth(), i - 1),
    })), 7);

    return (
        <table class='nv-w-100'>
          <thead>
            { shortDays.map((day, idx) => (
              <th class='day-name' key={idx}>
                <div>
                  {/* <nv-tooltip text={longDays[idx]} position='top'> */}
                    {day}
                  {/* </nv-tooltip> */}
                </div>
              </th>
            )) }
          </thead>
          <tbody>
            {
              days.map((row, rdx) => (
                <tr key={rdx}>
                  {
                    row.map(({ date }, didx) => (
                      <td
                        onClick={() => this.setDay(date)}
                        key={didx}
                        class={{
                          'not-current-month': !this.checkSameMonth(date),
                          'selected-day': this.checkCurrentDay(date),
                          'calendar-day': true,
                        }}
                      >
                       <div>{ DateTime.fromJSDate(date).day }</div>
                      </td>
                    ))
                  }
                </tr>
              ))
            }
          </tbody>
        </table>
    );
  }

  private makeMonthsPicker() {
    if (!this.displayYear) return;

    const months = this.calendarGroup(Array.from({ length: 12 }, (_, i) => {
      const date = new Date();
      date.setMonth(i);

      return {
        month: i,
        name: DateTime.fromJSDate(date).setLocale(this.locale).toFormat('LLL'),
      };
    }), 4);

    return (
      <div class='nv-w-100'>
        {
          months.map((row, rdx) => (
            <div class='nv-d-flex nv-justify-content-around nv-mt-1' key={rdx}>
              {
                row.map(({ name, month }, didx) => (
                  <div
                    onClick={() => this.setMonth(month)}
                    key={didx}
                    class={{
                      'nv-d-flex': true,
                      'nv-align-items-center': true,
                      'nv-justify-content-center': true,
                      'selected-month': this.checkCurrentMonth(month),
                      'calendar-month': true,
                    }}
                  >
                    <div>{ name }</div>
                  </div>
                ))
              }
            </div>
          ))
        }
      </div>
    );
  }

  render() {
    return (
      <Host>
        <div
          class={{
            'nv-date-picker': true,
            opened: this.isOpen,
          }}
          onClick={this.open}
        >
          <div class='date-picker'>
            <span
              class={{
                'nv-date-picker-label': true,
                'nv-date-picker-label-active': Boolean(this.selectedDate),
              }}
            >
              { this.label }
            </span>

            <span class='nv-date-picker-selected-value'>
              { DateTime.fromJSDate(this.selectedDate)
                .setLocale(this.locale)
                .toLocaleString(DateTime.DATE_SHORT)
              }
            </span>

            {/* Select icon */}
            <svg width='30' height='30' viewBox='0 0 30 30' class='nv-date-picker-icon'>
              <g transform='translate(0 -289.06)'>
                <path d='m30 289.06v30h-30z'/>
              </g>
            </svg>
          </div>

          <div
            class='calendar'
          >
            <div>
              <div class='nv-d-flex nv-justify-content-between nv-align-itens-center'>
                <span
                  class='calendar-title'
                  onClick={this.setDisplayYear}
                >
                    { this.monthName() }
                  </span>
                  {
                    this.renderNavButtons()
                  }
              </div>
              {this.makeCalendar()}
              {this.makeMonthsPicker()}
            </div>
          </div>
        </div>
      </Host>
    );
  }
}
