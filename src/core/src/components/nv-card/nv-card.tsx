/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Element, Prop, State, Host } from '@stencil/core';
// import Ripple                    from '../../utils/ripple';
@Component({
  tag: 'nv-card',
  shadow: false,
})
export class NvCard {
  /**
   *  Reference to the host element
   */
  @Element() el!: HTMLElement;

  //
  // State
  //

  @State() footerContainer!: HTMLDivElement;

  /**
   * The compact mode for the card.
   */
  @Prop() compact: boolean = false;

  componentDidLoad() {
    // Get button element
    // const cardBody: HTMLElement = this.el.querySelector('.nv-card-body');

    // Add ripple effect
    // Ripple(cardBody);

    // Set attribute on media
    if (this.compact) {
      let media = this.el.querySelector('nv-card-media');

      if (!media) {
        media = document.createElement('nv-card-media');
        media.setAttribute('type', 'compact');
        this.el?.querySelector('.nv-card-body')?.append(media);
        return;
      }

      media.setAttribute('type-card', 'compact');
    }
  }

  render() {
    return (
      <Host
        class={{
          'nv-card': true,
          'nv-card-compact-mode': this.compact,
        }}
      >
        <div class='nv-card-body'>
          <slot />
        </div>
        {/* Footer */}
        <div class='nv-footer-container'>
          <slot name='footer'></slot>
        </div>
      </Host>
    );
  }
}
