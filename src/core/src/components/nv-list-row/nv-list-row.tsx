/**
* Copyright (c) 2020 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, h, Host } from '@stencil/core';

@Component({
  tag: 'nv-list-row',
  styleUrl: './style.scss',
  shadow: true,
})
export class NvListRow {
  render() {
    return (
      <Host class='nv-details-list-row'>
        <slot />
      </Host>
    );
  }
}
