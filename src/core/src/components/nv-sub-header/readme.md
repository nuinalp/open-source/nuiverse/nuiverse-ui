# nv-sub-header



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description     | Type     | Default |
| -------- | --------- | --------------- | -------- | ------- |
| `level`  | `level`   | The aria-level. | `number` | `1`     |


## Dependencies

### Used by

 - [nv-list-header](../nv-list-header)

### Graph
```mermaid
graph TD;
  nv-list-header --> nv-sub-header
  style nv-sub-header fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
