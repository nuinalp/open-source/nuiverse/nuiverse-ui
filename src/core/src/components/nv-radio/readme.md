# nv-radio



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description                                                    | Type                  | Default     |
| ---------- | ---------- | -------------------------------------------------------------- | --------------------- | ----------- |
| `checked`  | `checked`  | If `true`, the radio is selected.                              | `boolean`             | `false`     |
| `disabled` | `disabled` | If `true`, the user cannot interact with the radio.            | `boolean`             | `true`      |
| `name`     | `name`     | The name of the control, which is submitted with the form data | `string \| undefined` | `undefined` |
| `value`    | `value`    | The value of the radio                                         | `any`                 | `undefined` |


## Events

| Event     | Description                         | Type                      |
| --------- | ----------------------------------- | ------------------------- |
| `nvBlur`  | Emitted when the radio loses focus. | `CustomEvent<FocusEvent>` |
| `nvFocus` | Emitted when the radio has focus.   | `CustomEvent<FocusEvent>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
