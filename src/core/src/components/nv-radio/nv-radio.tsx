/**
* Copyright (c) 2019 The Nuinalp Authors. All rights reserved.
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

import { Component, Prop, h, Event, EventEmitter, Watch, Host, Element }  from '@stencil/core';

@Component({
  tag: 'nv-radio',
  styleUrl: './style.scss',
  shadow: true,
})
export class NvRadio {

  /**
   * Reference to the host element
   */
  @Element() el!: HTMLNvRadioElement;

  /**
   * Reference to the radio group
   */
  radioGroup!: HTMLNvRadioGroupElement;

  //
  // Props
  //

  /**
   * If `true`, the user cannot interact with the radio.
   */
  @Prop() disabled: boolean = true;

  /**
   * The name of the control, which is submitted with the form data
   */
  @Prop() name?: string;

  /**
   * If `true`, the radio is selected.
   */
  @Prop({ mutable: true }) checked: boolean = false;

  /**
   * The value of the radio
   */
  @Prop() value?: any | null;


  //
  // Events
  //

  /**
   * Emitted when the radio loses focus.
   */
  @Event() nvBlur!: EventEmitter<FocusEvent>;

  /**
   * Emitted when the radio has focus.
   */
  @Event() nvFocus!: EventEmitter<FocusEvent>;

  //
  // Lifecycles
  //

  connectedCallback() {
    const radioGroup = this.el.closest('nv-radio-group');
    if (radioGroup) {
      this.radioGroup = radioGroup;
      this.updateState();
    }
  }

  //
  // Watch
  //

  @Watch('checked')
  handleChecked(isChecked: boolean) {
    if (isChecked) this.updateCheck(isChecked);
  }

  //
  // Methods
  //

  private updateState = () => {
    if (this.radioGroup) {
      this.checked = this.value === this.radioGroup.value;
      return;
    }
  }

  private updateCheck = (value?: boolean) => {
    this.checked = value || !this.checked;
    if (this.checked)  this.radioGroup.value = this.value;
  }

  render() {
    return (
      <Host
        role='radio'
        class={{
          'nv-radio': true,
          'nv-radio-checked': this.checked,
          'nv-radio-disabled': this.disabled,
        }}
        onClick={ this.updateCheck }
      >
        <input
          type='radio'
          name={ this.name }
          value={ this.value }
          checked={ this.checked }
        />
        <div class='checkmark-container'>
          <span class='checkmark'></span>
        </div>
      </Host>
    );
  }
}
